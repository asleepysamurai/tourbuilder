echo "Unmaking production version of client"

echo "Modifying references to normal versions"
cd /media/sda7/tools/wamp/www/t/client/
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/jquery.min.js/jquery.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/jquery-ui-1.9.2.custom.min.js/jquery-ui-1.9.2.custom.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/jquery-ui-1.9.2.custom.min.css/jquery-ui-1.9.2.custom.css/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/bootstrap.min.js/bootstrap.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/bootstrap.min.css/bootstrap.css/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/player.bootstrap.min.css/player.bootstrap.css/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/persistanceManager.min.js/persistanceManager.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/api.min.js/api.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/ui.min.js/ui.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/modal.min.js/modal.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/tour.min.js/tour.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/step.min.js/step.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/markdown.min.js/markdown.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/app.min.css/app.css/g'
echo "Modification done"
echo "Client Done"

echo "Unmaking production version of www"
echo "Modifying references to normal versions"
cd /media/sda7/tools/wamp/www/t/server/www/
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/jquery.min.js/jquery.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/bootstrap.min.js/bootstrap.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/mailcheck.min.js/mailcheck.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/prettify.min.js/prettify.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/spin.min.js/spin.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/bootstrap-responsive.min.css/bootstrap-responsive.css/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/app.min.css/app.css/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/blog.min.css/blog.css/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/cosmo.min.css/cosmo.css/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/prettify.min.css/prettify.css/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/demoplayer.min.js/demoplayer.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/player.min.js/player.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/player.min.css/player.css/g'
echo "Modification done"
echo "www Done"

echo "Unamking production version of store"
cd /media/sda7/tools/wamp/www/t/fsstore
find . -name "*.xhtml" -print0 | xargs -0 sed -i '' -e 's/store.min.css/store.css/g'
echo "Store Done"

echo "All Done. Production ready."