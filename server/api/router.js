var qs = require('querystring');
var url = require('url');
var cookie = require('../utils/cookie.js');
var extend = require('../utils/extend.js');

//import required handlers
var user = require('./handlers/user.js');
var tour = require('./handlers/tour.js');
var player = require('./handlers/player.js');
//var tours = require('./handlers/app.js');
var common = require('./handlers/common.js');

var handlers = extend({}, common.handlers, user.handlers, tour.handlers, player.handlers);
//console.log([handlers, user.handlers, common.handlers]);

var routeValid = function(path){
	if(handlers[path]) return true;
	else return false;
};

var writeResponse = function(options){
	if(!options || !options.code) return;
	var headers = {};
	options.ctype = options.ctype ? options.ctype : 'text/json';
	headers['Content-Type'] = options.ctype;
	if(options.cookies){
		headers['Set-Cookie'] = [];
		for(var c in options.cookies)
			headers['Set-Cookie'].push(options.cookies[c]);
	}
	var body = {code: options.code};
	if(options.body) body.responseText = options.body;
	this.writeHead(200, headers);
	this.end(JSON.stringify(body));
};

var handleRequest = function(handler, req, resp){
	handler.handle(req, resp, function(err, result){
		if(err)
			resp.writer({code: 500});
		else
			resp.writer(result);
	});
};

var router = {
	//Map handlers to pathnames
	route: function(req, resp){
		resp.writer = writeResponse;

		// Do not respond to any method other than POST
		if(req.method != 'POST')
			return resp.writer({code: 405});

		var _url = url.parse(req.url);

		// Do not respond to unknown endpoints
		if(!routeValid(_url.pathname))
			return resp.writer({code: 404});

		// Get complete post data before responding to known endpoints
		req.body = '';
		req.on('data', function(data){
			req.body += data;
			//If request body is too large terminate request
			if (req.body.length > 1e6)
				return resp.writer({code: 413});
		});
		req.on('end', function(){
			req.query = qs.parse(req.body);
			var _handler = handlers[_url.pathname];
			
			// Write cookies to req.cookies
			req.cookies = cookie.getAll(req);

			// CSRF Check
			if(_handler.checkCSRF){
				var _csrfCookie = req.cookies.csrf;
				if(req.query['_csrf'] != _csrfCookie && _csrfCookie != 'expired')
					return resp.writer({code: 420, body: 'CSRF Detected. Request not fulfilled.'});
			}

			// Do not respond if required query params not present
			if(_handler.requires){
				for(var p in _handler.requires){
					if(!req.query[_handler.requires[p]])
						return resp.writer({code: 400});
				}
			}

			// Do not respond if required authentication is missing
			if(_handler.requiresAuthentication){
				user.loggedIn.handle(req, resp, function(err, result){
					if(result.code != 200 && result.code != 500){
						resp.writer({code: 401});
					}
					else{
						req.authenticatedUser = result.body.user;
						handleRequest(_handler, req, resp);
					}
				});
			}
			else
				handleRequest(_handler, req, resp);
		});
	}
};
module.exports = router;