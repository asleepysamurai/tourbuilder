var http = require('http');
var router = require('./router.js');

// Set as dev env
process.env.NODE_ENV = 'dev';

http.createServer(function (request, response){
	router.route(request, response);
}).listen(3578, '127.0.0.1');
console.log('Server running at http://127.0.0.1:3578/');