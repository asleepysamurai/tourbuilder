var crypto = require('crypto');
var bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');
var db = require('../../utils/db.js');
var cookie = require('../../utils/cookie.js');
var tour = require('./tour.js');

var smtpTransport = nodemailer.createTransport('SMTP', {
	service: 'Sendgrid',
	auth: {
		user: 'easyproducttours',
		pass: '12qw23we34er'
	}
});

var validate = {
	email: function(email){
		var eatpos = email.indexOf('@');
		if(email.length > 4 && eatpos != -1 && eatpos > 0 && eatpos < email.length-1 && email.split('@').length == 2) return true;
		return false;
	},
	password: function(pass){
		if(pass.length > 5) return true;
		return false;
	}
};

var user = {
	_createVerificationCode: function(email, name, callback){
		if(typeof name == 'function' && !callback)
			callback = name, name = null;
		var code = crypto.createHash('sha1').update(crypto.randomBytes(32).toString('ascii')).digest('hex');
		db.insert('verification', ['email', 'code', 'expires'], [email, code, Date.now() + (1000 * 60 * 60 * 24)], function(err, result){
			if(err && err.code == 'P0001')
				callback(401, {code: 401, body: err.toString()});
			else if(err)
				callback(err, result);
			else{
				var mail = {
					from: 'Easy Product Tours Support <support@easyproducttours.com>',
					to: email,
					subject: 'Please activate your account',
					text: 'Hi'+(name?' '+name:'')+',\nYou (or someone on your behalf) recently signed up for an Easy Product Tours account. Your account has been provisioned, and requires one last action on your part to activate. To activate your account, please copy and paste the below link into your browser\'s address bar.\nhttp://www.easyproducttours.com/user/activate?c='+code+'\n\nIf you did not sign up for an Easy Product Tours account, or if you believe you received this email by mistake, please let us know by replying to this email.\n\nIf you are having any trouble with activating your account, just reply to this email, and we\'ll happily help you out.\n\nThanks for signing up for Easy Product Tours, and have a great day :)\nEPT Support',
					html: 'Hi'+(name?' '+name:'')+',<br>You (or someone on your behalf) recently signed up for an Easy Product Tours account. Your account has been provisioned, and requires one last action on your part to activate. To activate your account simply visit the link below.<br><a href="https://www.easyproducttours.com/user/activate?c='+code+'">Activate Account</a>.<br><br>If you cannot click the above link, please copy and paste the below link into your browser\'s address bar.<br>http://www.easyproducttours.com/user/activate?c='+code+'<br><br>If you did not sign up for an Easy Product Tours account, or if you believe you received this email by mistake, please let us know by replying to this email.<br>If you are having any trouble with activating your account, just reply to this email, and we\'ll happily help you out.<br><br>Thanks for signing up for Easy Product Tours, and have a great day :)<br>EPT Support'
				};
				smtpTransport.sendMail(mail, function(err, result){
					if(err)
						callback(null, {code: 500});
					else
						callback(null, {code: 200});
				});
			}
		});
	},
	_validateUser: function(email, pass, callback){
		db.select('users', ['*'], 'email', email, function(err, results){
			if(err)
				callback(err);
			else if(!results || results.rowCount <= 0)
				callback({cc: 404});
			else{
				bcrypt.compare(pass, results.rows[0].pass, function(err, valid){
					if(err)
						callback({cc: 500});
					else if(valid && !results.rows[0].verified)
						callback({cc: 201});
					else
						callback(null, {valid: valid, plan: results.rows[0].plan});
				});
			}
		});
	},
	_initSession: function(email, callback){
		var sessionKey = crypto.createHash('sha1').update(crypto.randomBytes(32).toString('ascii')).digest('hex');
		var csrfToken = crypto.createHash('sha1').update(crypto.randomBytes(32).toString('ascii')).digest('hex');
		var expires = Date.now() + (1000 * 60 * 60 * 24);
		db.upsert('sessions', ['email'], ['key', 'email', 'expires'], [sessionKey, email, expires], [null, null, 'BIGINT'], function(err, result){
			if(err)
				callback({code: 500, err: err});
			else{
				callback({code: 200, cookies: [cookie.make('session', sessionKey, 'www.easyproducttours.com', '/', expires, true),
														 cookie.make('csrf', csrfToken, 'www.easyproducttours.com', '/', expires)]});
			}
		});
	},
	create: {
		checkCSRF: true,
		requires: ['e', 'p'],
		handle: function(req, resp, callback){
			if(!validate.password(req.query.p))
				callback(null, {code: 400, body: 'pass'});
			else if(!validate.email(req.query.e))
				callback(null, {code: 400, body: 'email'});
			else{
				bcrypt.hash(req.query.p, 16, function(err, hash){
					db.insert('users', ['email', 'pass'], [req.query.e, hash], function(err, results){
						if(err && err.code == 23505) //Account already exists. Check if valid user and login
							user.login.handle(req, resp, callback);
						else if(err)
							 callback(null, {code: 500});
						else{
							//Account created successfully. Create a verification code.
							user._createVerificationCode(req.query.e, function(err, results){
								if(err)
									console.log('Creating verification code failed.' + err.code == 23503 ? 'No such user exists.' : '');
							});
							callback(null, {code: 201});
						}
					});
				});
			}
		}
	},
	verify: {
		requires: ['c'],
		handle: function(req, resp, callback){
			db._query('DELETE FROM verification WHERE code=$1 RETURNING *', [req.query.c], function(err, results){
				if(err)
					callback(null, {code: 500});
				else if(!results || results.rowCount <= 0)
					callback(null, {code: 401});
				else if(Date.now() < results.rows[0].expires){
					db.update('users', ['verified'], [true], 'email', results.rows[0].email, function(err, results){
						if(err)
							callback(null, {code: 500});
						else{
							user._initSession(results.rows[0].email, function(respData){
								callback(null, respData);
							});
						}							
					});
				}
				else
					callback(null, {code: 409});
			});
		}
	},
	resend: {
		requires: ['e'],
		handle: function(req, resp, callback){
			if(!validate.email(req.query.e))
				callback(null, {code: 400, body: 'email'});
			else{
				//Create new code and send - delete old code
				db.remove('verification', 'email', req.query.e, function(err, results){
					if(err)
						callback(null, {code: 500});//('Error occurred while trying to destroy old verification code');
					else{
						user._createVerificationCode(req.query.e, function(err, results){
							if(err && err == 401)
								callback(null, {code: 401});
							else if(err)
								callback(null, {code: err.code == 23503 ? 404 : 500});
							else{
								callback(null, {code: 200});
							}
						});
					}
				});
			}
		}
	},
	forgot: {
		requires: ['e'],
		handle: function(req, resp, callback){
			if(!validate.email(req.query.e))
				callback(null, {code: 400, body: 'email'});
			else{
				db.remove('reset', 'email', req.query.e, function(err, results){
					if(err)
						callback(null, {code: 500});//('Error occurred while trying to destroy old reset code');
					else{
						var code = crypto.createHash('sha1').update(crypto.randomBytes(32).toString('ascii')).digest('hex'); 
						db.insert('reset', ['email', 'code', 'expires'], [req.query.e, code, Date.now() + (1000 * 60 * 60 * 24)], function(err, results){
							if(err)
								callback(null, {code: err.code == 23503 ? 404 : 500});
							else{
								var mail = {
									from: 'Easy Product Tours Support <support@easyproducttours.com>',
									to: req.query.e,
									subject: 'Reset your password',
									text: 'Hi,\nYou (or someone on your behalf) recently requested a password reset for your Easy Product Tours account. To complete your password reset, please copy and paste the below link into your browser\'s address bar.\nhttp://www.easyproducttours.com/user/changepassword?c='+code+'\n\nIf you did not request a password reset, or if you believe you received this email by mistake, simply ignore it.\nIf you are having any trouble with resetting your password, just reply to this email.\n\nThanks and have a great day :)<br>EPT Support',
									html: 'Hi,<br>You (or someone on your behalf) recently requested a password reset for your Easy Product Tours account. To complete your password reset, please visit the link below:<br><a href="https://www.easyproducttours.com/user/changepassword?c='+code+'">Reset Password</a>.<br><br>If you cannot click the above link, please copy and paste the below link into your browser\'s address bar.<br>http://www.easyproducttours.com/user/changepassword?c='+code+'<br><br>If you did not request a password reset, or if you believe you received this email by mistake, simply ignore it.<br>If you are having any trouble with resetting your password, just reply to this email.<br><br>Thanks and have a great day :)<br>EPT Support'
								};
								smtpTransport.sendMail(mail, function(err, result){
									if(err)
										callback(null, {code: 500});
									else
										callback(null, {code: 200});
								});
							}
						});
					}
				});
			}
		}
	},
	change: {
		requires: ['c', 'p'],
		handle: function(req, resp, callback){
			if(req.query.p.length < 6)
				callback(null, {code: 400, body: 'pass'});
			else{
				db._query('DELETE FROM reset WHERE code=$1 RETURNING *', [req.query.c], function(err, results){
					if(err)
						callback(null, {code: 500});
					else if(!results || results.rowCount <= 0)
						callback(null, {code: 401});
					else if(Date.now() < results.rows[0].expires){
						bcrypt.hash(req.query.p, 16, function(err, hash){
							if(err)
								callback(null, {code: 500});
							db.update('users', ['pass'], [hash], 'email', results.rows[0].email, function(err, results){
								if(err)
									callback(null, {code: 500});
								else
									callback(null, {code: 200});
							});
						});
					}
					else
						callback(null, {code: 401});
				});				
			}
		}
	},
	login: {
		requires: ['e', 'p'],
		handle: function(req, resp, callback){
			if(!validate.password(req.query.p))
				callback(null, {code: 400, body: 'pass'});
			else if(!validate.email(req.query.e))
				callback(null, {code: 400, body: 'email'});
			else{
				user._validateUser(req.query.e, req.query.p, function(err, valid){
					if(err)
						callback(null, {code: err.cc ? err.cc : 500});
					else if(valid && valid.valid){
						//User logged in - create session
						user._initSession(req.query.e, function(respData){
							callback(null, respData);
						});
					}
					else
						callback(null, {code: 401});
				});
			}
		}
	},
	logout: {
		handle: function(req, resp, callback){
			var session = req.cookies.session;
			if(session){
				db._query('DELETE FROM sessions WHERE key=$1 RETURNING *', [session], function(err, results){
					if(err)
						callback(null, {code: 500});
					else{
						var date = Date.now() - (1000 * 60 * 60 * 24);
						callback(null, {code: (!results || results.rowCount <= 0) ? 404 : 200, cookies: [cookie.make('session', 'expired', 'www.easyproducttours.com', '/', date, true),
												cookie.make('csrf', 'expired', 'www.easyproducttours.com', '/', date)]});
					}
				});
			}
			else
				callback(null, {code: 404});
		}
	},
	loggedIn: {
		requires: ['url', 'loggedin'],
		handle: function(req, resp, callback){
			var session = req.cookies.session;
			if(session){
				db._query('SELECT * FROM sessions WHERE key=$1', [session], function(err, results){
					if(err)
						callback(null, {code: 500});
					else if(!results || results.rowCount <= 0 || parseInt(results.rows[0].expires) < Date.now())
						callback(null, {code: 401});
					else{
						if(req.query.url && req.query.loggedin){
							req.authenticatedUser = results.rows[0].email;
							tour.get.handle(req, resp, function(err, result){
								if(typeof result == 'object'){
									result.body = result.body ? result.body : {};
									result.body.user = results.rows[0].email;
								}
								callback(null, result);
							});
						}
						else
							callback(null, {code: 200, body: {user: results.rows[0].email, plan: results.rows[0].plan}});
					}
				});
			}
			else
				callback(null, {code: 401});
		}
	}
};
user.handlers = {
	'/api/user/create': user.create,
	'/api/user/activate': user.verify,
	'/api/user/resend': user.resend,
	'/api/user/reset': user.forgot,
	'/api/user/reset': user.reset,
	'/api/user/change': user.change,
	'/api/user/login': user.login,
	'/api/user/logout': user.logout,
	'/api/user/loggedin': user.loggedIn,
};
module.exports = user;