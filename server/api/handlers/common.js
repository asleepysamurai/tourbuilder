var cookie = require('../../utils/cookie.js');
var crypto = require('crypto');

var common = {
	csrf: {
		handle: function(req, resp){
			var csrfToken = crypto.createHash('sha1').update(crypto.randomBytes(32).toString('ascii')).digest('hex');
			callback(null, {code: 200, cookies: [cookie.make('csrf', csrfToken, 'www.easyproducttours.com', '/')]});
		}
	}
}
common.handlers = {
	'/api/csrf': common.csrf
}
module.exports = common;