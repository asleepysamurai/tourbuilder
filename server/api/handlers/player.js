var db = require('../../utils/db.js');

var validate = {
	integer: function(number){
		if(number == parseInt(number)) return true;
		return false;
	}
};

var player = {
	get: {
		requires: ['tid'],
		handle: function(req, resp, callback){
			if(parseInt(req.query.tid) != req.query.tid)
				return callback(null, {code: 400, body: 'tid'});
			req.query.fl = req.query.fl === false ? false : true;
			if(req.query.fl){
				var date = new Date(Date.now());
				var my = parseInt((date.getUTCMonth()+1)+''+date.getUTCFullYear());
				db._query('WITH new_table (tid, my) AS (values (CAST($1 AS BIGINT), CAST($2 AS INT))), upsert AS (UPDATE tpv t SET pv = pv + 1 FROM new_table nt WHERE t.tid = nt.tid AND t.my = nt.my RETURNING t.*) INSERT INTO tpv (tid, my) SELECT * FROM new_table WHERE NOT EXISTS (SELECT 1 FROM upsert up WHERE up.tid = new_table.tid AND up.my = new_table.my)', [req.query.tid, my], function(err, result){			
				});	
			}
			db.select('tours', ['*'], ['id'], [req.query.tid], function(err, tour_results){
				if(err)
					callback(null, {code: 500});
				else if(tour_results && tour_results.rows && tour_results.rows.length > 0){
					if(req.query.uid){
						db.select('secuser', ['*'], ['uid', 'fqdn'], [req.query.uid, tour_results.rows[0].fqdn], function(err, secuser_results){
							if(err)
								callback(null, {code: 500});
							else if(secuser_results && secuser_results.rows && secuser_results.rows.length > 0)
								callback(null, {code: 200, body: {tours: tour_results.rows[0], user: {uid: req.query.uid, toursSeen: secuser_results.rows[0].tours}}});
							else
								callback(null, {code: 200, body: {tours: tour_results.rows[0]}});
						});
					}
					else
						callback(null, {code: 200, body: {tours: tour_results.rows[0]}});
				}
				else
					callback(null, {code: 404});
			});
		}
	},
	userSaw: {
		requires: ['tid', 'uid', 'fqdn'],
		handle: function(req, resp, callback){
			if(!validate.integer(req.query.tid))
				return callback(null, {code: 400, body: 'tour'});
			db._query('WITH new_table (uid, fqdn, tours) AS (values ($1, $2, ARRAY[CAST($3 AS BIGINT)])), upsert AS (UPDATE secuser t SET tours = t.tours || nt.tours::BIGINT[] FROM new_table nt WHERE t.uid = nt.uid AND t.fqdn = nt.fqdn AND not(t.tours @> nt.tours::BIGINT[]) RETURNING t.*) INSERT INTO secuser (uid, fqdn, tours) SELECT * FROM new_table WHERE NOT EXISTS (SELECT 1 FROM upsert up WHERE up.uid = new_table.uid AND up.fqdn = new_table.fqdn)', [req.query.uid, req.query.fqdn, req.query.tid], function(err, result){
				if(err)
					callback(err, {code: 500});
				else{
					callback(null, {code: 200});
				}
			});
		}
	}
};

player.handlers = {
	'/api/player/tours/get': player.get,
	'/api/player/tours/usersaw': player.userSaw

};
module.exports = player;