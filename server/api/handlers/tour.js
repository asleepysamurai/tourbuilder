var db = require('../../utils/db.js');
var extend = require('../../utils/extend.js');
var libURL = require('url');

var getFQDN = function(url){
	url = url.toLowerCase();
	var _url = libURL.parse(url), ulen = _url.pathname.length-1;
	if(!_url.hostname)
		return getFQDN('http://'+url);
	url = _url.hostname;
	return {fqdn: url, afqdn: url.indexOf('www.') == 0 ? url.replace('www.','') : 'www.'+url, parts: _url};
};

var validate = {
	url: function(url){
		var _url = libURL.parse(url);
		if(!_url.hostname)
			return validate.url('http://'+url);
		if(_url.hostname && _url.pathname)
			return true;
		return false;
	},
	integer: function(number){
		if(number == parseInt(number)) return true;
		return false;
	}
};

var tour = {
	add: {
		requiresAuthentication: true,
		requires: ['name', 'fqdn'],
		handle: function(req, resp, callback){
			if(typeof req.query.steps == 'object')
				req.query.steps = JSON.stringify(req.query.steps);
			db.insert('tours', ['name', 'fqdn', 'email'], [req.query.name, req.query.fqdn, req.authenticatedUser], function(err, results){
				if(err || !results || !results.rows || !results.rows.length > 0){
					if(err && err.code == 'P0001')
						callback(null, {code: 401, body: err.toString()})
					else
					callback(null, {code: 500});
				}
				else
					callback(null, {code: 201, body: {tour: results.rows[0]}});
			});
		}
	},
	get: {
		requiresAuthentication: true,
		requires: ['url'],
		handle: function(req, resp, callback){
			db.select('tours', ['*'], ['email'], [req.authenticatedUser], 'AND del IS NOT TRUE', function(err, results){
				if(err)
					callback(null, {code: 500});
				else if(results && results.rows && results.rows.length > 0){
					var rows = results.rows, bestMatch = '', matchingTours = [], match;
					req.query.url = getFQDN(req.query.url);
					for(var r in rows){
						if(rows[r].fqdn == req.query.url.fqdn || rows[r].fqdn == req.query.url.afqdn)
							matchingTours.push(rows[r]);
					}
					if(!matchingTours.length)
						return callback(null, {code: 404});
					else
						callback(null, {code: 200, body: {tours: matchingTours}});
				}
				else
					callback(null, {code: 404});
			});
		}
	},
	edit: {			
		requiresAuthentication: true,
		requires: ['tour', 'steps'],
		handle: function(req, resp, callback){
			if(!validate.integer(req.query.tour))
				return callback(null, {code: 400, body: 'tour'});
			req.query.steps = JSON.parse(req.query.steps);
			if(typeof req.query.steps != 'object')
				return callback(null, {code: 400, body: 'step'});
			db.select('tours', ['steps'], ['id', 'email'], [req.query.tour, req.authenticatedUser], 'AND del IS NOT TRUE',	 function(err, tour_result){
				if(err)
					callback(err);
				else if(tour_result && tour_result.rows && tour_result.rows.length > 0){
					var steps = tour_result.rows[0].steps ? JSON.parse(tour_result.rows[0].steps) : [];
					//extend(true, steps, JSON.parse(req.query.steps));
					db.update('tours', ['steps'], [JSON.stringify(req.query.steps)], ['id', 'email'], [req.query.tour, req.authenticatedUser], function(err, result){
						if(err)
							callback(null, {code: 500});
						else if(result && result.rows && result.rows.length > 0)
							callback(null, {code: 200, body: JSON.stringify(result.rows[0])});
						else
							callback(null, {code: 404});
					});
				}
				else
					callback(null, {code: 404});
			});
		}
	},
	remove: {
		requiresAuthentication: true,
		requires: ['tour'],
		handle: function(req, resp, callback){				
			if(!validate.integer(req.query.tour))
				return callback(null, {code: 400, body: 'tour'});
			db.update('tours', ['del'], [true], ['id', 'email'], [req.query.tour, req.authenticatedUser], function(err, result){
				if(err)
					callback(null, {code: 500});
				else
					callback(null, {code: 200});
			});
		}
	},
	rename: {
		requiresAuthentication: true,
		requires: ['tour', 'name'],
		handle: function(req, resp, callback){				
			if(!validate.integer(req.query.tour))
				return callback(null, {code: 400, body: 'tour'});
			db.update('tours', ['name'], [req.query.name], ['id', 'email'], [req.query.tour, req.authenticatedUser], function(err, result){
				if(err)
					callback(null, {code: 500});
				else if(result && result.rows && result.rows.length > 0){
					delete result.rows[0].steps;
					callback(null, {code: 200, body: JSON.stringify(result.rows[0])});
				}
				else
					callback(null, {code: 404});
			});
		}
	}
};

tour.handlers = {
	'/api/tours/add': tour.add,
	'/api/tours/get': tour.get,
	'/api/tours/edit': tour.edit,
	'/api/tours/rename': tour.rename,
	'/api/tours/remove': tour.remove
};
module.exports = tour;