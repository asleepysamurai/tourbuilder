// Borrow extend from jQuery so we dont have to load entire jQuery.
var class2type = {
	"[object Boolean]": "boolean",
	"[object Number]": "number",
	"[object String]": "string",
	"[object Function]": "function",
	"[object Array]": "array",
	"[object Date]": "date",
	"[object RegExp]": "regexp",
	"[object Object]": "object"
};

var type = function(obj){
	if ( obj == null ) {
		return String( obj );
	}
	return typeof obj === "object" || typeof obj === "function" ?
		class2type[ Object.prototype.toString.call(obj) ] || "object" :
		typeof obj;
};
var isFunction = function(obj){
	return type(obj) === 'function';
};
var isArray = Array.isArray;

var isPlainObject = function(obj){
	// Not plain objects:
	// - Any object or value whose internal [[Class]] property is not "[object Object]"
	// - DOM nodes
	// - window
	if ( type( obj ) !== "object" || obj.nodeType ) {
		return false;
	}

	// Support: Firefox >16
	// The try/catch supresses exceptions thrown when attempting to access
	// the "constructor" property of certain host objects, ie. |window.location|
	try {
		if ( obj.constructor &&
				!Object.prototype.hasOwnProperty.call( obj.constructor.prototype, "isPrototypeOf" ) ) {
			return false;
		}
	} catch ( e ) {
		return false;
	}

	// If the function hasn't returned already, we're confident that
	// |obj| is a plain object, created by {} or constructed with new Object
	return true;
};

var extend = function(){
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[0] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;
		target = arguments[1] || {};
		// skip the boolean and the target
		i = 2;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !isFunction(target) ) {
		target = {};
	}

	// extend jQuery itself if only one argument is passed
	if ( length === i ) {
		target = this;
		--i;
	}

	for ( ; i < length; i++ ) {
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null ) {
			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( isPlainObject(copy) || (copyIsArray = isArray(copy)) ) ) {
					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && isArray(src) ? src : [];

					} else {
						clone = src && isPlainObject(src) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};
module.exports = extend;