cookie = {
	make: function(name, val, domain, path, expires, httpOnly, secure){
		if(!name || !val) return;
		name = name == 'session' ? 'EaToBu_session' : name == 'csrf' ? 'EaToBu_csrf' : name;
		var _cookie = [];
		_cookie.push(name+'='+val);
		if(domain) _cookie.push('Domain='+domain);
		if(path) _cookie.push('Path='+path);
		if(expires){
			//expires *= 1000; //Convert timestamp to milliseconds
			var expireDate = new Date(expires);
			var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
				months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
				date = expireDate.getUTCDate().toString();
			date = date.length < 2 ? "0"+date : date;
			var time = [];
			time.push(expireDate.getUTCHours());
			time.push(expireDate.getUTCMinutes());
			time.push(expireDate.getUTCSeconds());
			for(var t in time)
				time[t] = time[t] < 10 ? '0'+time[t] : time[t];
			_cookie.push('Expires='+days[expireDate.getUTCDay()]+', '+date+'-'+months[expireDate.getUTCMonth()]+'-'+expireDate.getUTCFullYear()+' '+time.join(':')+' GMT');
		}
		if(secure) _cookie.push('Secure');
		if(httpOnly) _cookie.push('HttpOnly');
		return _cookie.join('; ');
	},
	get: function(key, req){
		return cookie.getAll(req)[key];
	},
	getAll: function(req){
		var parts, cookies = {}, cookieString = req.headers ? req.headers.cookie : null;
		if(cookieString){
			cookieString = cookieString.split(';');
			for(var c in cookieString){
				cookieString[c] = cookieString[c].trim();
				parts = cookieString[c].split('=');
				cookies[parts[0] == 'EaToBu_session' ? 'session' : parts[0] == 'EaToBu_csrf' ? 'csrf' : parts[0]] = parts[1];
			}
		}
		return cookies;
	}
}
module.exports = cookie;