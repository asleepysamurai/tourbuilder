var pg = require('pg');
var connectionString = "tcp://tourbuilderapi:qwaszxwesdxc@localhost/tourbuilderdb";

var db = {
	_valueSelectorString: function(count){
		var vals = [];
		for(var i=1;i<=count;++i)
			vals.push('$'+i);
		return vals.join(', ');
	},
	_query: function(query, values, callback){
		if(!callback && values && typeof values == 'function')
			callback = values, values = null;
		pg.connect(connectionString, function(err, client){
			if(err)
				callback(err, null);
			else{				
				if(values)
					client.query(query, values, callback);
				else
					client.query(query, callback);
			}
		});
	},
	_buildSelectorString: function(selectors, values, starter){
		var selector = [];
		if(!starter) starter = 0;
		++starter;
		if(typeof selectors != 'string'){
			for(var s in selectors)
				selector.push(selectors[s]+'=$'+(parseInt(s)+starter));
			selector = selector.join(' AND ');
		}
		else
			selector = selectors+'=$'+starter;
		if(typeof values == 'string')
			values = [values];
		return {s: selector, v: values};
	},
	upsert: function(table, selectors, fields, values, casts, callback){
		var vals = [], nsf = [], i=0, casts = casts ? casts : [], sfs = [], nfs = [];
		for(var f in fields){
			vals.push(casts[f] ? 'CAST($'+(++i)+' AS '+casts[f]+')' : '$'+(++i));
			if(selectors.indexOf(fields[f]) == -1)
				nsf.push(fields[f]+' = nt.'+fields[f]);
		}
		for(var i=0,l=selectors.length;i<l;++i){
			sfs.push('up.'+selectors[i] + ' = new_table.'+selectors[i]);
			nfs.push('t.'+selectors[i] + ' = nt.'+selectors[i]);
		}
		var ftext = fields.join(', ');
		var query = 'WITH new_table ('+ftext+') AS (values ('+vals.join(', ')+')), '+
			'upsert AS (UPDATE '+table+' t SET '+nsf.join(', ')+' FROM new_table nt WHERE '+nfs.join(' AND ')+' RETURNING t.*) '+
			'INSERT INTO '+table+' ('+ftext+') SELECT * FROM new_table WHERE NOT EXISTS '+
			'(SELECT 1 FROM upsert up WHERE '+sfs.join(' AND ')+')';
		db._query(query, values, callback);
	},
	insert: function(table, fields, values, callback){
		var query = 'INSERT INTO '+table+' ('+fields.join(', ')+') values('+db._valueSelectorString(fields.length)+') RETURNING *';
		db._query(query, values, callback);
	},
	update: function(table, fields, values, selectors, selvalues, callback){
		var vals = [], sf = [], count=fields.length;
		for(var i=0;i<count;)
			sf.push(fields[i]+'=$'+(++i));
		var ss = db._buildSelectorString(selectors, selvalues, i);
		var query = 'UPDATE '+table+' SET '+sf.join(', ')+' WHERE '+ss.s+' RETURNING *';
		for(var v in ss.v)
			values.push(ss.v[v]);
		db._query(query, values, callback);
	},
	select: function(table, fields, selectors, values, extras, callback){
		if(typeof extras == 'function')
			callback = extras, extras = null;
		var ss = db._buildSelectorString(selectors, values);
		var query = 'SELECT '+fields.join(', ')+' FROM '+table+' WHERE '+ss.s+(extras ? ' '+extras : '');
		db._query(query, ss.v, callback);
	},
	remove: function(table, selectors, values, callback){
		var ss = db._buildSelectorString(selectors, values);
		var query = 'DELETE FROM '+table+' WHERE '+ss.s;
		db._query(query, ss.v, callback);
	},
	closePool: function(callback){
		//Never call closePool in production
		pg.end();
	}
}
module.exports = db;