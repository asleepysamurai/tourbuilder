var mu = require('mu2');
var extend = require('./extend.js');

mu.root = __dirname + '/../www/templates';

var render = function(name, hash, callback, js){
	hash = hash ? hash : {};
	hash.partials = hash.partials ? hash.partials : {};
	var partials = {content: 'mu/'+name};
	if(js)
		partials.scripts = 'js/'+name;
	extend(true, hash.partials, partials);

	if(process.env.NODE_ENV == 'dev')
		mu.clearCache();

	var body = '';
	mu.compileAndRender('base.mu', hash)
		.on('data', function(data){
			body += data.toString();
		})
		.on('end', function(){
			callback(null, body);
		})
		.on('error', function(error){
			callback(error, null);
		});
};
module.exports = render;