var http = require('http');
var router = require('./router.js');

// Set as dev env
process.env.NODE_ENV = 'dev';

// Monkeypatch require to flush cache on each request
// require.flush only works in dev environment
require.flush = function(){
	if(process.env.NODE_ENV == 'dev')
		this.cache = {};
};

http.createServer(function (request, response){
	router.route(request, response);
}).listen(3574, '127.0.0.1');
console.log('Server running at http://127.0.0.1:3574/');