var render = require('../../utils/render.js');
var userapi = require('../../api/handlers/user.js');

var user = {
	signup: {
		get: {
			handle: function(req, resp, callback){
				render('signup.mu', {
					title: ' - Signup',
					user: req.cookies.session ? true : false
				}, function(error, result){
					callback(error, {code: 200, body: result});
				}, true);
			}
		},
		post: userapi.create
	},
	login: {
		get: {
			unauthenticatedOnly: '/dash',
			handle: function(req, resp, callback){
				render('login.mu', {
					title: ' - Login', next: req.query.next,
					user: req.query.sb != 'ar' && req.cookies.session ? true : false
				}, function(error, result){
					var date = Date.now()-10000;
					var options = {code: 200, body: result};
					if(req.query.sb == 'ar')
						options.cookies = [cookie.make('session', 'expired', 'www.easyproducttours.com', '/', date, true),
												 cookie.make('csrf', 'expired', 'www.easyproducttours.com', '/', date)];
					callback(error, options);
				}, true);
			}
		},
		post: userapi.login
	},
	logout: {
		get: {
			handle: function(req, resp, callback){
				userapi.logout.handle(req, resp, function(error, result){
					render('logout#'+(result.code==500 ? 'error' : 'ok')+'.mu', {
						title: ' - Logout'
					}, function(error, html){
						callback(error, {code: 200, cookies: result.cookies, body: html});
					});
				});
			}
		},
		post: userapi.logout
	},
	loggedIn: {
		get: userapi.loggedIn,
		post: userapi.loggedIn
	},
	resend: {
		get: {
			handle: function(req, resp, callback){
				render('resend.mu', {
					title: ' - Resend Activation Code',
					user: req.cookies.session ? true : false
				}, function(error, result){
					callback(error, {code: 200, body: result});
				}, true);
			}
		},
		post: userapi.resend
	},
	forgot: {
		get: {
			handle: function(req, resp, callback){				
				render('forgot.mu', {
					title: ' - Forgot Password'
				}, function(error, result){
					callback(error, {code: 200, body: result});
				}, true);
			}
		},
		post: userapi.forgot
	},
	verify: {
		get: {
			handle: function(req, resp, callback){
				render('verify.mu', {
					title: ' - Activate',
					code: req.query.c
				}, function(error, result){
					callback(error, {code: 200, body: result});
				}, true);
			}
		},
		post: userapi.verify
	},
	change: {
		get: {
			requires: ['c'],
			handle: function(req, resp, callback){				
				render('change.mu', {
					title: ' - Change Password',
					code: req.query.c
				}, function(error, result){
					callback(error, {code: 200, body: result});
				}, true);
			}
		},
		post: userapi.change
	}
};
user.handlers = {
	'/login': user.login,
	'/logout': user.logout,
	'/resendactivation': user.resend,
	'/resetpassword': user.forgot,
	'/user/activate': user.verify,
	'/user/changepassword': user.change
};
module.exports = user;