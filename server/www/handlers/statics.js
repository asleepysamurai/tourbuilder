var render = require('../../utils/render.js');

var statics = {
	index: {
		get: {
			handle: function(req, resp, callback){
				render('index.mu', {
					title: ' - build an interactive tour and increase user engagement in minutes',
					user: req.cookies.session ? true : false
				}, function(error, result){
					callback(error, {code: 200, body: result});
				}, true);
			}
		}
	},
	faq: {
		get: {
			handle: function(req, resp, callback){
				render('faq.mu', {
					title: ' - Frequently Asked Questions',
					user: req.cookies.session ? true : false
				}, function(error, result){
					callback(error, {code: 200, body: result});
				});
			}
		}
	},
	pricing: {
		get: {
			unauthenticatedOnly: '/dash',
			handle: function(req, resp, callback){
				render('pricing.mu', {
					title: ' - Plans and Pricing',
					user: req.cookies.session ? true : false
				}, function(error, result){
					callback(error, {code: 200, body: result});
				}, true);
			}
		}
	},
	terms: {
		get: {
			handle: function(req, resp, callback){
				render('terms.mu', {
					title: ' - Terms and Conditions',
					user: req.cookies.session ? true : false
				}, function(error, result){
					callback(error, {code: 200, body: result});
				});
			}
		}
	},
	privacy: {
		get: {
			handle: function(req, resp, callback){
				render('privacy.mu', {
					title: ' - Privacy Policy',
					user: req.cookies.session ? true : false
				}, function(error, result){
					callback(error, {code: 200, body: result});
				});
			}
		}
	},
	plan: {
		expired: {
			get: {
				handle: function(req, resp, callback){
					render('plan-expired.mu', {
						title: ' - Plan Expired',
						user: req.cookies.session ? true : false
					}, function(error, result){
						callback(error, {code: 200, body: result});
					});
				}
			}
		},
		select: {
			get: {
				handle: function(req, resp, callback){
					render('select-plan.mu', {
						title: ' - Select a Plan',
						user: req.cookies.session ? true : false
					}, function(error, result){
						callback(error, {code: 200, body: result});
					});
				}
			}
		}
	}
};
statics.handlers = {
	'/': statics.index,
	'/index': statics.index,
	'/faq': statics.faq,
	'/signup': statics.pricing,
	'/pricing': statics.pricing,
	'/terms': statics.terms,
	'/privacy': statics.privacy,
};

module.exports = statics;