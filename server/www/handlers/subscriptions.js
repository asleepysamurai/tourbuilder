var crypto = require('crypto');
var bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');
var db = require('../../utils/db.js');
var userapi = require('../../api/handlers/user.js');

var smtpTransport = nodemailer.createTransport('SMTP', {
	service: 'Sendgrid',
	auth: {
		user: 'easyproducttours',
		pass: '12qw23we34er'
	}
});

var subscriptions = {
	order: {
		post: {
			requires: ['security_data', 'security_hash', 'ProductName','UserEmail','UserFullName','CustomerEmail','CustomerFullName'],
			handle: function(req, resp, callback){
				if(crypto.createHash('md5').update(req.query.security_data+'1026dbf3a71286ae3f6e36d0ac93b143').digest('hex') == req.query.security_hash){
					bcrypt.hash(Math.random()+'-pass', 16, function(err, hash){
						var plan, bcycle, pvlim = 10000;
						req.query.ProductName = req.query.ProductName.toLowerCase();
						if(req.query.ProductName.indexOf('[plan:3]') !== -1)
							plan = 3;
						else if(req.query.ProductName.indexOf('[plan:2]') !== -1)
							plan = 2;
						else
							plan = 1;
						if(req.query.ProductName.indexOf('[bcycle:24]') !== -1)
							bcycle = 24;
						else if(req.query.ProductName.indexOf('[bcycle:12]') !== -1)
							bcycle = 12;
						else
							bcycle = 1;
						if(plan == 3) pvlim = 1000000;
						else if(plan == 2) pvlim = 100000;
						db.insert('users', ['email', 'name', 'bemail', 'bname', 'pass', 'plan', 'bcycle', 'pvlim'], [req.query.UserEmail, req.query.UserFullName, req.query.CustomerEmail, req.query.CustomerFullName, hash, plan, bcycle, pvlim], function(err, results){
							if(err && err.code == 23505){ //Account already exists. Check if valid user and login
								var mail = {
									from: 'Easy Product Tours Support <support@easyproducttours.com>',
									to: req.query.UserEmail,
									subject: 'Alert: Account already exists',
									text: 'Hi '+req.query.UserFullName+',\nYou (or someone on your behalf) recently signed up for an Easy Product Tours account. However, this email address has already been used to register an Easy Product Tours account.\n\nYou can either continue using the old account or you can delete that account and setup a new account.\n\nIf you have forgotten your account password, please reset your account password by visiting https://www.easyproducttours.com/resetpassword.\n\nPlease let us know which action we need to take by replying to this email.\n\nThanks and have a great day :)\nEPT Support',
									html: 'Hi '+req.query.UserFullName+',<br>You (or someone on your behalf) recently signed up for an Easy Product Tours account. However, this email address has already been used to register an Easy Product Tours account.<br><br>You can either continue using the old account or you can delete that account and setup a new account.<br><br>If you have forgotten your account password, please <a href="https://www.easyproducttours.com/resetpassword">reset</a> your account password (https://www.easyproducttours.com/resetpassword).<br><br>Please let us know which action we need to take by replying to this email.<br><br>Thanks and have a great day :)<br>EPT Support'
								};
								smtpTransport.sendMail(mail, function(err, result){
									if(err)
										callback(null, {code: 500});
									else
										callback(null, {code: 200});
								});
							}
							else if(err)
								 callback(null, {code: 500});
							else{
								//Account created successfully. Create a verification code.
								userapi._createVerificationCode(req.query.UserEmail, req.query.UserFullName, function(err, results){
									if(err)
										console.log('Creating verification code failed.' + err.code == 23503 ? 'No such user exists.' : '');
								});
								callback(null, {code: 200});
							}
						});
					});
				}
				else{
					callback(null, {code: 401});
				}
			}
		}
	}
};
subscriptions.handlers = {
	'/subscriptions/order': subscriptions.order
};
module.exports = subscriptions;