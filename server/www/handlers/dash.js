var db = require('../../utils/db.js');
var libURL = require('url');
var mu = require('mu2');
var tourapi = require('../../api/handlers/tour.js');

mu.root = __dirname + '/../templates';

var render = function(name, hash, callback){
	if(process.env.NODE_ENV == 'dev')
		mu.clearCache();

	var body = '', name = 'dash/'+name;
	mu.compileAndRender(name, hash)
		.on('data', function(data){
			body += data.toString();
		})
		.on('end', function(){
			callback(null, body);
		})
		.on('error', function(error){
			callback(error, null);
		});
};

var validate = {
	url: function(url){
		var _url = libURL.parse(url);
		if(!_url.hostname)
			return validate.url('http://'+url);
		if(_url.hostname && _url.pathname)
			return true;
		return false;
	},
	integer: function(number){
		if(number == parseInt(number)) return true;
		return false;
	}
};

var sanitizeURL = function(url){
	url = url.toLowerCase();
	var _url = libURL.parse(url), ulen = _url.pathname.length-1;
	if(!_url.hostname)
		return sanitizeURL('http://'+url);
	url = _url.hostname;
	url += _url.pathname[ulen] == '/' ? _url.pathname.substring(0,ulen) : _url.pathname;
	return url;
};

var dash = {
	dash: {
		get: {
			requiresAuthentication: true,
			handle: function(req, resp, callback){
				if(!req.query.tid){
					var hash = { user: { email: req.authenticatedUser } };
					db.select('tours', ['id','name','fqdn'], 'email', req.authenticatedUser, 'AND del IS NOT TRUE ORDER BY id', function(err, tour_result){
						if(err)
							callback(err);
						else{
							if(tour_result && tour_result.rows && tour_result.rows.length > 0)
								hash.tours = tour_result.rows;

							render('app.mu', hash, function(error, html){
								callback(error, {code: 200, body: html});
							});
						}
					});
				}
				else{
					db.select('tours', ['*'], ['id','email'], [req.query.tid, req.authenticatedUser], 'AND del IS NOT TRUE', function(err, tour_result){
						if(err)
							callback(err);
						else{
							for(var r in tour_result.rows){
								var row = tour_result.rows[r];
								row.steps = JSON.parse(row.steps);
								for(var s in row.steps){
									row.steps[s].index = parseInt(s)+1;
									row.steps[s].b[0].s = row.steps[s].b[0].s == 1 ? 'Show' : row.steps[s].b[0].s == 2 ? 'Hide' : 'Auto';
									row.steps[s].b[1].s = row.steps[s].b[1].s == 1 ? 'Show' : row.steps[s].b[1].s == 2 ? 'Hide' : 'Auto';
								}
							}
							render('tour.mu', {user: {email: req.authenticatedUser}, tours: tour_result.rows}, function(error, html){
								callback(error, {code: 200, body: html});
							});
						}
					});
				}
			}
		}
	},
	tours: {
		edit: {
			post: tourapi.edit
		},
		remove: {
			post: tourapi.remove
		},
		rename: {
			post: tourapi.rename
		}		
	},
	account: {
		get: {
			requiresAuthentication: true,
			handle: function(req, resp, callback){				
				render('account.mu', {
					title: ' - Account Information',
					user: {email: req.authenticatedUser}
				}, function(error, html){
					callback(error, {code: 200, body: html});
				});
			}
		}
	}
};
dash.handlers = {
	'/dash': dash.dash,
	'/tour/edit': dash.tours.edit,
	'/tour/remove': dash.tours.remove,
	'/tour/rename': dash.tours.rename,
	'/account': dash.account
};

module.exports = dash;