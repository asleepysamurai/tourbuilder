var mu = require('mu2');
var extend = require('../../utils/extend.js');

mu.root = __dirname + '/../templates';

var render = function(name, hash, callback, js){
	hash = hash ? hash : {};
	hash.partials = hash.partials ? hash.partials : {};
	var partials = {content: 'mu/docs/'+name};
	if(js)
		partials.scripts = 'js/'+name;
	extend(true, hash.partials, partials);

	if(process.env.NODE_ENV == 'dev')
		mu.clearCache();

	var body = '';
	mu.compileAndRender('docs.mu', hash)
		.on('data', function(data){
			body += data.toString();
		})
		.on('end', function(){
			callback(null, body);
		})
		.on('error', function(error){
			callback(error, null);
		});
};

var handler = function(file, title, req, resp, callback, scripts){
	render(file, {
		title: ' - Documentation: ' + title,
		user: req.cookies.session ? true : false
	}, function(error, result){
		callback(error, {code: 200, body: result});
	}, scripts);
};

var docs = {
	index:{
		get: {
			handle: function(req, resp, callback){
				handler('index.mu', 'Getting Started', req, resp, callback);
			}
		}
	},
	general:{
		password_reset: {
			get: {
				handle: function(req, resp, callback){
					handler('password-reset.mu', 'Reset EPT User Account Password', req, resp, callback);
				}
			}
		},
		change_plan: {
			get: {
				handle: function(req, resp, callback){
					handler('change-plan.mu', 'Change EPT Plan', req, resp, callback);
				}
			}
		},
		editor_demo: {
			get: {
				handle: function(req, resp, callback){
					handler('editor-demo.mu', 'Demo', req, resp, callback, true);
				}
			}
		},
		player_demo: {
			get: {
				handle: function(req, resp, callback){
					handler('player-demo.mu', 'Demo', req, resp, callback, true);
				}
			}
		}
	},
	applications: {
		guide: {
			get: {
				handle: function(req, resp, callback){
					handler('applications-guide.mu', 'Applications Explained', req, resp, callback);
				}
			}
		},
		create: {
			get: {
				handle: function(req, resp, callback){
					handler('applications-create.mu', 'Applications Explained', req, resp, callback);
				}
			}
		},
		edit: {
			get: {
				handle: function(req, resp, callback){
					handler('applications-edit.mu', 'Applications Explained', req, resp, callback);
				}
			}
		},
		remove: {
			get: {
				handle: function(req, resp, callback){
					handler('applications-delete.mu', 'Applications Explained', req, resp, callback);
				}
			}
		}
	},
	build:{
		launch_editor: {
			get: {
				handle: function(req, resp, callback){
					handler('launch-editor.mu', 'Launch the Editor', req, resp, callback);
				}
			}
		},
		editor_interface: {
			get: {
				handle: function(req, resp, callback){
					handler('editor-interface.mu', 'Editor Interface Explained', req, resp, callback);
				}
			}
		},
		create_tour: {
			get: {
				handle: function(req, resp, callback){
					handler('create-tour.mu', 'Create a New Tour', req, resp, callback);
				}
			}
		},
		edit_tour: {
			get: {
				handle: function(req, resp, callback){
					handler('edit-tour.mu', 'Edit a Tour', req, resp, callback);
				}
			}
		},
		delete_tour: {
			get: {
				handle: function(req, resp, callback){
					handler('delete-tour.mu', 'Delete an Existing Tour', req, resp, callback);
				}
			}
		},
		multipage_tour: {
			get: {
				handle: function(req, resp, callback){
					handler('multipage-tour.mu', 'Create a Multi-Page Tour', req, resp, callback);
				}
			}
		},
		multiple_tours: {
			get: {
				handle: function(req, resp, callback){
					handler('multiple-tours.mu', 'Create Multiple Tours on a Single Page', req, resp, callback);
				}
			}
		},
		add_step: {
			get: {
				handle: function(req, resp, callback){
					handler('add-step.mu', 'Add a New Tour Step', req, resp, callback);
				}
			}
		},
		edit_step: {
			get: {
				handle: function(req, resp, callback){
					handler('edit-step.mu', 'Edit an Existing Tour Step', req, resp, callback);
				}
			}
		},
		insert_step: {
			get: {
				handle: function(req, resp, callback){
					handler('insert-step.mu', 'Insert a New Tour Step', req, resp, callback);
				}
			}
		},
		reorder_step: {
			get: {
				handle: function(req, resp, callback){
					handler('reorder-step.mu', 'Reorder a Tour Step', req, resp, callback);
				}
			}
		},
		delete_step: {
			get: {
				handle: function(req, resp, callback){
					handler('delete-step.mu', 'Delete a Tour Step', req, resp, callback);
				}
			}
		},
		quick_edit: {
			get: {
				handle: function(req, resp, callback){
					handler('quick-edit.mu', 'Quick Edit All Steps', req, resp, callback);
				}
			}
		}
	},
	play:{
		player_script: {
			get: {
				handle: function(req, resp, callback){
					handler('player-script.mu', 'Tour Player Javascript Widget', req, resp, callback);
				}
			}
		},
		load_tour: {
			get: {
				handle: function(req, resp, callback){
					handler('load-tour.mu', 'Loading a Tour', req, resp, callback);
				}
			}
		},
		track_users: {
			get: {
				handle: function(req, resp, callback){
					handler('tracking-users.mu', 'Tracking Users', req, resp, callback);
				}
			}
		},
		dynamic_urls: {
			get: {
				handle: function(req, resp, callback){
					handler('dynamic-urls.mu', 'Using Dynamic URLs', req, resp, callback);
				}
			}
		},
		play_tour: {
			get: {
				handle: function(req, resp, callback){
					handler('play-tour.mu', 'Playing a Tour', req, resp, callback);
				}
			}
		},
		pause_tour: {
			get: {
				handle: function(req, resp, callback){
					handler('pause-tour.mu', 'Pausing a Tour', req, resp, callback);
				}
			}
		},
		stop_tour: {
			get: {
				handle: function(req, resp, callback){
					handler('stop-tour.mu', 'Stopping a Tour', req, resp, callback);
				}
			}
		},
		resume_tour: {
			get: {
				handle: function(req, resp, callback){
					handler('resume-tour.mu', 'Resuming a Tour', req, resp, callback);
				}
			}
		},
		show_next_step: {
			get: {
				handle: function(req, resp, callback){
					handler('show-next-step.mu', 'Programatically Showing the Next Step', req, resp, callback);
				}
			}
		},
		show_previous_step: {
			get: {
				handle: function(req, resp, callback){
					handler('show-previous-step.mu', 'Programatically Showing the Previous Step', req, resp, callback);
				}
			}
		},
		show_step_index: {
			get: {
				handle: function(req, resp, callback){
					handler('show-step-index.mu', 'Programatically Showing a Particular Step', req, resp, callback);
				}
			}
		},
		catching_events: {
			get: {
				handle: function(req, resp, callback){
					handler('catching-events.mu', 'Catching Events Thrown by the Player', req, resp, callback);
				}
			}
		},
		catching_errors: {
			get: {
				handle: function(req, resp, callback){
					handler('catching-errors.mu', 'Catching Errors Thrown by the Player', req, resp, callback);
				}
			}
		},
		events_thrown: {
			get: {
				handle: function(req, resp, callback){
					handler('events-thrown.mu', 'List of Events Thrown by the Player', req, resp, callback);
				}
			}
		},
		errors_thrown: {
			get: {
				handle: function(req, resp, callback){
					handler('errors-thrown.mu', 'List of Errors Thrown by the Player', req, resp, callback);
				}
			}
		}
	}
};

docs.handlers = {
	'/docs': docs.index,
	'/docs/': docs.index,
	'/docs/general/password-reset': docs.general.password_reset,
	'/docs/general/change-plan': docs.general.change_plan,
	'/docs/general/editor-demo': docs.general.editor_demo,
	'/docs/general/player-demo': docs.general.player_demo,
	'/docs/applications/guide': docs.applications.guide,
	'/docs/applications/create': docs.applications.create,
	'/docs/applications/edit': docs.applications.edit,
	'/docs/applications/delete': docs.applications.remove,
	'/docs/build/launch-editor': docs.build.launch_editor,
	'/docs/build/editor-interface': docs.build.editor_interface,
	'/docs/build/create-tour': docs.build.create_tour,
	'/docs/build/edit-tour': docs.build.edit_tour,
	'/docs/build/delete-tour': docs.build.delete_tour,
	'/docs/build/multipage-tour': docs.build.multipage_tour,
	'/docs/build/multiple-tours': docs.build.multiple_tours,
	'/docs/build/add-step': docs.build.add_step,
	'/docs/build/edit-step': docs.build.edit_step,
	'/docs/build/insert-step': docs.build.insert_step,
	'/docs/build/reorder-step': docs.build.reorder_step,
	'/docs/build/delete-step': docs.build.delete_step,
	'/docs/build/quick-edit': docs.build.quick_edit,
	'/docs/play/player-script': docs.play.player_script,
	'/docs/play/load-tour': docs.play.load_tour,
	'/docs/play/tracking-users': docs.play.track_users,
	'/docs/play/dynamic-urls': docs.play.dynamic_urls,
	'/docs/play/play-tour': docs.play.play_tour,
	'/docs/play/pause-tour': docs.play.pause_tour,
	'/docs/play/stop-tour': docs.play.stop_tour,
	'/docs/play/resume-tour': docs.play.resume_tour,
	'/docs/play/show-next-step': docs.play.show_next_step,
	'/docs/play/show-previous-step': docs.play.show_previous_step,
	'/docs/play/show-step-index': docs.play.show_step_index,
	'/docs/play/catching-events': docs.play.catching_events,
	'/docs/play/catching-errors': docs.play.catching_errors,
	'/docs/play/events-thrown': docs.play.events_thrown,
	'/docs/play/errors-thrown': docs.play.errors_thrown
};

module.exports = docs;