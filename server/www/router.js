var qs = require('querystring');
var url = require('url');
var cookie = require('../utils/cookie.js');
var extend = require('../utils/extend.js');

//import required handlers
var statics = require('./handlers/statics.js');
var user = require('./handlers/user.js');
var dash = require('./handlers/dash.js');
var docs = require('./handlers/docs.js');
var subscriptions = require('./handlers/subscriptions.js');

var handlers = extend({}, statics.handlers, user.handlers, dash.handlers, docs.handlers, subscriptions.handlers);

var routeValid = function(path){
	if(handlers[path]) return true;
	else return false;
};

var writeResponse = function(options){
if(!options || !options.code) return;
	var headers = {};
	if(options.location)
		headers['Location'] = options.location;
	options.ctype = options.ctype ? options.ctype : 'text/html';
	headers['Content-Type'] = options.ctype;
	if(options.cookies){
		headers['Set-Cookie'] = [];
		for(var c in options.cookies)
			headers['Set-Cookie'].push(options.cookies[c]);
	}
	this.writeHead(options.code, headers);
	if(!this.writeHeadersOnly){
		if(options.stream)
			this.pipe(options.stream);
		else if(options.body){
			if(typeof options.body == 'object')
				options.body = JSON.stringify(options.body);
			this.write(options.body);
		}
	}
	this.end();
};

var handleRequest = function(handler, req, resp){
	handler.handle(req, resp, function(err, result){
		if(err)
			resp.writer({code: 500});
		else
			resp.writer(result);
	});
};

var router = {
	//Map handlers to pathnames
	route: function(req, resp){
		resp.writer = writeResponse;

		var _url = url.parse(req.url);

		// Do not respond to unknown endpoints
		if(!routeValid(_url.pathname))
			return resp.writer({code: 404});

		// Do not respond to unsupported methods
		var method = req.method.toLowerCase();
		if(method == 'head'){
			resp.writeHeadersOnly = true;
			method = 'get';
		}
		if(!handlers[_url.pathname][method])
			return resp.writer({code: 405});

		// Get complete post data before responding to known endpoints
		req.body = '';
		req.on('data', function(data){
			req.body += data;
			//If request body is too large terminate request
			if (req.body.length > 1e6)
				return resp.writer({code: 413});
		});
		req.on('end', function(){
			req.query = qs.parse(method == 'post' ? req.body : _url.query);
			var _handler = handlers[_url.pathname][method];
			
			// Write cookies to req.cookies
			req.cookies = cookie.getAll(req);

			// CSRF Check
			if(_handler.checkCSRF){
				var _csrfCookie = req.cookies.csrf;
				if(req.query['_csrf'] != _csrfCookie && _csrfCookie != 'expired')
					return resp.writer({code: 420, location: '/login?sb=ar&next='});
			}

			// Do not respond if required query params not present
			if(_handler.requires){
				for(var p in _handler.requires){
					if(!req.query[_handler.requires[p]])
						return resp.writer({code: 400, body: 'Required query parameters missing.'});
				}
			}

			// Show authenticated pages only if authenticated. And show unauthenticated pages only if not.
			if(_handler.requiresAuthentication || _handler.unauthenticatedOnly){
				user.loggedIn.get.handle(req, resp, function(err, result){
					if(result.code != 200 && result.code != 500){
						if(_handler.requiresAuthentication)
							resp.writer({code: 302, location: '/login?sb=ar&next='+_url.pathname});
						else if(_handler.unauthenticatedOnly)
							handleRequest(_handler, req, resp);
					}
					else{
						if(_handler.requiresAuthentication){
							req.authenticatedUser = result.body.user;
							
							handleRequest(_handler, req, resp);
						}
						else if(_handler.unauthenticatedOnly){
							resp.writer({code: 302, location: _handler.unauthenticatedOnly});
						}
					}
				});
			}
			else
				handleRequest(_handler, req, resp);
		});
	}
};
module.exports = router;