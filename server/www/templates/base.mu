<!doctype HTML>
<html>
	<head>
		<title>Easy Product Tours{{title}}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="/css/cosmo.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-responsive.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/css/app.min.css"></link>
	</head>
	<body>
		<div class="content">
			<div class="navbar navbar-inverse">
				<div class="navbar-inner">
					<div class="container">
						<a class="brand" href="/"><img src="/img/logow.png" alt="Easy Product Tours" /><span>Easy Product Tours</span></a>
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<div class="nav-collapse">
							<ul class="nav pull-right">
								<li><a href="/#features">Features</a></li>
								<li><a href="/blog">Blog</a></li>
								<li><a href="/docs">Documentation</a></li>
								{{?user}}
									<li><a href="/dash">Dashboard</a></li>
									<li><a href="/logout">Logout</a></li>
								{{/user}}
								{{?!user}}
									<li><a href="/pricing">Pricing & Signup</a></li>
									<li><a href="/login">Login</a></li>
								{{/user}}
							</ul>
						</div>
					</div>
				</div>
			</div>
			{{> content}}
		</div>
		<div class="footer nomargin">
			<p><small>Easy Product Tours - <a href="mailto:support@easyproducttours.com">Support</a> - <a href="https://www.easyproducttours.com/terms">Terms and Conditions</a> - <a href="https://www.easyproducttours.com/privacy">Privacy Policy</a></small></p>
		</div>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-38610834-1']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<script type="text/javascript" src="/js/jquery.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
		{{> scripts}}
	</body>
</html>