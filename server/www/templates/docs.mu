<!doctype HTML>
<html>
	<head>
		<title>Easy Product Tours{{title}}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="/css/cosmo.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-responsive.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/css/prettify.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/css/app.min.css"></link>
	</head>
	<body style="background-color:#eee">
		<div class="content">
			<div class="navbar navbar-inverse">
				<div class="navbar-inner">
					<div class="container">
						<a class="brand" href="/"><img src="/img/logow.png" alt="Easy Product Tours" /><span>Easy Product Tours</span></a>
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<div class="nav-collapse">
							<ul class="nav pull-right">
								<li><a href="/#features">Features</a></li>
								<li><a href="/blog">Blog</a></li>
								<li class="active"><a href="/docs">Documentation</a></li>
								{{?user}}
									<li><a href="/dash">Dashboard</a></li>
									<li><a href="/logout">Logout</a></li>
								{{/user}}
								{{?!user}}
									<li><a href="/pricing">Pricing & Signup</a></li>
									<li><a href="/login">Login</a></li>
								{{/user}}
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="docs" class="page container row-fluid">
				<div class="span12 tabbable tabs-left" style="background:#fff;padding:20px 0px;margin-bottom:20px;">
					<ul class="nav nav-tabs">
						<li class="nav-header">General</li>
						<li class="active"><a href="/docs">Getting Started</a></li>
						<li><a href="/docs/general/password-reset">Reset Your Password</a></li>
						<li><a href="/docs/general/change-plan">Change Plans</a></li>
						<li><a href="/docs/general/editor-demo">Editor Demo</a></li>
						<!--<li><a href="/docs/general/player-demo">Player Demo</a></li>-->
						<li class="divider"></li>
						<li class="nav-header">Building a Tour</li>
						<li><a href="/docs/build/launch-editor">Launch the Editor</a></li>
						<li><a href="/docs/build/editor-interface">Editor Interface</a></li>
						<li><a href="/docs/build/create-tour">Create Tour</a></li>
						<li><a href="/docs/build/edit-tour">Edit Tour</a></li>
						<li><a href="/docs/build/delete-tour">Delete Tour</a></li>
						<li><a href="/docs/build/add-step">Add Step</a></li>
						<li><a href="/docs/build/edit-step">Edit Step</a></li>
						<li><a href="/docs/build/insert-step">Insert Step</a></li>
						<li><a href="/docs/build/reorder-step">Reorder Step</a></li>
						<li><a href="/docs/build/delete-step">Delete Step</a></li>
						<li><a href="/docs/build/quick-edit">Quick Edit Tour</a></li>
						<li><a href="/docs/build/multipage-tour">Multi-Page Tours</a></li>
						<li><a href="/docs/build/multiple-tours">Multi-Tour Pages</a></li>
						<li class="divider"></li>
						<li class="nav-header">Playing a Tour</li>
						<li><a href="/docs/play/player-script">Player Script</a></li>
						<li><a href="/docs/play/load-tour">Load Tour</a></li>
						<li><a href="/docs/play/tracking-users">Tracking Users</a></li>
						<li><a href="/docs/play/dynamic-urls">Dynamic URLs</a></li>
						<li><a href="/docs/play/play-tour">Play Tour</a></li>
						<li><a href="/docs/play/pause-tour">Pause Tour</a></li>
						<li><a href="/docs/play/stop-tour">Stop Tour</a></li>
						<li><a href="/docs/play/resume-tour">Resume Tour</a></li>
						<li><a href="/docs/play/show-next-step">Show Next Step</a></li>
						<li><a href="/docs/play/show-previous-step">Show Previous Step</a></li>
						<li><a href="/docs/play/show-step-index">Show Step at Index</a></li>
						<li><a href="/docs/play/catching-events">Catching Events</a></li>
						<li><a href="/docs/play/catching-errors">Catching Errors</a></li>
						<li><a href="/docs/play/events-thrown">List of Events Thrown</a></li>
						<li><a href="/docs/play/errors-thrown">List of Errors Thrown</a></li>
					</ul>					
					<div class="tab-content" style="margin-right:20px;">
						{{> content}}
						<br><br><p>If you face any difficulty, require assistance or in general have a question, please write to us at <a href="mailto:support@easyproducttours.com">support@easyproducttours.com</a>. We are always happy to talk to you :)</p>
					</div>
				</div>
			</div>
		</div>
		<div class="footer nomargin">
			<p><small>Easy Product Tours - <a href="mailto:support@easyproducttours.com">Support</a> - <a href="https://www.easyproducttours.com/terms">Terms and Conditions</a> - <a href="https://www.easyproducttours.com/privacy">Privacy Policy</a></small></p>
		</div>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-38610834-1']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<script type="text/javascript" src="/js/jquery.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/prettify.min.js"></script>
		<script type="text/javascript">
			$('div.page li.active').removeClass('active');
			$('div.page a[href="'+window.location.pathname+'"]').parent().addClass('active');
			prettyPrint();
		</script>
		{{> scripts}}
	</body>
</html>