<!doctype HTML>
<html>
	<head>
		<title>Easy Product Tours - Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="/css/cosmo.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-responsive.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/css/app.min.css"></link>
	</head>
	<body style="background-color:#eee;">
		<div class="content">
			<div class="navbar navbar-inverse">
				<div class="navbar-inner">
					<div class="container">
						<a class="brand" href="/"><img src="/img/logow.png" alt="Easy Product Tours" /><span>Easy Product Tours</span></a>
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<div class="nav-collapse">
							<ul class="nav pull-right">
								<li><a href="/blog">Blog</a></li>
								<li><a href="/docs">Documentation</a></li>
								<li><a href="/account">{{user.email}}</a></li>
								<li><a href="/logout">Logout</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="dash" class="page">
				<div class="container row-fluid">
					<div class="span12">
						<div class="tab-content">
							<div class="tab-pane active" id="app-{{id}}">
								<form class="form-horizontal" id="table-form-app-{{id}}">
									<div class="control-group">
										<h3 class="form-header control-label">All Tours</h3>
									</div>
									<table class="table table-striped table-bordered">
										{{?tours}}
										<tr>
											<th>ID</th>
											<th>Name</th>
											<th>Edit</th>
										</tr>
										{{/tours}}
										{{#tours}}
											<tr>
												<td>{{id}}</td>
												<td>{{name}}<br><small>{{fqdn}}</small></td>
												<td><a href="/dash/{{id}}">Edit</a></td>
											</tr>
										{{/tours}}
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="nomargin">
					<div class="container row-fluid">
						<div class="span12">
							<p>To create a new tour, navigate to the page you wish to run the tour on and <a href="/docs/build/launch-editor" target="_blank">launch</a> Easy Product Tours. For detailed instructions read the <a href="/docs">documentation</a>.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
			<p><small>Easy Product Tours - <a href="mailto:support@easyproducttours.com">Support</a> - <a href="https://www.easyproducttours.com/terms">Terms and Conditions</a> - <a href="https://www.easyproducttours.com/privacy">Privacy Policy</a></small></p>
		</div>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-38610834-1']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<script type="text/javascript" src="/js/jquery.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	</body>
</html>