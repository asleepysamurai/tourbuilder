<!doctype HTML>
<html>
	<head>
		<title>Easy Product Tours - Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="/css/cosmo.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-responsive.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/css/app.min.css"></link>
	</head>
	<body>		
		<div class="content">
			<div class="navbar navbar-inverse">
				<div class="navbar-inner">
					<div class="container">
						<a class="brand" href="/"><img src="/img/logow.png" alt="Easy Product Tours" /><span>Easy Product Tours</span></a>
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<div class="nav-collapse">
							<ul class="nav pull-right">
								<li><a href="/blog">Blog</a></li>
								<li><a href="/docs">Documentation</a></li>
								<li><a href="/account">{{user.email}}</a></li>
								<li><a href="/logout">Logout</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="account" class="page container">
				<div class="row-fluid">
					<div class="span12 form-account">
						<form class="form-horizontal" id="account" novalidate="novalidate">
							<div class="control-group">
								<h3 class="form-header control-label">Account Information</h3>
							</div>
							<div class="control-group" id="cg-email">
								<label class="control-label" for="i-email">Email</label>
								<div class="controls">
									<input type="email" id="i-email" placeholder="Email" class="email login" autocorrect="off" autocapitalize="off" disabled="true" value="{{user.email}}">
									<small><span class="help-inline" id="help-email">The email id with which you signed up. In order to change your email id, please email <a href="mailto:support@easyproducttours.com">support@easyproducttours.com</a>.</span></small>
								</div>
							</div>
							<div class="control-group" id="cg-password">
								<label class="control-label" for="i-password">Password</label>
								<div class="controls">
									<input type="password" id="i-password" class="password login" autocorrect="off" autocapitalize="off" disabled="true" value="password">
									<small><span class="help-inline" id="help-password">To request a password reset, click <a href="/resetpassword">here</a>.</span></small>
								</div>
							</div>
							<div class="control-group">
								<div class="controls">
									<a href="/dash" class="btn btn-success">Go to Dashboard</a>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div id="processing">
							<span class="spinner-text">Processing... Please wait... </span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer nomargin">
			<p><small>Easy Product Tours - <a href="mailto:support@easyproducttours.com">Support</a> - <a href="https://www.easyproducttours.com/terms">Terms and Conditions</a> - <a href="https://www.easyproducttours.com/privacy">Privacy Policy</a></small></p>
		</div>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-38610834-1']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<script type="text/javascript" src="/js/jquery.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	</body>
</html>