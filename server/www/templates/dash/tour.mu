<!doctype HTML>
<html>
	<head>
		<title>Easy Product Tours - Quick Edit Tour </title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="/css/cosmo.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-responsive.min.css"></link>
		<link rel="stylesheet" type="text/css" href="/css/app.min.css"></link>
	</head>
	<body style="background-color:#eee;">
		<div class="content">
			<div class="navbar navbar-inverse">
				<div class="navbar-inner">
					<div class="container">
						<a class="brand" href="/"><img src="/img/logow.png" alt="Easy Product Tours" /><span>Easy Product Tours</span></a>
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<div class="nav-collapse">
							<ul class="nav pull-right">
								<li><a href="/blog">Blog</a></li>
								<li><a href="/docs">Documentation</a></li>
								<li><a href="/dash">Dashboard</a></li>
								<li><a href="/account">{{user.email}}</a></li>
								<li><a href="/logout">Logout</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="m-tour-del" class="modal hide fade" data-backdrop="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3>Delete this Tour</h3>
				</div>
				<div class="modal-body">
					<p>You will lose all tour related data including tour steps. Once deleted, this tour <strong>cannot</strong> be played on your site. This action is <strong>not</strong> reversible. <strong>Are you sure you want to delete this tour?</strong></p>
				</div>
				<div class="modal-footer">
					<button data-dismiss="modal" class="btn btn-primary" id="b-tour-del-cancel">Dont Delete</button>
					<button class="btn btn-danger" id="b-tour-del-ok">Delete Tour</button>
				</div>
			</div>
			<div id="m-tour-error" class="modal hide fade" data-backdrop="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3>Delete this Tour</h3>
				</div>
				<div class="modal-body">
					<p class="tour-error-500" style="display:none;">An error occurred while trying to edit this tour. Please try again later in sometime.</p>
					<p class="tour-error-404" style="display:none;">The tour you tried to edit does not exist. It might have been deleted recently.</p>
				</div>
				<div class="modal-footer">
					<button data-dismiss="modal" class="btn btn-primary" id="b-tour-del-cancel">OK</button>
				</div>
			</div>
			<div id="m-tour-rename" class="modal hide fade" data-backdrop="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3>Rename Tour</h3>
				</div>
				<form class="form-horizontal" action="#">
				<div class="modal-body">
						<div class="td-control">
							<label class="control-label" for="i-tour-rename">Tour Name</label>
							<div class="controls">
								<input type="text" id="i-tour-rename" placeholder="Tour Name">
								<span class="help-inline"></span>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="reset" data-dismiss="modal" class="btn btn-primary" id="b-tour-rename-cancel">Cancel</button>
					<button type="submit" class="btn btn-success" id="b-tour-rename-ok">Rename Tour</button>
				</div>
				</form>
			</div>
			<div id="dash" class="page">
				<div class="container row-fluid">
					<div class="span12 {{?!tours}}well{{/tours}}">
						<ul class="nav nav-pills" style="display:none">
							{{#tours}}
								<li class="{{_class}}"><a href="#tour-{{id}}" class="capitalized" id="t-tour-{{id}}" data-toggle="pill" data-tour="{{id}}" data-tour-name="{{name}}">{{name}}</a></li>
							{{/tours}}
						</ul>
						<div class="tab-content">
							{{#tours}}
								<div class="tab-pane active" id="tour-{{id}}">
									<form class="form-horizontal" id="form-tour-{{id}}">
										<h3 class="form-header capitalized" style="text-align:left;margin:20px 0px 40px 0px;"><span class="tour-name">{{name}}</span>&nbsp;&nbsp;<small><a href="/dash">(All Tours)</a></small></h3>
										<div class="inline-helper" style="padding-bottom:20px;margin-top:-20px;">
											<button class="btn btn-primary edit-steps" data-tour="{{id}}">Quick Edit</button>
											<button class="btn btn-success save-steps" data-tour="{{id}}" disabled="true">Save Changes</button>
											<div class="pull-right">
												<button class="btn btn-primary rename-tour" data-tour="{{id}}" data-target="#m-tour-rename" data-toggle="modal">Rename Tour</button>
												<button class="btn btn-danger delete-tour" data-tour="{{id}}" data-target="#m-tour-del" data-toggle="modal">Delete Tour</button>
											</div>
										</div>
										<table class="table table-striped table-bordered">
											<tr>
												<th rowspan="1">Step</th>
												<th rowspan="1">URL</th>
												<th rowspan="1">Title</th>
												<th rowspan="1" colspan="2">Content</th>
											</tr>
											{{#steps}}
												<tr class="step-{{index}}">
													<td rowspan="3">
														<span>{{index}}</span>
													</td>
													<td class="maxiwidth td-control">
														<span class="ta-val ta-page" data-name="ia-page" data-placeholder="Page URL">{{u}}</span>
													</td>
													<td class="td-control">
														<span class="ta-val ta-title" data-name="ia-title" data-placeholder="Title">{{t}}</span>
													</td>
													<td colspan="2" class="td-control">
														<span class="ta-val ta-content" data-name="ia-content" data-placeholder="Content">{{c}}</span>
													</td>
												</tr>
												<tr class="step-{{index}}">
													<th class="maxiwidth">Previous</th>
													<td class="td-control">
														<span class="t-val t-previous-title t-btn-title" data-name="i-previous-title" data-placeholder="Previous">{{b.0.t}}</span>
													</td>
													<td class="td-button">
														<strong>Display</strong>
														<a href="javascript:void(0)" class="b-event-shown-previous-{{index}} capitalized dropdown-toggle disabled b-previous-shown" data-toggle="dropdown">{{b.0.s}}{{?!b.0.s}}Auto{{/b.0.s}}</a>
														<ul class="dropdown-menu">
															<li><a href="javascript:void(0);" class="dd-show dd-event-shown" data-step="{{index}}" data-button="previous" data-tour="{{id}}">Show</a></li>
															<li><a href="javascript:void(0);" class="dd-hide dd-event-shown" data-step="{{index}}" data-button="previous" data-tour="{{id}}">Hide</a></li>
															<li><a href="javascript:void(0);" class="dd-auto dd-event-shown" data-step="{{index}}" data-button="previous" data-tour="{{id}}">Auto</a></li>
														</ul>
													</td>
													<td colspan="2" class="td-control">
														<strong class="edit-hide">Fires </strong>
														<span class="t-val t-previous-event" data-empty="{{?b.0.e}}false{{/b.0.e}}{{?!b.0.e}}true{{/b.0.e}}" data-name="i-previous-event" data-placeholder="Event Name">{{?b.0.e}}{{b.0.e}}{{/b.0.e}}{{?!b.0.e}}(None){{/b.0.e}}</span>
													</td>
												</tr>
												<tr class="step-{{index}}">
													<th class="maxiwidth">Next</th>
													<td class="td-control">
														<span class="t-val t-next-title t-btn-title" data-name="i-next-title" data-placeholder="Next">{{b.1.t}}</span>
													</td>
													<td class="td-button">
														<strong>Display</strong>
														<a href="javascript:void(0);" class="b-event-shown-next-{{index}} capitalized dropdown-toggle disabled b-next-shown" data-toggle="dropdown">{{b.1.s}}{{?!b.1.s}}Auto{{/b.1.s}}</a>
														<ul class="dropdown-menu">
															<li><a href="javascript:void(0);" class="dd-show dd-event-shown" data-step="{{index}}" data-button="next" data-tour="{{id}}">Show</a></li>
															<li><a href="javascript:void(0);" class="dd-hide dd-event-shown" data-step="{{index}}" data-button="next" data-tour="{{id}}">Hide</a></li>
															<li><a href="javascript:void(0);" class="dd-auto dd-event-shown" data-step="{{index}}" data-button="next" data-tour="{{id}}">Auto</a></li>
														</ul>
													</td>
													<td class="td-control">
														<strong class="edit-hide">Fires </strong>
														<span class="t-val t-next-event" data-empty="{{?b.1.e}}false{{/b.1.e}}{{?!b.1.e}}true{{/b.1.e}}" data-name="i-next-event" data-placeholder="Event Name">{{?b.1.e}}{{b.1.e}}{{/b.1.e}}{{?!b.1.e}}(None){{/b.1.e}}</span>
													</td>
												</tr>
											{{/steps}}
										</table>
										<div class="inline-helper">
											<button class="btn btn-primary edit-steps" data-tour="{{id}}">Quick Edit</button>
											<button class="btn btn-success save-steps" data-tour="{{id}}" disabled="true">Save Changes</button>
											<div class="pull-right">
												<button class="btn btn-primary rename-tour" data-tour="{{id}}" data-target="#m-tour-rename" data-toggle="modal">Rename Tour</button>
												<button class="btn btn-danger delete-tour" data-tour="{{id}}" data-target="#m-tour-del" data-toggle="modal">Delete Tour</button>
											</div>
										</div>
									</form>
								</div>
							{{/tours}}
						</div>
						<div id="processing-container" class="border-box">
							<div id="processing" style="display:block;">
								<span class="spinner-text"></span>
							</div>
						</div>
						{{?servererror}}
							<div>
								<h3>Could not retrieve data.</h3>
								<p>An error occurred while trying to retreive tour data. This is a temporary error and should be resolved soon. Please try again later.</p>
								<a href="/dash" class="btn btn-success">Go back to dashboard</a>
							</div>
						{{/servererror}}
						{{?!servererror}}
						{{?!tours}}
							<div>
								<h3>No such tour exists.</h3>
								<p>The tour you are trying to edit does not exist. This tour might have been deleted recently. If you believe this message is mistaken, please email <a href="mailto:support@easyproducttours.com">support@easyproducttours.com</a>.</p>
								<a href="/dash" class="btn btn-success">Go back to dashboard</a>
							</div>
						{{/tours}}
						{{/servererror}}
					</div>
				</div>
				{{?tours}}
					<div class="container row-fluid">
						<div class="inline-helper span12" style="padding-bottom:20px;">
							To add a new step, navigate to the page you wish to run the tour on and <a href="/docs/build/launch-editor" target="_blank">launch</a> Easy Product Tours. For detailed instructions read the <a href="/docs">documentation</a>.
						</div>
					</div>
				{{/tours}}
			</div>
		</div>
		<div class="footer">
			<p><small>Easy Product Tours - <a href="mailto:support@easyproducttours.com">Support</a> - <a href="https://www.easyproducttours.com/terms">Terms and Conditions</a> - <a href="https://www.easyproducttours.com/privacy">Privacy Policy</a></small></p>			
		</div>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-38610834-1']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<script type="text/javascript" src="/js/jquery.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/spin.min.js"></script>
		<script type="text/javascript">
			var initUI = function(){
				$('form').on('submit', function(ev){
					ev.preventDefault();
				});
				$('a.dd-event-shown').on('click', function(ev){
					var $t = $(this);
					$('#tour-'+$t.attr('data-tour')+' a.b-event-shown-'+$t.attr('data-button')+'-'+$t.attr('data-step')).html($t.text());
				});
				$('a[data-toggle=pill]').on('click', function(ev){
					var $t = $(this);
					history.pushState({}, document.title, '/dash/'+$t.attr('data-tour'));
				}).on('shown', function(ev){
					var $t = $(this);
					document.title = $t.attr('data-tour-name')+' - Easy Product Tours';
				});
				$(window).on('popstate', function(ev){
					var tid = $.trim(window.location.pathname).split('/');
					if(tid[3] && tid[3].length > 0)
						$('#t-tour-'+tid[3]).tab('show');
					else
						$('a[data-toggle=pill]').eq(0).tab('show');
				});
				$('button.edit-steps').on('click', edit);
				$('button.save-steps').on('click', save);
			};
			$('input, textarea').live('keyup', function(ev){
				var $t = $(this);
				if([13,27,37,38,39,40].indexOf(ev.which) == -1){
					$t.parents('.td-control').removeClass('error').find('.help-inline').text('');
					$t.removeClass('error').attr('placeholder', $t.data('placeholder'));
				}
			});
			var display = {
				error: function(input, text){
					var $t = $(input);
					$t.data('placeholder', $t.attr('placeholder'));
					$t.attr('placeholder', text);
					$t.parents('.td-control').addClass('error').find('.help-inline').text('Cannot be empty').show();
					$t.focus();
					if($('#processing').is(':visible'))
						display.processing();
				},
				processing: function(text, html){
					var $p = $('#processing');
					if($p.is(':visible')){
						$p.parent().hide();
						var restoreState = function(){ $t = $(this); $t.prop('disabled', $t.data('disabled')); };
						$('input, button').each(restoreState);
					}
					else{
						$p.parent().show();
						$p.find('.spinner-text')[html?'html':'text'](text ? text : 'Processing... Please wait...');
						var saveStateAndDisable = function(){ $t = $(this); $t.data('disabled', $t.is(':disabled')); $t.prop('disabled', true); };
						$('input, button').each(saveStateAndDisable);
					}
				}
			};
			var edit = function(ev){
				var $t = $(this)
				var $ta = $('button.edit-steps');
				var tid = $t.attr('data-tour');
				if($t.attr('data-edit') !== 'true'){
					$ta.text('Discard Edits').attr('data-edit', 'true');
					var ths = $('#tour-'+tid+' th');
					var hws = [ '98%', ths.eq(1).css('width'), ths.eq(2).css('width'), ths.eq(3).width() - 20 ];
					hws[4] = hws[3] + 16 + 'px', hws[3] -= 90, hws[3] += 'px';
					$('#tour-'+tid+' span.t-val').each(function(ev){
						var $t = $(this), h = Math.max(30, $t.parent().height()-10);
						var w = $t.hasClass('ta-page') ? 1 : $t.hasClass('ta-title') ? 2 : $t.hasClass('t-previous-event') || $t.hasClass('t-next-event') ? 3 : $t.hasClass('ta-content') ? 4 : 0;
						w = hws[w];
						$t.hide().after('<input type="text" class="i-val border-box '+$t.attr('data-name')+'" value="'+($t.attr('data-empty')=='true'?'':$.trim($t.text()))+'" style="height:'+h+'px;width:'+w+'" placeholder="'+$t.attr('data-placeholder')+'">');
					});
					$('#tour-'+tid+' span.ta-val').each(function(ev){
						var $t = $(this), h = Math.max(60, $t.parent().height());
						var w = $t.hasClass('ta-page') || $t.hasClass('t-btn-title') ? 1 : $t.hasClass('ta-title') ? 2 : $t.hasClass('t-previous-event') || $t.hasClass('t-next-event') ? 3 : $t.hasClass('ta-content') ? 4 : 0;
						$t.parent().removeClass('error');
						$t.hide().after('<textarea class="ia-val border-box '+$t.attr('data-name')+'" style="height:'+h+'px;width:'+hws[w]+';" placeholder="'+$t.attr('data-placeholder')+'">'+$t.text()+'</textarea>');
					});
					$('#tour-'+tid+' .edit-hide').css('display','none');
					$('#tour-'+tid+' button.save-steps').prop('disabled', false);
					$('#tour-'+tid+' a.dropdown-toggle').each(function(ev){
						var $t = $(this);
						$t.removeClass('disabled');
						$t.data('oval', $t.html());
					});
				}
				else{
					$ta.text('Quick Edit').attr('data-edit', 'false');
					$('#tour-'+tid+' span.t-val').each(function(ev){
						$(this).show().siblings('input').remove();
					});
					$('#tour-'+tid+' span.ta-val').each(function(ev){
						$(this).show().siblings('textarea').remove();
					});
					$('#tour-'+tid+' .edit-hide').css('display','inline');
					$('#tour-'+tid+' button.save-steps').not($t).prop('disabled', true);
					$('#tour-'+tid+' a.dropdown-toggle').each(function(ev){
						var $t = $(this);
						$t.html($t.data('oval'));
						$t.addClass('disabled');
						$t.removeData('oval');
					});
				}
			};
			
			var handle404 = function(xhr, textStatus, error){
				$('#m-tour-error').modal('show').find('.tour-error-404').css('display', 'inline-block');
				$('#m-tour-error').find('.tour-error-500').css('display', 'none');
				display.processing();
			};
			var handle500 = function(xhr, textStatus, error){
				$('#m-tour-error').modal('show').find('.tour-error-500').css('display', 'inline-block');
				$('#m-tour-error').find('.tour-error-404').css('display', 'none');
				display.processing();
			};
			var api = {
				_getCSRFToken: function(){
					var cookies = document.cookie, key='EaToBu_csrf';
					if(!cookies || cookies.length < 1) return null;
					cookies = cookies.split(';');
					for(var c in cookies){
						cookies[c] = $.trim(cookies[c]);
						if(cookies[c].indexOf(key+'=') == 0)
							return cookies[c].replace(key+'=','');
					}
					return null;
				},
				_safePost: function(url, postData, callbackMap){
					//postData should always be passed in as an object. If passed as string urlencoding issues happen. So pass as object and let jQuery do the heavy lifting
					//append csrf token to postdata params
					var _csrf = api._getCSRFToken();
					if(_csrf){
						postData = postData ? postData : {};
						postData._csrf = _csrf;
					}

					$.extend(callbackMap, {420: function(xhr, textStatus, error){
						window.location.href = xhr.getResponseHeader('location') + window.location.pathname;
					}});

					display.processing();
					$.ajax({
						type: 'POST',
						url: url,
						dataType: 'json',
						data: postData,
						statusCode: callbackMap
					});
				},
				steps: function(tour, steps, callbackMap){
					api._safePost('/tour/edit', {tour: tour, steps: JSON.stringify(steps)}, callbackMap);
				},
				rename: function(tour, name, callbackMap){
					api._safePost('/tour/rename', {tour: tour, name: name}, callbackMap);
				},
				remove: function(tour, callbackMap){
					api._safePost('/tour/remove', {tour: tour, action: 'remove'}, callbackMap);
				}
			};
			var save = function(ev){
				var $t = $(this);
				var tid = $t.attr('data-tour');
				var $tab = $('#tour-'+tid);
				var $strs = $tab.find('tr:not(:first)'), steps = [], step, valid = true;
				$strs.each(function(i,e){
					if(valid){
						i = i%3;
						var $t = $(e);
						var isZeroLength = function(text){
							if(!text || text.length < 1){ valid = false; return true};
							return false;
						};
						if(i==0){
							step = {};
							var $e = $t.find('.ia-page');
							step.u = $e.val();
							if(isZeroLength(step.u)) return display.error($e, 'Cannot be empty');
							$e = $t.find('.ia-title');
							step.t = $e.val();
							if(isZeroLength(step.t)) return display.error($e, 'Cannot be empty');
							$e = $t.find('.ia-content');
							step.c = $e.val();
							if(isZeroLength(step.c)) return display.error($e, 'Cannot be empty');
							step.b = [];
						}
						else if(i==1){
							if(!step.b) return;
							var $b = $t.find('.b-previous-shown'), b = $.trim($b.text()).toLowerCase();
							var btn = {s: b == 'show' ? 1 : b == 'hide' ? 2 : 0}, $e;
							btn.t = $t.find('.i-previous-title').val();
							btn.e = $t.find('.i-previous-event').val();
							step.b.push(btn);
						}
						else if(i==2){
							if(!step.b) return;
							var $b = $t.find('.b-next-shown'), b = $.trim($b.text()).toLowerCase();
							var btn = {s: b == 'show' ? 1 : b == 'hide' ? 2 : 0}, $e;
							btn.t = $t.find('.i-next-title').val();
							btn.e = $t.find('.i-next-event').val();
							step.b.push(btn);
							steps.push(step);
						}
					}
				});
				console.log(steps);
				api.steps($t.attr('data-tour'), steps, {
					200: function(data, textStatus, xhr){
						var $tab = $('#tour-'+data.id);
						var $strs = $tab.find('tr:not(:first)');
						data.steps = JSON.parse(data.steps);
						$strs.each(function(i,e){
							var j = ~~(i/3), i = i%3;
							var step = data.steps[j];
							var $t = $(e);
							if(i==0){
								$t.find('.ta-page').text(step.u);
								$t.find('.ta-title').text(step.t);
								$t.find('.ta-content').text(step.c);
							}
							else if(i==1){
								var b = step.b[0], v = b.s == 1 ? 'Show' : b.s == 2 ? 'Hide' : 'Auto';
								$t.find('.b-previous-shown').text(v).data('oval', v);
								$t.find('.t-previous-title').text(b.t);
								$t.find('.t-previous-event').text(b.e);
							}
							else if(i==2){
								var b = step.b[1], v = b.s == 1 ? 'Show' : b.s == 2 ? 'Hide' : 'Auto';
								$t.find('.b-next-shown').text(v).data('oval', v);
								$t.find('.t-next-title').text(b.t);
								$t.find('.t-next-event').text(b.e);
							}
						});
						$tab.find('.edit-steps:first').trigger('click');
						display.processing();
						$tab.find('.save-steps').prop('disabled', true);
					},
					404: handle404,
					500: handle500
				});
			};
			$('#m-tour-rename').on('show', function(){
				$(this).find('.error').removeClass('error');
			}).on('shown', function(){
				$(this).find('input:first').focus().val($('ul.nav-pills li.active a').attr('data-tour-name'));	
			}).find('.form-horizontal').on('submit', function(ev){
				ev.preventDefault();
				$('#m-tour-rename').modal('hide');
				var $i = $('#i-tour-rename'), i = $.trim($i.val());
				if(i.length < 1)
					display.error($i, 'Tour Name');
				else{
					var $t = $('ul.nav-pills li.active a');
					api.rename($t.attr('data-tour'),i, {
						200: function(data, textStatus, xhr){
							$('#t-tour-'+data.id).text(data.name).attr('data-tour-name', data.name);
							$('#tour-'+data.id).find('.tour-name').text(data.name);
							display.processing();
						},
						404: handle404,
						500: handle500
					});
				}
			});
			$('#b-tour-del-ok').on('click', function(){
				var $t = $('ul.nav-pills li.active a');
				api.remove($t.attr('data-tour'), {
					200: function(data, textStatus, xhr){
						window.location.href = '/dash';
					},
					404: handle404,
					500: handle500
				});
			});
			$(window).ready(function(){
				initUI();
				var spinner = new Spinner({
					radius: 4, lines: 8, width: 4, length: 6, shadow: false, left: '10', top: '10'
				}).spin($('#processing')[0]);

				var $t = $('ul.nav-pills li.active a');
				tour = $t.attr('data-tour-name');
				document.title = (tour ? tour+' - ' : '')+'Easy Product Tours';
				history.replaceState({}, document.title, window.location.pathname);
			});
		</script>
	</body>
</html>