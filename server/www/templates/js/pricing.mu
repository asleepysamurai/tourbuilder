<script type="text/javascript">
	var changePrice = function(i, e, p, l){
		var j = i%3;
		var $e = $(e);
		if(j == 0){
			$e.text(p[0]+' USD/mo*');
			$e.siblings('a:eq(0)').attr('href', l[0]);
		}
		else if(j == 1){
			$e.text(p[1]+' USD/mo*');
			$e.siblings('a:eq(0)').attr('href', l[1]);
		}
		else if(j == 2){
			$e.text(p[2]+' USD/mo*');
			$e.siblings('a:eq(0)').attr('href', l[2]);
		}
	};
	$('#plan-duration button').on('click', function(ev){
		var $t = $(this), d = $t.attr('data-duration');
		if(d == 'm'){
			$('#pricing .plan h3').each(function(i,e){
				changePrice(i,e,['27','17','12'],['https://sites.fastspring.com/easyproducttours/instant/premier-monthly','https://sites.fastspring.com/easyproducttours/instant/regular-monthly','https://sites.fastspring.com/easyproducttours/instant/starter-monthly']);
			});
			$('#pricing-duration').text('a month');
			$('#plan-save-duration').text('a year');
			$('#plan-save').text('Save 36 USD');
		}
		else if(d == 'y'){
			$('#pricing .plan h3').each(function(i,e){
				changePrice(i,e,['24','14','9'],['https://sites.fastspring.com/easyproducttours/instant/premier-annually','https://sites.fastspring.com/easyproducttours/instant/regular-annually','https://sites.fastspring.com/easyproducttours/instant/starter-annually']);
			});
			$('#pricing-duration').text('one year');
			$('#plan-save-duration').text('two years');
			$('#plan-save').text('Save 120 USD');
		}
		else if(d == '2y'){
			$('#pricing .plan h3').each(function(i,e){
				changePrice(i,e,['22','12','7'],['https://sites.fastspring.com/easyproducttours/instant/premier-biennielly','https://sites.fastspring.com/easyproducttours/instant/regular-biennielly','https://sites.fastspring.com/easyproducttours/instant/starter-biennielly']);
			});
			$('#pricing-duration').text('two years');
			$('#plan-save-duration').text('two years');
			$('#plan-save').text('Save 120 USD');
		}
	});
</script>