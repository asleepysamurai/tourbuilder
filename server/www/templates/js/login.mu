<script type="text/javascript" src="/js/mailcheck.min.js"></script>
<script type="text/javascript" src="/js/spin.min.js"></script>
<script type="text/javascript">
	var display = {
		_display: function(text, eps, sls, html, _class){
			var sls = sls.substr(0,1);
			if(text){
				$('#help-'+eps+'-'+sls)[html?'html':'text'](text);
				$('#cg-'+eps+'-'+sls).removeClass('error').removeClass('warning').addClass(_class);
			}
			else{	
				$('#help-'+eps+'-'+sls).text('');
				$('#cg-'+eps+'-'+sls).removeClass('error').removeClass('warning');
			}
		},
		warn: function(text, eps, sls, html){
			display._display(text, eps, sls, html, 'warning');
		},
		error: function(text, eps, sls, html){
			display._display(text, eps, sls, html, 'error');
		},
		processing: function(){
			var $p = $('#processing');
			if($p.is(':visible')){
				$p.hide();
				$('input').prop('disabled', false);
				$('button').prop('disabled', false);
				$('#a-suggestion').on('click', mailcheckClick);
			}
			else{
				$p.show();
				$('input').prop('disabled', true);
				$('button').prop('disabled', true);
				$('#a-suggestion').off('click');
			}
		}
	};
	var validate = {
		email: function(email){
			var eatpos = email.indexOf('@');
			if(email.length > 4 && eatpos != -1 && eatpos > 0 && eatpos < email.length-1 && email.split('@').length == 2) return true;
			return false;
		},
		password: function(pass){
			if(pass.length > 5) return true;
			return false;
		}
	};
	var api = {
		_safePost: function(url, postData, callbackMap){
			//postData should always be passed in as an object. If passed as string urlencoding issues happen. So pass as object and let jQuery do the heavy lifting
			//append csrf token to postdata params
			$.extend(callbackMap, {420: function(xhr, textStatus, error){
				console.log('420: Possible CSRF attack detected. The server dropped this request.');
			}});

			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'json',
				data: postData,
				statusCode: callbackMap
			});
		},
		login: function(email, pass, callbackMap){
			api._safePost('/login', {e: email, p: pass, v: 'a'}, callbackMap);
		}
	};
	var handleLogin = function(){
		var $e = $('#i-email-l'), $p = $('#i-password-l');
		var e = $e.val(), p = $p.val();
		if(!validate.email($e.val())) return display.error('Invalid email.', 'email', 'login'), $e.focus();
		if(!validate.password($p.val())) return display.error('Minimum 6 characters.', 'password', 'login'), $p.focus();
		display.processing();
		api.login(e, p, {
			200: function(data, textStatus, xhr){
				var next = $.trim($('#i-next').val());
				if(next.length <= 0)
					next = '/dash';
				window.location.href = next;
			},
			201: function(data, textStatus, xhr){
				window.location.href = '/user/activate'
			},
			400: function(xhr, textStatus, error){
				display.processing();
				$('#processing').hide();
				if(xhr.responseText.indexOf('email'))
					return display.error('Invalid email.', 'email', sls), $e.focus();
				else if(xhr.responseText.indexOf('password'))
					return display.error('Minimum 6 characters.', 'password', sls), $p.focus();
			},
			401: function(xhr, textStatus, error){
				display.error('Wrong password. <a href="/resetpassword">Forgot password?</a>', 'password', 'login', true);
				display.processing();
			},
			404: function(xhr, textStatus, error){
				display.error('Email not registered. <a href="/signup">Create new account?</a>', 'email', 'login', true);
				display.processing();						
			},
			500: function(xhr, textStatus, error){
				//Server Error
				display.error('An error occurred while trying to process this request. Please try again later.', 'email', 'login');
				display.processing();
			}
		});
	};
	var mailcheckClick = function(){
		$('#i-email-l').val($(this).text());
		display.warn(null, 'email', 'login');
	};
	$(window).ready(function(){
		$('input').eq(0).focus();
		var spinner = new Spinner({
			radius: 4, lines: 8, width: 4, length: 6, shadow: false, left: '10', top: '10'
		}).spin($('#processing')[0]);

		var $forms = $('div .form-horizontal');
		$forms.on('submit', function(ev){
			ev.preventDefault();
			handleLogin();
		});

		$forms.find('.email').on('change', function(ev){
			var $t = $(this);
			$t.mailcheck({suggested: function(element, suggestion){
				display.error(null, 'password', 'login');
				display.warn('Did you mean <a href="javascript:void(0)" id="a-suggestion">'+suggestion.full+'</a>', 'email', 'login', true);
				$('#a-suggestion').on('click', mailcheckClick);
			}, empty: function(element){
				display.warn(null, 'email', 'login');
			}});
		});
		$forms.find('input').on('keyup', function(ev){
			if([13,27,37,38,39,40].indexOf(ev.which) == -1){
				var $t = $(this);
				display.error(null, $t.hasClass('email') ? 'email' : 'password', 'login');
			}
		});
	});
</script>