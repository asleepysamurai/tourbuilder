<script type="text/javascript" src="/js/mailcheck.min.js"></script>
<script type="text/javascript" src="/js/spin.min.js"></script>
<script type="text/javascript">
	var displayNotice = function(text, _class, html){
		if(text){
			$('#help-email')[html?'html':'text'](text);
			$('#cg-email').removeClass('error').removeClass('warning').addClass(_class);
		}
		else{
			$('#help-email').text('');
			$('#cg-email').removeClass('error').removeClass('warning');
		}
	}
	var displaySpinner = function(){
		var $p = $('#processing');
		if($p.is(':visible')){
			$p.hide();
			$('input').prop('disabled', false);
			$('button').prop('disabled', false);
			$('#a-suggestion').on('click', mailcheckClick);
		}
		else{
			$p.show();
			$('input').prop('disabled', true);
			$('button').prop('disabled', true);
			$('#a-suggestion').off('click');
		}
	}
	var validateEmail = function(email){
		var eatpos = email.indexOf('@');
		if(email.length > 4 && eatpos != -1 && eatpos > 0 && eatpos < email.length-1 && email.split('@').length == 2) return true;
		return false;
	}
	var handleVerification = function(){
		var $e = $('#i-email'), email = $.trim($e.val());
		if(!validateEmail(email))
			return displayNotice('Invalid email.', 'error'), $e.focus();

		displaySpinner();
		$.ajax({
			type: 'POST',
			url: '/resetpassword',
			dataType: 'json',
			data: {e: email},
			statusCode: {
				200: function(data, textStatus, xhr){
					displaySpinner();
					$('#action-msg').show();
				},
				404: function(xhr, textStatus, error){
					displaySpinner();
					return displayNotice('This email is not registered.', 'error');
				},
				500: function(xhr, textStatus, error){
					displaySpinner();
					return displayNotice('An error occurred while trying to reset your password. Please try again later.', 'error');
				}
			}
		});
	};
	var mailcheckClick = function(){
		$('#i-email').val($(this).text());
		displayNotice();
	};
	$(window).ready(function(){
		$('input').eq(0).focus();
		var spinner = new Spinner({
			radius: 4, lines: 8, width: 4, length: 6, shadow: false, left: '10', top: '10'
		}).spin($('#processing')[0]);

		var $forms = $('div .form-horizontal');
		$forms.on('submit', function(ev){
			ev.preventDefault();
			handleVerification();
		});

		$forms.find('input').on('keyup', function(ev){
			if([13,27,37,38,39,40].indexOf(ev.which) == -1){
				displayNotice();
				var $t = $(this);
				$t.mailcheck({suggested: function(element, suggestion){
					displayNotice();
					displayNotice('Did you mean <a href="javascript:void(0)" id="a-suggestion">'+suggestion.full+'</a>', 'warning', true);
					$('#a-suggestion').on('click', mailcheckClick);
				}, empty: function(element){
					displayNotice();
				}});
			}
		});
	});
</script>