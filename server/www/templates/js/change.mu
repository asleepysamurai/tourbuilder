<script type="text/javascript" src="/js/spin.min.js"></script>
<script type="text/javascript">
	var display = {
		error: function(text, eps, sls, html){
			var sls = sls.substr(0,1);
			if(text){
				$('#help-'+eps+'-'+sls)[html?'html':'text'](text);
				$('#cg-'+eps+'-'+sls).removeClass('error').removeClass('warning').addClass('error');
			}
			else{	
				$('#help-'+eps+'-'+sls).text('');
				$('#cg-'+eps+'-'+sls).removeClass('error').removeClass('warning');
			}
		},
		notice: function(msg, html){
			$('#action-msg')[html?'html':'text'](msg).show();
		},
		processing: function(){
			var $p = $('#processing');
			if($p.is(':visible')){
				$p.hide();
				$('input').prop('disabled', false);
				$('button').prop('disabled', false);
			}
			else{
				$p.show();
				$('input').prop('disabled', true);
				$('button').prop('disabled', true);
			}
		}
	};
	var handleChange = function(){
		var $e = $('#i-password-1'), $p = $('#i-password-2');
		var e = $e.val(), p = $p.val(), c = $('#i-code').val();
		if(e.length < 5) return display.error('Minimum 6 characters.', 'password', '1'), $e.focus();
		if(p.length < 5) return display.error('Minimum 6 characters.', 'password', '2'), $p.focus();
		if(e !== p) return display.error('Passwords do not match.', 'password', '1'), $e.focus();
			
		display.processing();
		$.ajax({
			type: 'POST',
			url: '/user/changepassword',
			dataType: 'json',
			data: {c: c, p: p},
			statusCode: {
				200: function(data, textStatus, xhr){
					display.processing();
					return display.notice('Password changed successfully. You can now <a href="/login">login</a> with your new password.', true);
				},
				401: function(xhr, textStatus, error){
					display.processing();
					return display.notice('Invalid reset code or reset code expired. <a href="/resetpassword">Resend Password Reset Code?</a>', true);
				},
				500: function(xhr, textStatus, error){
					display.processing();
					return display.notice('An error occurred while trying to change your password. Please try again later.');
				}
			}
		});
	};
	$(window).ready(function(){
		$('input').eq(0).focus();
		var spinner = new Spinner({
			radius: 4, lines: 8, width: 4, length: 6, shadow: false, left: '10', top: '10'
		}).spin($('#processing')[0]);

		var $forms = $('div .form-horizontal');
		$forms.on('submit', function(ev){
			ev.preventDefault();
			handleChange();
		});

		$forms.find('input').on('keydown', function(ev){
			if([13,27,37,38,39,40].indexOf(ev.which) == -1){
				var $t = $(this);
				display.error(null, 'password', $t.hasClass('1') ? '1' : '2');
			}
		});
	});
</script>