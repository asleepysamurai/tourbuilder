<script type="text/javascript" src="/js/spin.min.js"></script>
<script type="text/javascript">
	var displayNotice = function(text, _class, html){
		if(text){
			$('#help-code')[html?'html':'text'](text);
			$('#cg-code').removeClass('error').removeClass('warning').addClass(_class);
		}
		else{
			$('#help-code').text('');
			$('#cg-code').removeClass('error').removeClass('warning');
		}
	}
	var displaySpinner = function(){
		var $p = $('#processing');
		if($p.is(':visible')){
			$p.hide();
			$('input').prop('disabled', false);
			$('button').prop('disabled', false);
		}
		else{
			$p.show();
			$('input').prop('disabled', true);
			$('button').prop('disabled', true);
		}
	}
	var handleVerification = function(){
		var code = $.trim($('#i-code').val());
		if(code.length < 6)
			return displayNotice('Invalid activation code. Please copy-paste the activation code from the email you received.', 'error');

		displaySpinner();
		$.ajax({
			type: 'POST',
			url: '/user/activate',
			dataType: 'json',
			data: {c: code},
			statusCode: {
				200: function(data, textStatus, xhr){
					window.location.href = '/user/changepassword?c='+code;
				},
				401: function(xhr, textStatus, error){
					displaySpinner();
					return displayNotice('Invalid activation code. Please copy-paste the activation code from the email you received.<br><a href="/resendactivation">Resend Activation Code?</a>', 'error', true);
				},
				409: function(xhr, textStatus, error){
					displaySpinner();
					return displayNotice('Activation code expired.<br><a href="/resendactivation">Resend Activation Code?</a>', 'error', true);
				},
				500: function(xhr, textStatus, error){
					displaySpinner();
					return displayNotice('An error occurred while trying to activate your account. Please try again later.', 'error');
				}
			}
		});
	};

	$(window).ready(function(){
		$('input').eq(0).focus();
		var spinner = new Spinner({
			radius: 4, lines: 8, width: 4, length: 6, shadow: false, left: '10', top: '10'
		}).spin($('#processing')[0]);

		var $forms = $('div .form-horizontal');
		$forms.on('submit', function(ev){
			ev.preventDefault();
			handleVerification();
		});

		$forms.find('input').on('keyup', function(ev){
			if([13,27,37,38,39,40].indexOf(ev.which) == -1)
				displayNotice();
		});
	});
</script>