<script type="text/javascript" src="/js/mailcheck.min.js"></script>
<script type="text/javascript" src="/js/spin.min.js"></script>
<script type="text/javascript">
	var display = {
		_display: function(text, eps, sls, html, _class){
			var sls = sls.substr(0,1);
			if(text){
				$('#help-'+eps+'-'+sls)[html?'html':'text'](text);
				$('#cg-'+eps+'-'+sls).removeClass('error').removeClass('warning').addClass(_class);
			}
			else{	
				$('#help-'+eps+'-'+sls).text('');
				$('#cg-'+eps+'-'+sls).removeClass('error').removeClass('warning');
			}
		},
		warn: function(text, eps, sls, html){
			display._display(text, eps, sls, html, 'warning');
		},
		error: function(text, eps, sls, html){
			display._display(text, eps, sls, html, 'error');
		},
		processing: function(){
			var $p = $('#processing');
			if($p.is(':visible')){
				$p.hide();
				$('input').prop('disabled', false);
				$('button').prop('disabled', false);
				$('#a-suggestion').off('click');
			}
			else{						
				$p.show();
				$('input').prop('disabled', true);
				$('button').prop('disabled', true);
				$('#a-suggestion').on('click', mailcheckClick);
			}
		}
	};
	var validate = {
		email: function(email){
			var eatpos = email.indexOf('@');
			if(email.length > 4 && eatpos != -1 && eatpos > 0 && eatpos < email.length-1 && email.split('@').length == 2) return true;
			return false;
		},
		password: function(pass){
			if(pass.length > 5) return true;
			return false;
		}
	};
	var api = {
		_getCSRFToken: function(){
			var cookies = document.cookie, key='EaToBu_csrf';
			if(!cookies || cookies.length < 1) return null;
			cookies = cookies.split(';');
			for(var c in cookies){
				cookies[c] = cookies[c].trim();
				if(cookies[c].indexOf(key+'=') == 0)
					return cookies[c].replace(key+'=','');
			}
			return null;
		},
		_safePost: function(url, postData, callbackMap){
			//postData should always be passed in as an object. If passed as string urlencoding issues happen. So pass as object and let jQuery do the heavy lifting
			//append csrf token to postdata params
			$.extend(callbackMap, {420: function(xhr, textStatus, error){
				console.log('420: Possible CSRF attack detected. The server dropped this request.');
			}});

			var _csrf = api._getCSRFToken();
			if(_csrf){
				if(!postData)
					postData = {};
				$.extend(postData, {'_csrf': _csrf});
			}

			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'json',
				data: postData,
				statusCode: callbackMap
			});
		},
		signup: function(email, pass, callbackMap){
			api._safePost('/signup', {e: email, p: pass}, callbackMap);
		}
	};
	var handleSignup = function(){
		var $e = $('#i-email-s'), $p = $('#i-password-s');
		var e = $e.val(), p = $p.val();
		if(!validate.email(e)) return display.error('Invalid email.', 'email', 'signup'), $e.focus();
		if(!validate.password(p)) return display.error('Minimum 6 characters.', 'password', 'signup'), $p.focus();
		display.processing();
		api.signup(e, p, {
			200: function(data, textStatus, xhr){
				window.location.href = '/dash';
			},
			201: function(data, textStatus, xhr){
				//Signup OK. Account created. Redirect to verification code page.
				window.location.href = '/user/activate';
			},
			400: function(xhr, textStatus, error){
				display.processing();
				$('#processing').hide();
				if(xhr.responseText.indexOf('email'))
					return display.error('Invalid email.', 'email', sls), $e.focus();
				else if(xhr.responseText.indexOf('password'))
					return display.error('Minimum 6 characters.', 'password', sls), $p.focus();
			},
			401: function(xhr, textStatus, error){
				//Already exists - bad password
				display.error('Email already registered. <a href="/resetpassword">Forgot password?</a>', 'email', 'signup', true);
				display.processing();
			},
			500: function(xhr, textStatus, error){
				//Server Error
				display.error('An error occurred while trying to process this request. Please try again later.', 'email', 'signup');
				display.processing();
			}
		});
	};
	var mailcheckClick = function(ev){
		$('#i-email-s').val($(this).text());
		display.warn(null, 'email', 'signup');
	};
	$(window).ready(function(){
		$('input').eq(0).focus();
		var spinner = new Spinner({
			radius: 4, lines: 8, width: 4, length: 6, shadow: false, left: '10', top: '10'
		}).spin($('#processing')[0]);

		var $forms = $('div .form-horizontal');
		$forms.on('submit', function(ev){
			ev.preventDefault();
			handleSignup();
		});

		$forms.find('.email').on('change', function(ev){
			var $t = $(this);
			$t.mailcheck({suggested: function(element, suggestion){
				display.error(null, 'password', 'signup');
				display.warn('Did you mean <a href="javascript:void(0)" id="a-suggestion">'+suggestion.full+'</a>', 'email', 'signup', true);
				$('#a-suggestion').on('click', mailcheckClick);
			}, empty: function(element){
				display.warn(null, 'email', 'signup');
			}});
		});
		$forms.find('input').on('keyup', function(ev){
			if([13,27,37,38,39,40].indexOf(ev.which) == -1){
				var $t = $(this);
				display.error(null, $t.hasClass('email') ? 'email' : 'password', 'signup');
			}
		});
	});
</script>