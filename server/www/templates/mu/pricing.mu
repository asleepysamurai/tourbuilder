
<div id="pricing" class="page">
	<div class="row-fluid container">
		<div class="span12" style="text-align:center;">
			<h2>Free 14 Day Trial on All Plans</h2>
			<p>No charges during trial. Cancel anytime. Pro-rated refunds on all cancellations.</p>
		</div>
	</div>
	<div id="signup-steps" class="row-fluid container">
		<div class="span12" style="text-align:center;width:100%">
			<div class="signup-step active">1. Select a Plan</div>
			<div class="signup-step">2. Payment Details</div>
			<div class="signup-step">3. Account Details</div>
		</div>
	</div>
	<br>
	<div class="row-fluid container">
		<div class="span12" style="text-align:center; padding-bottom: 20px;">
			<div id="plan-duration" class="btn-group" data-toggle="buttons-radio">
				<button class="btn" data-duration="m">Per Month</button>
				<button class="btn active" data-duration="y">Per Year</button>
				<button class="btn" data-duration="2y">For Two Years</button>
			</div>
		</div>
	</div>
	<div class="row-fluid container">
		<div class="span12" style="text-align:center; padding-bottom: 20px;">
			<p><big>Buy upfront for <span id="plan-save-duration">two years</span>. <big id="plan-save">Save 120 USD</big>.</big></p>
		</div>
	</div>
	<div class="row-fluid container" id="plans">
		<div class="span12" style="text-align:center; padding-bottom: 20px;">
			All plans come with unlimited tours, unlimited user-tracking, custom theme support and SSL support.
		</div>
		<div class="span12" style="margin:0 2%;">
			<div class="plan" id="plan1" style="border-right:0px;margin-top:20px">
				<h3>24 USD/mo*</h3><h4>Premier</h4>
				1M Views/Month<br><br>
				<a href="https://sites.fastspring.com/easyproducttours/instant/premier-annually" class="btn btn-primary">Buy Now</a><br><br>
			</div>
			<div class="plan" id="plan2" style="font-size: 140%; padding-bottom: 25px; z-index: 10;">
				<div><button class="btn btn-small btn-danger" style="float:right">Most Popular</button></div>
				<div class="clearfix"></div>
				<h3 style="font-size: 140%;">14 USD/mo*</h3><h4 style="font-size: 100%;">Regular</h4>
				100,000 Views/Month<br><br>
				<a href="https://sites.fastspring.com/easyproducttours/instant/regular-annually" class="btn btn-success btn-large">Buy Now</a><br>
			</div>
			<div class="plan" id="plan3" style="border-left: 1px;margin-top:20px">
				<h3>9 USD/mo*</h3><h4>Starter</h4>
				10,000 Views/Month<br><br>
				<a href="https://sites.fastspring.com/easyproducttours/instant/starter-annually" class="btn btn-primary">Buy Now</a><br><br>
			</div>
		</div>
	</div>
	<div class="row-fluid container">
		<div class="span12" style="text-align:center;">
			<br>*<small>Prices applicable when plan purchased upfront for <span id="pricing-duration">one year</span>.</small>
			<small>Valid credit card required for signup.</small>
		</div>
	</div>
	<div class="row-fluid container">
		<div class="span12" style="text-align:center;">
			<p>Have different needs? Write to us at <a href="mailto:sales@easyproducttours.com">sales@easyproducttours.com</a>.</p>
		</div>
	</div>
	<br>
	<div class="well nomargin">
		<div class="row-fluid container">
			<div class="span12">
				<h3 style="text-align:left">Pricing FAQ:</h3><br>
				<ol>
					<li>
						<p><span class="question">What is a tour?</span><br>
							A tour is each unique tour you wish to run on a particular site. Each site can have multiple tours. Tours can span multiple pages. You can also create multiple tours for a single page.
						</p>
					</li>
					<li>
						<p><span class="question">Who are users?</span><br>
						Easy Product Tours tracks your site's users in order to remember which users have seen the tour already and to determine whether or not to run the tour again. Thus each user of your site will count as a unique user in Easy Product Tours.</p>
					</li>
					<li>
						<p><span class="question">Is a free trial available?</span><br>
						Yes. All plans come with a free fourteen day trial. You will not be charged for the first fourteen days.</p>
					</li>
					<li>
						<p><span class="question">Will I be charged if I cancel within the trial period?</span><br>
						No, you will not be charged if you cancel before your trial period ends.</p>
					</li>
					<li>
						<p><span class="question">What hapens when my free trial ends?</span><br>
						After the end of your free trial, if you have not yet cancelled, you will be billed for the next month/year/2 years of usage, depending on the plan you signed up for. You will never be charged for the first fourteen days.</p>
					</li>
					<li>
						<p><span class="question">When can I cancel my plan?</span><br>
						You can cancel your plan at anytime during or after the trial.</p>
					</li>
					<li>
						<p><span class="question">What happens when I cancel my plan?</span><br>
						If you cancel your plan within the trial period, you will not be charged anything. If you cancel after your trial period has ended, you will be charged for the number of months you used Easy Product Tours and a pro-rated refund will be made.</p>
					</li>
					<li>
						<p><span class="question">What happens when I reach the end of my plan duration?</span><br>
						When you reach the end of your plan duration, you will be automatically charged for the next month/year/2 years depending on the plan you signed up for (provided, you have not cancelled your plan before then).</p>
					</li>
					<li>
						<p><span class="question">What happens when I reach/exceed the pageview limit for my current plan?</span><br>
						When the pageview limit is reached for your plan, you will be sent an email asking you to upgrade to a higher plan. In case this is a temporary surge in traffic and you expect your pageview count to go back to normal levels, you can continue on your current plan. However if your traffic for the next month still exceeds the pageview count for your plan, you will be asked to upgrade again. If you do not upgrade within thirty days of receiving that email, then your account will be deactivated.
						</p>
					</li>
					<li>
						<p><span class="question">What happens if I upgrade/downgrade mid-cycle?</span><br>
						You will be charged/refunded the difference between your old plan and your new plan, pro-rated for the remainder of your billing cycle.</p>
					</li>
					<li>
						<p><span class="question">What happens if you increase your prices in future?</span><br>
						Whenever we increase our prices, you will still be able to use Easy Product Tours at the old plan rates. During the next billing cycle, you will be charged at the new rates.</p>
					</li>
					<li>
						<p><span class="question">Which plan offers the best value?</span><br>
						This really depends on your needs. You can write to us at <a href="mailto:sales@easyproducttours.com">sales@easyproducttours.com</a> to dicuss your specific needs.</p>
						<p>However, whichever plan you choose you will obtain a better value for your money by choosing a longer billing cycle (annually/biennielly) as opposed to a shorter billing cycle (monthly). This is because we offer discounted rates for longer billing cycles. In addition, if we increase prices in the future, you can enjoy the older prices for a longer time, if you chose a longer billing cycle.</p>
					</li>
					<li>
						<p><span class="question">I'm convinced. How do I signup?</span><br>
						<a href="#signup-steps">Select a plan</a>, and click 'Buy Now'. </p>
				</ol>
			</div>
		</div>
	</div>
</div>