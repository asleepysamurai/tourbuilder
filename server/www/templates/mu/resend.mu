<div id="resendverification" class="page container">
	<div class="row-fluid">
		<div class="span12 form-resendverification">
			<form class="form-horizontal" novalidate="novalidate">
				<div class="control-group">
					<h3 class="form-header control-label">Resend Activation</h3>
				</div>
				<div class="control-group"  id="cg-email">
					<label class="control-label" for="i-email">Email</label>
					<div class="controls">
						<input type="email" id="i-email" class="email" autocorrect="off" autocapitalize="off">
						<small><span class="help-inline" id="help-email">Enter the email with which you signed up.</span></small>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn btn-success">Resend</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div id="processing">
				<span class="spinner-text">Processing... Please wait... </span>
			</div>
		</div>
	</div>
</div>