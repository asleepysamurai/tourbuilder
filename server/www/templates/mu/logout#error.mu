<div id="logout" class="page container">
	<div class="row-fluid">
		<div class="span12">
			<h2>An error occurred while trying to log you out :(</h2>
			<p class="lead">You might or might not have been logged out completely. Please try again in sometime.</p>
		</div>
	</div>
</div>