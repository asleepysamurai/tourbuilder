<div id="signup" class="page container">
	<div class="row-fluid">
		<div class="span6">
		</div>
		<div class="span6 form-signup">
			<form class="form-horizontal" id="signup" novalidate="novalidate">
				<div class="control-group">
					<h3 class="form-header control-label">Signup</h3>
				</div>
				<div class="control-group" id="cg-email-s">
					<label class="control-label" for="i-email-s">Email</label>
					<div class="controls">
						<input type="email" id="i-email-s" placeholder="Email" class="email signup" autocorrect="off" autocapitalize="off">
						<small><span class="help-inline" id="help-email-s"></span></small>
					</div>
				</div>
				<div class="control-group" id="cg-password-s">
					<label class="control-label" for="i-password-s">Password</label>
					<div class="controls">
						<input type="password" id="i-password-s" placeholder="Password" class="password signup" autocorrect="off" autocapitalize="off">
						<small><span class="help-inline" id="help-password-s"></span></small>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn btn-success">Sign up</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div id="processing">
				<span class="spinner-text">Processing... Please wait... </span>
			</div>
		</div>
	</div>
</div>