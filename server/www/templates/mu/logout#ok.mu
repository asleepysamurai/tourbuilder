<div id="logout" class="page container">
	<div class="row-fluid">
		<div class="span12">
			<h2>You have been logged out.</h2>
			<p class="lead">Please come back again, soon! We'll miss you :)</p>
			<a href="/login" class="btn btn-success btn-large">Login Again</a>
		</div>
	</div>
</div>