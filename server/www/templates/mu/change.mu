<div id="changepassword" class="page container">
	<div class="row-fluid">
		<div class="span12 form-changepassword">
			<form class="form-horizontal" id="login">
				<div class="control-group">
					<h3 class="form-header control-label">Change Password</h3>
				</div>
				<div class="control-group" id="cg-password-1">
					<label class="control-label" for="i-password-1">Password</label>
					<div class="controls">
						<input type="password" id="i-password-1" placeholder="Password" class="password 1" autocorrect="off" autocapitalize="off">
						<small><span class="help-inline" id="help-password-1"></span></small>
					</div>
				</div>
				<div class="control-group" id="cg-password-2">
					<label class="control-label" for="i-password-2">Password</label>
					<div class="controls">
						<input type="password" id="i-password-2" placeholder="Password" class="password 2" autocorrect="off" autocapitalize="off">
						<small><span class="help-inline" id="help-password-2"></span></small>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="hidden" id="i-code" value="{{code}}">
						<button type="submit" class="btn btn-success">Change</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div id="action-msg">				
			</div>
			<div id="processing">
				<span class="spinner-text">Processing... Please wait... </span>
			</div>
		</div>
	</div>
</div>