<h3 style="text-align:left;" class="well">Delete a Tour Step</h3>
<br>
<p>
	Before deleting a step, ask yourself if you are really sure about deleting the step. This action cannot be undone.
</p>
<br>
<h4>To Delete a Step</h4>
<ol>
	<li>Navigate to the URL on which the step you wish to delete exists.</li>
	<li><a href="/docs/build/launch-editor" target="_blank">Launch</a> the EPT Tour Editor.</li>
	<li>If you are not automatically logged in, then click the 'Login' button, and fill in and submit the login form.</li>
	<li>Once logged in, the tour you were editing should be automatically loaded. If it doesnt load automatically, click the 'Switch Tour' button and choose the tour.</li>
	<li>Once the tour is loaded, click the 'Switch Step' button and select the step which you wish to delete. That step should be loaded.</li>
	<li>Click the 'Delete' button on the right side of the toolbar (next to the 'New Step' button). The step should be deleted and the next step will be loaded.</li>
</ol>
<p>
	You have successfully deleted a step from your tour.
</p>