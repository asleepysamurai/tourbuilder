<h3 style="text-align:left;" class="well">Showing the Previous Step in a Tour</h3>
<br>
<p>You can programatically show the previous step in a tour, without waiting for the user to click the 'Previous' button. To do this call the <code class="prettyprint">previous</code> method as below:</p>
<pre class="prettyprint">eptPlayer.previous();</pre>
<p>This method does not take any arguments.</p>
<p>When this method is called, the currently displayed step is unloaded and the previous step in the sequence is displayed.</p>
<p>If the previous step is on a different page, the browser is redirected to that page.</p>