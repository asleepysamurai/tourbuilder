<h3 style="text-align:left;" class="well">Delete an Existing Tour</h3>
<br>
<p>
	Before deleting a tour, ask yourself if you are really sure about deleting the tour. This is not an action which can be undone, and deleting a tour will also delete all data associated with it including all the steps for that tour. Once a tour is deleted, it cannot be played on your site.
</p>
<br>
<h4>Via the EPT Tour Editor</h4>
<p>	
	<ol>
		<li>Navigate to the page on which the tour you wish to delete runs.</li>
		<li><a href="/docs/build/launch-editor" target="_blank">Launch</a> the EPT Tour Editor.</li>
		<li>If you are not automatically logged in, then click the 'Login' button, and fill in and submit the login form.</li>
		<li>Once logged in, click the 'Switch Tour' button, and select the tour you wish to delete from the drop-down box.</li>
		<li>A modal dialog should popup, asking you to confirm if you want to delete the tour.</li>
		<li>Click the 'Delete' button, to delete the tour.</li>
		(or)
		<li>If you decide against deleting the tour, simply click the 'Cancel' button.</li>
	</ol>
</p>
<br>
<h4>Via the EPT Dashboard</h4>
<p>
	<ol>
		<li><a href="/login" target="_blank">Login</a> to your EPT Dashboard.</li>
		<li>Click the 'Edit' link corresponding to the tour which you wish to delete.</li>
		<li>Click any of the two 'Delete Tour' buttons on the tour page.</li>
		<li>A modal dialog should popup, asking you to confirm if you really wish to delete the tour.</li>
		<li>Click the 'Delete Tour' button, to delete the tour.</li>
		(or)
		<li>If you decide against deleting the tour, simply click the 'Don't Delete' button.</li>
	</ol>
</p>
<br>
<p>
	<strong>NOTE:</strong> In case you accidentally deleted a tour, you can write to us at <a href="mailto:support@easyproducttours.com">support@easyproducttours.com</a>, and we will try to recover it for you. However, we cannot guarantee that your tour data can be recovered, so it is best to tread carefully while deleting tours.
</p>