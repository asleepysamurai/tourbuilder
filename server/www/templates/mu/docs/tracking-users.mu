<h3 style="text-align:left;" class="well">Tracking Users and Tours Viewed</h3>
<br>
<p>Showing the same tour to the same user again and again does not serve any purpose. In addition it detracts from the experience of using your site. Hence, in order to only show the tour to users who have not already seen it, Easy Product Tours (EPT) has a built-in user-tracking system.</p>
<p>In order to utilize this user tracking system, when the tour is <a href="/docs/play/load-tour" target="_blank">loaded</a>, the <code class="prettyprint">unique_userid</code> must be set to a value that is neither null nor false. The possible values it can have are:</p>
<p>
	When the <code class="prettyprint">unique_userid</code> is either null, undefined, or a boolean value with value false, user tracking is completely disabled. When the user visits your site again, the tour will be shown irrespective of whether it has already been seen or not.
</p>
<p>
	When the <code class="prettyprint">unique_userid</code> is a boolean value, and is true, user tracking is performed via client-side mechanisms. The tour progress state of the user is stored in the client-side on the user's browser. Thus if the user clears their browsing data or visits using another computer or browser, the tour will be displayed to them even if they have already seen it.
</p>
<p>
	When the <code class="prettyprint">unique_userid</code> is a string, the user progress tracking is done on the server side and the state of the user is stored in the EPT databases. Hence when the user visits your site again, no matter where they access your site from, as long as their user-id remains the same, they will not be shown repeated tours. This is the recommended way of tracking your user's tour progress.
</p>