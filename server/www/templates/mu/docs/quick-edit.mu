<h3 style="text-align:left;" class="well">Quick Editing an Entire Tour</h3>
<br>
<p>
	At times, you may want to make a small change in the wording of your tour, or a particular word which occurs in many steps might need to be changed to another word, or you might want to change the event triggered by multiple buttons. 
</p>
<p>
	In such cases, loading the EPT Tour Editor, navigating to each step and making the change would be a little tedious. In order to facilitate easy, quick changes to multiple steps, EPT provides a 'Quick Edit' function for each tour.
</p>
<br>
<h4>To Quick Edit a Tour</h4>
<ol>
	<li><a href="/login" target="_blank">Login</a> to your EPT Dashboard.</li>
	<li>Click the 'Edit' link corresponding to the tour which you wish to edit.</li>
	<li>Once, the tour page is loaded, click either of the two 'Quick Edit' buttons. The tour page should now change to edit mode.</li>
	<li>Make the changes you require.</li>
	<li>Click the 'Save Changes' button to save the changes you just made to the tour.</li>
	(or)
	<li>In case you decide against saving the changes, click the 'Discard Edits' button to reset the tour back to the original state.</li>
</ol>
