<h3 style="text-align:left;" class="well">Add a Tour Step</h3>
<br>
<h4>To Add a New Step</h4>
<ol>
	<li>Navigate to the URL on which you wish to place the new step of your tour.</li>
	<li><a href="/docs/build/launch-editor" target="_blank">Launch</a> the EPT Tour Editor.</li>
	<li>If you are not automatically logged in, then click the 'Login' button, and fill in and submit the login form.</li>
	<li>Once logged in, the tour you were editing should be automatically loaded. If it doesnt load automatically, click the 'Switch Tour' button and choose the tour.</li>
	<li>Once the tour is loaded, click the 'New Step' button to add a new step. <a href="/docs/build/edit-step" target="_blank">Edit</a> the step as per your needs.</li>
</ol>
<p>
	You have successfully added a new step to your tour.
</p>