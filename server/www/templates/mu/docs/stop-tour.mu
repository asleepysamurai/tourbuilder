<h3 style="text-align:left;" class="well">Stopping a Tour Being Played</h3>
<br>
<p>In order to stop the currently playing tour call the <code class="prettyprint">stop</code> method as below:</p>
<pre class="prettyprint">eptPlayer.stop();</pre>
<p>This method does not take any arguments.</p>
<p>When this method is called, the currently displayed tour step is removed, and the user's tour progress state is reset.</p>
<p>You can show the tour again by <a href="/docs/play/play-tour" target="_blank">playing</a> it.