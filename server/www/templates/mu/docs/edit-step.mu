<h3 style="text-align:left;" class="well">Editing a Tour Step</h3>
<br>
<p>Editing a tour step includes editing the:
	<ul>
		<li>Step Title</li>
		<li>Step Content</li>
		<li>Button Names</li>
		<li>Button Visibility</li>
		<li>Button Events</li>
		<li>Step Binding</li>
		<li>Step Arrow Pointer Direction and Positioning</li>
		<li>Step Position</li>
	</ul>
</p>
<br>
<h4 id="load">Loading the Step to Edit</h4>
<p>For all the step editing options available, the first action is to load the step you wish to edit. To do this:
	<ol>
		<li>Navigate to the URL on which you placed the step you will be editing.</li>
		<li><a href="/docs/build/launch-editor" target="_blank">Launch</a> the EPT Tour Editor.</li>
		<li>If you are not automatically logged in, then click the 'Login' button, and fill in and submit the login form.</li>
		<li>Once logged in, the tour you were editing should be automatically loaded. If it doesnt load automatically, click the 'Switch Tour' button and choose the tour.</li>
		<li>Once the tour is loaded, click the 'Switch Step' button and select the step you wish to edit.</li>
	</ol>
	The step you wanted to edit, should now be loaded and displayed.
</p>
<br>
<h4>Editing the Step Title</h4>
<ol>
	<li><a href="#load">Load</a> the step you wish to edit.</li>
	<li>Click the 'Edit' link in the top-right corner of the step popover. This should bring the step into edit mode.</li>
	<li>Enter the new title for that step in the 'Title' textfield (first field from top to bottom).</li>
	<li>Click the 'Done' link in the top-right corner of the step popover. This should bring the step back into preview mode.</li>
</ol>
<p>
	You have successfully edited the title of your step.
</p>
<br>
<h4>Editing the Step Contents</h4>
<ol>
	<li><a href="#load">Load</a> the step you wish to edit.</li>
	<li>Click the 'Edit' link in the top-right corner of the step popover. This should bring the step into edit mode.</li>
	<li>Enter the new title for that step in the 'Content' textfield (second field from top to bottom).</li>
	<li>Click the 'Done' link in the top-right corner of the step popover. This should bring the step back into preview mode.</li>
</ol>
<p>
	You have successfully edited the contents of your step.
</p>
<p><strong>NOTE:</strong> The content field <strong>supports markdown</strong> input. To see a preview of how your markdown looks, click the 'Done' button and enter preview mode.</p>
<br>
<h4>Editing the Button Names</h4>
<ol>
	<li><a href="#load">Load</a> the step you wish to edit.</li>
	<li>Click the 'Edit' link in the top-right corner of the step popover. This should bring the step into edit mode.</li>
	<li>The Next and Previous buttons are now replaced with textfields, with a drop-down menu. Click the textfield corresponding to the button you wish to rename, and type in your new name for that button.</li>
	<li>Click the 'Done' link in the top-right corner of the step popover. This should bring the step back into preview mode.</li>
</ol>
<p>
	You have successfully edited the names of your step buttons.
</p>
<br>
<h4>Changing Button Visibility</h4>
<ol>
	<li><a href="#load">Load</a> the step you wish to edit.</li>
	<li>Click the 'Edit' link in the top-right corner of the step popover. This should bring the step into edit mode.</li>
	<li>The Next and Previous buttons are now replaced with textfields, with a drop-down menu. Click the drop-down button next to the step you wish to edit and a drop-down menu appears.</li>
	<li>Hover over the 'Display' menu item, and a submenu with three options appears. The three display options are:
		<ul>
			<li><strong>Show:</strong> Always shows the button.</li>
			<li><strong>Hide:</strong> Never shows the button.</li>
			<li><strong>Auto:</strong> Hides the 'Previous' button if the current step is the first step in the tour. Hides the 'Next' button if the current step is the last step in the tour. In all other cases, displays the button.</li>
		</ul>
	<li>Click the 'Done' link in the top-right corner of the step popover. This should bring the step back into preview mode.</li>
</ol>
<p>
	You have successfully edited the visibility of your step buttons.
</p>
<br>
<h4 id="events">Changing Button Events</h4>
<p>You can configure your steps such that they fire custom javascript events when the user clicks on either of the two buttons. This will cause EPT Tour Player to throw these custom events in addition to the events it <a href="/docs/play/events-thrown" target="_blank">normally throws</a>. For a detailed explanation of events, click <a href="/docs/play/catching-events" target="_blank">here</a>.</p>
<br>
<h4 id="add-trigger">Adding a New Event Trigger</h4>
<ol>
	<li><a href="#load">Load</a> the step you wish to edit.</li>
	<li>Click the 'Edit' link in the top-right corner of the step popover. This should bring the step into edit mode.</li>
	<li>The Next and Previous buttons are now replaced with textfields, with a drop-down menu. Click the drop-down button next to the step you wish to edit and a drop-down menu appears.</li>
	<li>Click the 'Edit Trigger' menu item. The 'Add Trigger' modal dialog should popup.</li>
	<li>Enter the name of the event you need to fire, in the 'Event Name' field.</li>
	<li>To add the new event trigger, click the 'Add Trigger' button.</li>
	(or)
	<li>If you decide against adding a new event trigger, simply click 'Cancel'.</li>
	<li>Click the 'Done' link in the top-right corner of the step popover. This should bring the step back into preview mode.</li>
</ol>
<p>
	You have successfully added a new event trigger.
</p>
<br>
<h4 id="remove-trigger">Removing an Existing Event Trigger</h4>
<ol>
	<li><a href="#load">Load</a> the step you wish to edit.</li>
	<li>Click the 'Edit' link in the top-right corner of the step popover. This should bring the step into edit mode.</li>
	<li>The Next and Previous buttons are now replaced with textfields, with a drop-down menu. Click the drop-down button next to the step you wish to edit and a drop-down menu appears.</li>
	<li>Click the 'Edit Trigger' menu item. The 'Remove Trigger' modal dialog should popup.</li>
	<li>To remove this event trigger, click the 'Remove Trigger' button.</li>
	(or)
	<li>If you decide against removing the event trigger, simply click 'Cancel'.</li>
	<li>Click the 'Done' link in the top-right corner of the step popover. This should bring the step back into preview mode.</li>
</ol>
<p>
	You have successfully removed the custom event trigger.
</p>
<br>
<h4>Editing an Event Trigger</h4>
<ol>
	<li><a href="#remove-trigger">Remove</a> the existing trigger.</li>
	<li><a href="#add-trigger">Add</a> a new event trigger.</li>
</ol>
<br>
<h4>Step Binding</h4>
<p>In an EPT Tour, every tour step is 'bound' to one of the following:
	<ul>
		<li><strong>Window</strong>: When the step is bound to the (browser) window, the step cannot be displayed outside the viewport. The position of the step is fixed with respect to the viewport, and the step does not change position when the page is scrolled.</li>
		<li><strong>Document</strong>: When the step is bound to the (HTML) document, the step can be displayed anywhere on the document. The position of the step is fixed with respect to the document, and the step will scroll and change position along with the page.</li>
		<li><strong>Element</strong>: When the step is bound to a DOM element, the position of the step is fixed with respect to that element, and the step will always appear relative to that element irrespective of where the element appears on the screen. This is extremely useful if your page is constantly being updated, since changing the layout of your page will not break your tour layout.</li>
	</ul>
</p>
<br>
<h4>Editing the Step Binding</h4>
<ol>
	<li><a href="#load">Load</a> the step you wish to edit.</li>
	<li>Click the 'Edit' link in the top-right corner of the step popover. This should bring the step into edit mode.</li>
	<li>Click the button corresponding to the required step binding option: Window, Document or Element.</li>
		<ul>
			<li>If you chose 'Element' binding, an 'Element Selector' modal dialog is displayed. Enter a valid <a href="http://api.jquery.com/category/selectors/" target="_blank">jQuery element selector</a>.</li>
			<li>Click the 'Add Selector' button to add the element binding.</li>
			<li>If the selector you entered does not match any item on the page, or is not a valid jQuery selector, an error message will be displayed. Please correct your selector and click 'Add Selector' again.</li>
			<li>If the selector you entered matches more than one item on the page, the step will be bound to the first element as per the DOM order.</li>
			<li>If you decide against binding to an element, simply click the 'Cancel' button.</li>
		</ul>
	<li>Click the 'Done' link in the top-right corner of the step popover. This should bring the step back into preview mode.</li>
</ol>
<p>
	You have successfully edited the step binding.
</p>
<br>
<h4 id="arrow">Editing the Step Arrow Pointer Direction and Positioning</h4>
<p>Every step of your tour can be displayed wither with or without a small arrow pointing towards a direction of your choice. In case you chose to bind your step to an element, the direction of the arrow will also determine the placement of your step, relative to the bound element. The default value is none. The arrow position relative to the step popover can also be manually set. The default position for this is 'Center' or 'Middle'.
<ol>
	<li><a href="#load">Load</a> the step you wish to edit.</li>
	<li>Click the 'Edit' link in the top-right corner of the step popover. This should bring the step into edit mode.</li>
	<li>To change the arrow pointing direction, click one of the 'Arrow Pointing Direction' buttons. The available options are:
		<ul>
			<li><strong>None:</strong> No arrow is displayed.</li>
			<li><strong>Up:</strong> The arrow is displayed pointing upwards. If your step is element-bound, the step is displayed below the element.</li>
			<li><strong>Down:</strong> The arrow is displayed pointing downwards. If your step is element-bound, the step is displayed above the element.</li>
			<li><strong>Left:</strong> The arrow is displayed pointing leftwards. If your step is element-bound, the step is displayed to the right of the element.</li>
			<li><strong>Right:</strong> The arrow is displayed pointing rightwards. If your step is element-bound, the step is displayed to the left of the element.</li>
		</ul>
	</li>
	<li>It is not possible to change the arrow position for element-bound steps. To change the arrow position, click one of the 'Arrow Position' buttons. The available options are:
		<ul>
			<li><strong>Top:</strong> The arrow is positioned towards the top of the step popover.</li>
			<li><strong>Middle:</strong> The arrow is positioned towards the middle of the step popover.</li>
			<li><strong>Bottom:</strong> The arrow is positioned towards the bottom of the step popover.</li>
			<li><strong>Left:</strong> The arrow is positioned towards the left of the step popover.</li>
			<li><strong>Center:</strong> The arrow is positioned towards the center of the step popover.</li>
			<li><strong>Right:</strong> The arrow is positioned towards the right of the step popover.</li>
		</ul>
		The Top/Middle/Bottom options are available when the arrow direction is either Left or Right. The Left/Center/Right options are available when the arrow direction is either Up or Down.
	</li>
	<li>Click the 'Done' link in the top-right corner of the step popover. This should bring the step back into preview mode.</li>
</ol>
<br>
<h4>Changing the Step Position</h4>
<ol>
	<li><a href="#load">Load</a> the step you wish to edit.</li>
	<li>If your step is not element-bound, simply drag and drop the step to the new position.</li>
	(or)
	<li>If your step is element-bound, <a href="#arrow">change the arrow pointer direction</a>.</li>	
</ol>
<p>
	You have successfully repositioned your tour step.
</p>
