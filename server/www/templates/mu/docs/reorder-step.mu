<h3 style="text-align:left;" class="well">Re-order Tour Steps</h3>
<br>
<h4>To Re-order Tour Steps</h4>
<ol>
	<li>Navigate to the URL on which your tour runs.</li>
	<li><a href="/docs/build/launch-editor" target="_blank">Launch</a> the EPT Tour Editor.</li>
	<li>If you are not automatically logged in, then click the 'Login' button, and fill in and submit the login form.</li>
	<li>Once logged in, the tour you were editing should be automatically loaded. If it doesnt load automatically, click the 'Switch Tour' button and choose the tour.</li>
	<li>Once the tour is loaded, click the 'Switch Step' button. A drop-down menu containing a list of all the tour steps should be displayed.</li>
	<li>Drag the step you want to reorder, and drop it in it's new position. Repeat for each step you wish to re-order.</li>
</ol>
<p>
	You have successfully re-ordered the steps in your tour.
</p>