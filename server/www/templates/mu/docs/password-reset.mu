<h3 style="text-align:left;" class="well">Reset Your EPT User Account Password</h3>
<p>
	In case you forgot the password to your EPT user account, or you wish to change your account password:
	<ol>
		<li>Request a password reset email. You can do this by visiting <a href="/resetpassword" target="_blank">https://www.easyproducttours.com/resetpassword</a>.</li>
		<li>Once you receive the email, visit the link contained in it.</li>
		<li>Enter your new password in each field. Both passwords should match and should be atleast 6 characters long.</li>
		<li>Click the 'Change' button.</li>
	</ol>
	Your password has now been changed and you can use it to login to your account.
</p>