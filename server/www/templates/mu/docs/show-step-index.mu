<h3 style="text-align:left;" class="well">Showing the Step at a Particular Index</h3>
<br>
<p>You can programatically show any specific step in a tour, without waiting for the user to click the navigate to that step. To do this call the <code class="prettyprint">go</code> method as below:</p>
<pre class="prettyprint">eptPlayer.go(step_index);</pre>
<p>The arguments taken are:
	<ul>
		<li><strong>step_index</strong>: This is the index of the step you wish to go to.<br><strong>Type:</strong> Integer. <strong>Required:</strong> Yes.</li>
	</ul>
</p>
<p>When this method is called, the currently displayed step is unloaded and the step at the specified index in the sequence is displayed.</p>
<p>If the previous step is on a different page, the browser is redirected to that page.</p>