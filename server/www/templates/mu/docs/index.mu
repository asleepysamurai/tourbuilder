<h3 style="text-align:left;" class="well">Getting Started With Easy Product Tours</h3>
<p id="started">
	<strong>Easy Product Tours (EPT)</strong>, is a simple and easy to use service, which allows you to create interactive product tours by using a What-You-See-Is-What-You-Get (WYSIWYG) tour editor.
</p>
<br>
<h4 id="create">Create an EPT user account</h4>
<p>
	In order to use EPT, you first need an EPT user account. To create an EPT user account:
	<ol>
		<li>Go to the signup page at <a href="/signup" target="_blank">https://www.easyproducttours.com/signup</a>.</li>
		<li>Select the billing cycle and plan of your choice. Remember, it is better to go for a longer billing cycle rather than a shorter one.</li>
		<li>Click the corresponding 'Buy Now' button.</li>
		<li>You will be taken to the Easy Product Tours order page. This page is hosted by <a href="http://www.fastspring.com/purchasing-through-fastspring.php" target="_blank">FastSpring</a>, our trusted reseller. Your transaction is guaranteed to be secure by VeriSign.</li>
		<li>Verify that the plan and billing cycle you selected are reflected accurately on the order page.</li>
		<li>In the 'Contact Information' section, enter the contact details of the person purchasing the subscription. Typically, this would be someone in the purchasing/accounts department of your company.</li>
		<li>In the 'Mailing Address' section, enter the mailing details of the person purchasing the subscription. This would be the same person as in the previous step.</li>
		<li>Select a Payment Method, and click the 'Next' button.</li>
		<li>If any errors are displayed, please correct them and click the 'Next' button again. You will be taken to the 'Payment and Account Details' page.</li>
		<li>In the 'Your Payment' section, enter the appropriate details. If you need to change the Billing Address, click the 'Make Changes' link in the 'Billing Address' section.</li>
		<li>In the 'Account Information' section, enter the Full Name and the Email address of the person who will be using Easy Product Tours. Typically, this will be a software developer in your company.</li>
		<li>Double check that the email address you enter is correct. This is the address that will be associated with your Easy Product Tours account. This address will be used to login to EPT, and account activation and regular communication will be sent to this account.</li>
		<li>Click the 'Complete Order' button. If any errors are displayed, correct them and click the 'Complete Order' button again.</li>
		<li>You will be taken to the order confirmation page. Please take a screenshot of this page or note down the 'Order ID'. In case there are issues with activating your account this information might be needed.</li>
		<li>Within a few minutes, you will receive the order confirmation email and another email with instructions on how to activate your account. Follow the instructions in the email to activate your EPT account.</li>
	</ol>
</p>
<br>
<h4 id="activate">Activating your EPT user account</h4>
<p>
	Within ten minutes of signing up, we will provision a brand new account for you and send an activation email to the address you specified in the 'Account Information' section while signing up. To activate your account:
		<ul>
			<li>Simply click the <strong>activation link</strong> in the email.</li>
			(or)
			<li>Visit <a href="/user/activate" target="_blank">https://www.easyproducttours.com/user/activate</a>, and enter the <strong>activation code</strong> present in the email.</li>
			<li>Click the 'Activate' button.</li>
			<li>Once the activation code is verified, you will be taken to the password setup page.</li>
			<li>Enter any password of your choice (provided it is atleast 6 characters long), and click 'Change'.</li>
		</ul>
</p>
<p>Your account has now been activated, and you can <a href="/login" target="_blank">login</a> using your email address and the password you set.</p>
<p>If you did not receive your activation email, or if your activation code has expired, please request a new activation email by visiting <a href="/resendactivation" target="_blank">https://www.easyproducttours.com/resendactivation</a>, and entering the email address you specified in the 'Account Information' section while signing up. Also, remember to check your SPAM folder for the activation email.</p>
<br>
<h4 id="login">Login to your EPT account</h4>
<p>
	In order to login to your EPT user account:
	<ol>
		<li>Go to the login page at <a href="/login" target="_blank">https://www.easyproducttours.com/login</a>.</li>
		<li>Enter your email address in the email field.</li>
		<li>Enter your password in the password field.</li>
		<li>Click the Login button.</li>
	</ol>
	You will now be redirected to the EPT dashboard.
</p>
<br>
<h4 id="logout">Logout from your EPT account</h4>
<p>In order to logout from your EPT account:
	<ul>
		<li>Click the 'Logout' link in the navigation bar on the top of any page.</li>
		(or)
		<li>Visit <a href="/logout" target="_blank">https://www.easyproducttours.com/logout</a>.</li>
	</ul>
</p>