<h3 style="text-align:left;" class="well">Pausing a Tour Being Played</h3>
<br>
<p>In order to pause the currently playing tour call the <code class="prettyprint">pause</code> method as below:</p>
<pre class="prettyprint">eptPlayer.pause();</pre>
<p>This method does not take any arguments.</p>
<p>When this method is called, the currently displayed tour step is hidden.</p>
<p>You can continue playing the tour by <a href="/docs/play/resume-tour" target="_blank">resuming</a> it.