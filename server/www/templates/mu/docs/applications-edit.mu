<h3 style="text-align:left;" class="well">Edit an Application</h3>
<br>
<h4 id="edit">Edit an Application</h4>
<p>
	To edit an existing application:
	<ol>
		<li><a href="/login" target="_blank">Login</a> to Easy Product Tours.</li>
		<li>Once, you are at your <a href="/dash" target="_blank">EPT Dashboard</a>, click the tab for the app you wish to edit.</li>
		<li>Click the 'Edit Application' button at the bottom of the page. The application details form fields should be enabled now.</li>
		<li>Edit the form values to your liking.</li>
		<li>To save your edits, click the 'Save Changes' button.</li>
		<li>If you decide not to save your edits, click the 'Discard Edits' button and the form values will revert to their original values.</li>
	</ol>
</p>
