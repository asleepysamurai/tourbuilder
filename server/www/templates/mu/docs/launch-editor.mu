<h3 style="text-align:left;" class="well">Launching the Tour Editor</h3>
<br>
<p>
	Creating a new tour or editing an existing tour is done using the Easy Product Tours (EPT) Tour Editor. To launch the Tour Editor, you need to first install the Tour Editor bookmarklet.
</p>
<br>
<h4 id="install">Installing the Tour Editor Bookmarklet</h4>
<p>
	<ul>
		<li>Drag one of the links below to your browser's bookmarks toolbar.</li>
		<ul>
			<li>
				If your site already uses Twitter Bootstrap and you want your tour to match your site's Bootstrap theme, use this
				 <a href="javascript:(function(){_script=document.createElement('script');_script.type='text/javascript';_script.src=window.location.protocol+'//www.easyproducttours.com/editor?x=0'+(Math.random());_script.id = 'tour-builder-js-loader';_script.className='tour-builder-js-loader';_script.setAttribute('data-loadbootstrap', 'false');document.head.appendChild(_script);})();">EPT Tour Editor</a> link.
			</li>
			(or)
			<li>
				If your site does not use Twitter Bootstrap, use this
				 <a href="javascript:(function(){_script=document.createElement('script');_script.type='text/javascript';_script.src=window.location.protocol+'//www.easyproducttours.com/editor?x=0'+(Math.random());_script.id = 'tour-builder-js-loader';_script.className='tour-builder-js-loader';_script.setAttribute('data-loadbootstrap', 'true');document.head.appendChild(_script);})();">EPT Tour Editor</a> link.
			</li>
		</ul><br>
		(or)<br><br>
		<li>Create a new bookmark, using your browser's bookmark manager.</li>
		<ol>
			<li>In the title field, type in 'EPT Tour Editor'.</li>
			<li>If your site already uses Twitter Bootstrap and you want your tour to match your site's Bootstrap theme, in the URL/address field, copy-paste the below text:
				<pre class="prettyprint">javascript:(function(){_script=document.createElement('script');_script.type='text/javascript';_script.src=window.location.protocol+'//www.easyproducttours.com/editor?x=0'+(Math.random());_script.id = 'tour-builder-js-loader';_script.className='tour-builder-js-loader';_script.setAttribute('data-loadbootstrap', 'false');document.getElementsByTagName('head')[0].appendChild(_script);})();</pre>
			</li>
			(or)
			<li>If your site does not use Twitter Bootstrap, in the URL/address field, copy-paste the below text:
				<pre class="prettyprint">javascript:(function(){_script=document.createElement('script');_script.type='text/javascript';_script.src=window.location.protocol+'//www.easyproducttours.com/editor?x=0'+(Math.random());_script.id = 'tour-builder-js-loader';_script.className='tour-builder-js-loader';_script.setAttribute('data-loadbootstrap', 'true');document.getElementsByTagName('head')[0].appendChild(_script);})();</pre>
			</li>
			<li>Click the OK/Save button.</li>
		</ol>
	</ul>
	Your application has now been added and you will be redirected to the tab for your new application on the dashboard.
</p>
<br>
<h4 id="launch">Launch the Tour Editor</h4>
<p>
	Once you have installed the EPT Tour Editor bookmarklet, you can launch the tour editor by:
	<ol>
		<li>Navigate to the page you wish to run a tour on.</li>
		<li>Click the EPT Tour Editor bookmarklet. Please wait while the bookmarklet loads. This should take a maximum of a few seconds.</li>
		<li>Once the bookmarklet has loaded completely, a new EPT Tour Editor toolbar will be visible on the top of the page.</li>
	</ol>
	You have now launched the EPT Tour Editor. For a detailed description of the editor interface, click <a href="/docs/build/editor-interface" target="_blank">here</a>.
</p>
<br>
<h4 id="exit">Exit the Tour Editor</h4>
<p>
	After launching the tour editor, you can exit it at any time by:
	<ul>
		<li>Clicking the EPT Tour Editor bookmarklet again.</li>
		(or)
		<li>Closing the browser window/tab in which the editor is running.</li>
	</ul>
	You have now successfully exited the EPT Tour Editor.
</p>