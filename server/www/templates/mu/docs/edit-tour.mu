<h3 style="text-align:left;" class="well">Edit an Existing Tour</h3>
<br>
<p>Currently the only tour edit action is renaming the tour.</p>
<br>
<h4>To rename a tour</h4>
	<ol>
		<li><a href="/login" target="_blank">Login</a> to your EPT Dashboard.</li>
		<li>Click the 'Edit' link corresponding to the tour which you wish to rename.</li>
		<li>Click any of the two 'Rename Tour' buttons on the tour page.</li>
		<li>A modal dialog should popup, asking you for a new name for that tour. Enter the new name in the 'Tour Name' field.</li>
		<li>Click the 'Rename Tour' button, to rename the tour to the newly entered name.</li>
		(or)
		<li>If you decide against renaming the tour, simply click the 'Cancel' button.</li>
	</ol>
</p>