<h3 style="text-align:left;" class="well">Loading a Tour into the EPT Tour Player</h3>
<br>
<p>In order to play a particular tour, you have to first load that tour into the player. To load a tour you need to know that tour's unique ID.</p>
<br>
<h4>Finding the Tour ID of a Tour</h4>
<ol>
	<li><a href="/login" target="_blank">Login</a> to your EPT Dashboard.</li>
	<li>A list of tours created by you should be displayed. The number specified under the ID column corresponding to the tour you wish to load is the unique Tour ID for that tour.</li>
</ol>
<br>
<h4>Loading a Tour</h4>
<p>Once you have the Tour ID of the tour you wish to load, use the following Javascript snippet, to actually load the tour into the tour player.</p>
<pre class="prettyprint">eptPlayer.load(tour_id, autoplay, unique_userid, dynamic_urls);</pre>
<p>The arguments passed are:
	<ul>
		<li><code class="prettyprint">tour_id</code>: The unique ID for the tour you wish to load.<br><strong>Type:</strong> Integer. <strong>Required:</strong> Yes.</li>
		<li><code class="prettyprint">autoplay</code>: Whether or not to automatically play the tour after loading it.<br><strong>Type:</strong> Boolean. <strong>Required:</strong> No. <strong>Default:</strong> True.</li>
		<li><code class="prettyprint">unique_userid</code>: Unique id to track a user. Usually, this would be the user's userid or email id in your site. This is used to track which users have seen which tours so as not to show tours repeatedly, to those who have already seen them. For further details check out <a href="/docs/play/tracking-users" target="_blank">Tracking Users</a>.<br><strong>Type:</strong> String/Boolean. <strong>Required:</strong> No. <strong>Default:</strong> True.</li>
		<li><code class="prettyprint">dynamic_urls</code>: An array of custom URLs used to override the default URLs on which the tour runs. For further details check out <a href="/docs/play/dynamic-urls" target="_blank">Dynamic URLs</a>.<br><strong>Type:</strong> Array. <strong>Required:</strong> No. <strong>Default:</strong> Null.</li>
	</ul>
</p>