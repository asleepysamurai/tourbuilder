<h3 style="text-align:left;" class="well">Multi-Page Tours</h3>
<br>
<p>
	A multi-page tour is a tour which has steps in multiple pages. Using Easy Product Tours (EPT), you can easily create a tour which spans multiple pages. 
</p>
<br>
<h4>Creating Multi-Page Tours</h4>
<p>
	Creating a multi-page tour is as simple and easy as creating a normal single page tour. Assume that you need to create a multi-page tour as follows:
	<ul>
		<li>Steps 1 and 2 in 'http://www.example.com/myapp1/page1'</li>
		<li>Step 3 and 4 in 'http://www.example.com/myapp1/page2'</li>
		<li>Step 5 in 'http://www.example.com/myapp1/page1' again.</li>
	</ul>
	To create this tour:
	<ol>
		<li>Navigate to 'http://www.example.com/myapp1/page1'.</li>
		<li><a href="/docs/build/launch-editor" target="_blank">Launch</a> the EPT Tour Editor.</li>
		<li>If you are not automatically logged in, then click the 'Login' button, and fill in and submit the login form.</li>
		<li>Once logged in, click the 'New Tour' button and enter a name for the tour. Click 'Create' to create the tour.</li>
		<li>A new tour with the name specified should be created and Step 1 should also be created and loaded. <a href="/docs/build/edit-step" target="_blank">Edit</a> the step as per your needs.</li>
		<li>Click the 'New Step' button to add Step 2. <a href="/docs/build/edit-step" target="_blank">Edit</a> the step as per your needs.</li>
		<li>Navigate to 'http://www.example.com/myapp1/page2'.</li>
		<li><a href="/docs/build/launch-editor" target="_blank">Launch</a> the EPT Tour Editor.</li>
		<li>If you are not automatically logged in, then click the 'Login' button, and fill in and submit the login form.</li>
		<li>Once logged in, the tour you were editing should be automatically loaded. If it doesnt load automatically, click the 'Switch Tour' button and choose the tour.</li>
		<li>Once the tour is loaded, click the 'New Step' button to add Step 3. <a href="/docs/build/edit-step" target="_blank">Edit</a> the step as per your needs.</li>
		<li>Click the 'New Step' button to add Step 4. <a href="/docs/build/edit-step" target="_blank">Edit</a> the step as per your needs.</li>
		<li>Navigate to 'http://www.example.com/myapp1/page1'.</li>
		<li><a href="/docs/build/launch-editor" target="_blank">Launch</a> the EPT Tour Editor.</li>
		<li>If you are not automatically logged in, then click the 'Login' button, and fill in and submit the login form.</li>
		<li>Once logged in, the tour you were editing should be automatically loaded. If it doesnt load automatically, click the 'Switch Tour' button and choose the tour.</li>
		<li>Click the 'New Step' button to add Step 5. <a href="/docs/build/edit-step" target="_blank">Edit</a> the step as per your needs.</li>		
	</ol>
	You have successfully created a multi-page tour. A multi-page tour can span as many pages as you wish and can even visit the same page, multiple times in the course of the tour.
</p>