<h3 style="text-align:left;" class="well">Multiple Tours on a Single Page</h3>
<br>
<p>
	Easy Product Tours supports creating and running multiple tours on a single page. Assume that we wish to create and run two tours - 'Tour 1' and 'Tour 2' on a single page - 'http://www.example.com/myapp'. 
</p>
<br>
<h4>To Create Multiple Tours</h4>
	<ol>
		<li>Navigate to 'http://www.example.com/myapp'.</li>
		<li><a href="/docs/build/launch-editor" target="_blank">Launch</a> the EPT Tour Editor.</li>
		<li>If you are not automatically logged in, then click the 'Login' button, and fill in and submit the login form.</li>
		<li>Once logged in, click the 'New Tour' button and enter a name ('Tour 1') for the tour. Click 'Create' to create the tour.</li>
		<li>A new tour with the name specified should be created and Step 1 should also be created and loaded. <a href="/docs/build/edit-step" target="_blank">Edit</a> the step as per your needs. Add any other steps you may require.</li>
		<li>To create 'Tour 2', click the 'New Tour' button and enter a name ('Tour 2') for the tour. Click 'Create' to create the tour.</li>
		<li>A new tour with the name specified should be created and Step 1 should also be created and loaded. <a href="/docs/build/edit-step" target="_blank">Edit</a> the step as per your needs. Add any other steps you may require.</li>
	</ol>
	You have successfully created multiple tours on the same page. You can create as many tours as you wish on a particular page, and each tour can contain as many steps as you wish.
</p>