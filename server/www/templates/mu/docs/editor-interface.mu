<h3 style="text-align:left;" class="well">EPT Tour Editor Interface Explained</h3>
<br>
<p>
	Once you launch the EPT Tour Editor from the bookmarklet, a toolbar is added to the top of your page. This toolbar is the primary interface of the EPT Tour Editor. Most of the actions you perform (including adding, editing and deleting tours/steps), will be performed via this toolbar. This page will explain the various parts of the toolbar and their actions.
</p>
<br>
<h4 id="overview">Overview of the EPT Toolbar</h4>
<p>
	The editor is divided into 3 main areas: 
	<ul>
		<li><strong>Tour Actions Area:</strong> This is the group of buttons on the left side of the toolbar. With the exception of the 'Logout/Login' button, all the buttons in this area perform actions related to tours.</li>
		<li><strong>Step Actions Area:</strong> This is the group of buttons on the right side of the toolbar. All the buttons in this area perform actions related to steps.</li>
		<li><strong>Notifications Area:</strong> This is the area in between the two button clusters. This area is generally used to display textual notifications. It most commonly displays the name of the tour currently being edited.</li>
	</ul>
</p>
<br>
<h4 id="touractions">Tour Actions Area</h4>
<p>
	This area contains four buttons:
	<ul>
		<li><strong>Login/Logout Button:</strong> This is the first button on the toolbar (left-to-right). It is labelled as either 'Login' or 'Logout'. Clicking the button will either log you into EPT or log you out, depending on the label on the button.</li>
		<li><strong>New Tour Button:</strong> This is the second button on the toolbar (left-to-right). It is labelled as 'New Tour'. Clicking this button will create a new tour for that page.</li>
		<li><strong>Delete Button:</strong> This is the third button on the toolbar (left-to-right). It is labelled as 'Delete'. Clicking this button will delete the tour you are currently editing.</li>
		<li><strong>Switch Tour Button:</strong> This is the fourth button on the toolbar (left-to-right). It is labelled as 'Switch Tour'. Clicking this button will show a dropdown list, containing all the tours for the current site. You can click on any tour to switch to it.</li>
	</ul>
</p>
<br>
<h4 id="stepactions">Step Actions Area</h4>
<p>
	This area contains five buttons:
	<ul>
		<li><strong>Switch Step Button:</strong> This is the fifth button on the toolbar (left-to-right). It is labelled as 'Switch Step'. Clicking this button will show a dropdown list, containing all the steps for the current tour. You can click on any step to switch to it. You can also drag the steps to reorder them.</li>
		<li><strong>Delete Button:</strong> This is the sixth button on the toolbar (left-to-right). It is labelled as 'Delete'. Clicking this button will delete the step you are currently editing.</li>
		<li><strong>New Step Button:</strong> This is the seventh button on the toolbar (left-to-right). It is labelled as 'New Step'. Clicking this button will insert a new step after the current step on that page.</li>
		<li><strong>Previous Button:</strong> This is the eigth button on the toolbar (left-to-right). It is labelled as 'Previous'. Clicking the button will load the step before the current step.</li>
		<li><strong>Next Button:</strong> This is the ninth button on the toolbar (left-to-right). It is labelled as 'Next'. Clicking the button will load the step after the current step.</li>
	</ul>
</p>
<br>
<p>
	The functions performed by these buttons are explored in greater detail in the corresponding documentation page for them.
</p>