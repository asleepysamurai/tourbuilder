<h3 style="text-align:left;" class="well">Showing the Next Step in a Tour</h3>
<br>
<p>You can programatically show the next step in a tour, without waiting for the user to click the 'Next' button. To do this call the <code class="prettyprint">next</code> method as below:</p>
<pre class="prettyprint">eptPlayer.next();</pre>
<p>This method does not take any arguments.</p>
<p>When this method is called, the currently displayed step is unloaded and the next step in the sequence is displayed.</p>
<p>If the next step is on a different page, the browser is redirected to that page.</p>