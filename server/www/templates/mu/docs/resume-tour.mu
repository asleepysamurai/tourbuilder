<h3 style="text-align:left;" class="well">Resuming a Paused Tour</h3>
<br>
<p>In order to resume a tour that has been paused previously, call the <code class="prettyprint">play</code> method as below:</p>
<pre class="prettyprint">eptPlayer.play();</pre>
<p>This method does not take any arguments.</p>
<p>Additional details about this method available <a href="/docs/play/play-tour" target="_blank">here</a>.</p>