<h3 style="text-align:left;" class="well">Create an Application</h3>
<br>
<h4 id="register">Register an Application</h4>
<p>
	In order to register an application:
	<ol>
		<li><a href="/login" target="_blank">Login</a> to Easy Product Tours. If you do not have an EPT account, <a href="/signup" target="_blank">signup</a> for one. The complete guide to getting started with EPT is available <a href="/docs" target="_blank">here</a>.</li>
		<li>Once, you are at your <a href="/dash" target="_blank">EPT Dashboard</a>, click the <a href="/dash/new" target="_blank">Add New App</a> tab.</li>
		<li>In the 'Root URL' text field, enter the root URL of your webapp. The root URL of your app is the common portion of the url with which all the pages on which you wish to run tours start with. For example, consider an app with the following URLs:
			<ul>
				<li>http://www.example.com/myapp</li>
				<li>http://www.example.com/myapp/page1</li>
				<li>http://www.example.com/myapp/dir2/page5</li>
				<li>http://www.example.com/myapp/dir3?q1=v1&q2=v2</li>
			</ul>
			The root URL for this app would be 'http://www.example.com/myapp'. When you enter this URL into the 'Root URL' field, EPT will normalize it to 'www.example.com/myapp' (dropping the protocol and any URL fragments).
		</li>
		<li>In the 'Name' field, enter the name you wish to represent this application by. This name is purely for display purposes and you can have multiple apps with the same name (you might get confused once in a while though ;).</li>
		<li>Click the 'Add Application' button.</li>
	</ol>
	Your application has now been added and you will be redirected to the tab for your new application on the dashboard.
</p>
<br>
<h4 id="multiple">Registering Multiple Applications</h4>
<p>
	In order to register multiple applications:
	<ol>
		<li>Ensure that you have not exhausted the application quota for your EPT user account. In case you have exhausted your quota, <a href="/docs/general/change-plan" target="_blank">upgrade your EPT User Account Plan</a>.</li>
		<li>Once you have ensured that you have enough applications remaining in your quota, follow <a href="#register">these steps</a> to register each new application one by one. It is currently not possible to batch add new applications.</li>
	</ol>
</p>
