<h3 style="text-align:left;" class="well">A Guide to EPT Applications</h3>
<br>
<h4>What is an Application?</h4>
<p>
	Each individual webapp or website for which you wish to create tours, is represented in Easy Product Tours (EPT) as an 'application' <a href="#why-seperate">(why?)</a>. Thus, the first step to creating a tour is to ensure that the site in question has been registered as an application in EPT. 
</p>
<br>
<h4 id="why-seperate">Why Should I Register Seperate Applications?</h4>
<p>
	EPT requires that each tour you create is associated with a particular application for two reasons:
	<ul>
		<li>To ensure that your tours play only on the specific domain which you have registered as the application's root URL.</li>
		<li>To accurately track which tours have been completely viewed by which users.</li>
	</ul>
	By creating only one application and associating all your tours with that application, tours may be displayed improperly by the tour player. In addition, user tracking will not be accurate, which again causes improper display of tours.
</p>
<p>So, always register a seperate application for each individual webapp or website that you wish to run tours on.</p>
