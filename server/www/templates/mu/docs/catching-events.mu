<h3 style="text-align:left;" class="well">Catching Events Thrown by the Tour Player</h3>
<br>
<p>During the course of playing a tour EPT Tour Player will throw <a href="/docs/play/events-thrown" target="_blank">various events</a>. Depending on whether or not your app/site needs to respond to specific actions, you might need to catch these events.</p>
<br>
<h4>Registering Event Handlers</h4>
<p>In order to catch an event, you need to first register an event handler for that particular event. To register an event handler you have to call the <code class="prettyprint">on</code> method as below:</p>
<pre class="prettyprint">eptPlayer.on(event_name, handler);</pre>
<p>The arguments are:
	<ul>
		<li><strong>event_name:</strong> This is the name of the event that you wish to handle. A list of events thrown by EPT Tour Player is available <a href="/docs/play/events-thrown" target="_blank">here</a>.<br><strong>Type:</strong> String. <strong>Required:</strong> Yes.</li>
		<li><strong>handler:</strong> This is the callback function which will handle the event thrown. When EPT Tour Player calls this callback function, it passes one object as the argument to it. The exact content of the object depends on the event thrown.<br><strong>Type:</strong> Function. <strong>Required:</strong> Yes.</li>
	</ul>
</p>
<br>
<h4>De-registering Event Handlers</h4>
<p>If you no longer wish to catch an event, you need to de-register the event handler for that particular event. To de-register an event handler you have to call the <code class="prettyprint">off</code> method as below:</p>
<pre class="prettyprint">eptPlayer.off(event_name);</pre>
<p>The arguments are:
	<ul>
		<li><strong>event_name:</strong> This is the name of the event that you had previously registered an event handler for.<br><strong>Type:</strong> String. <strong>Required:</strong> Yes.</li>
	</ul>
</p>