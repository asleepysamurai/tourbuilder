<h3 style="text-align:left;" class="well">Playing a Tour</h3>
<br>
<p>In order to play a particular tour, you have to first <a href="/docs/play/load-tour" target="_blank">load</a> that tour into the player. Once the tour has been loaded, you can play it by calling the <code class="prettyprint">play</code> method as below:</p>
<pre class="prettyprint">eptPlayer.play();</pre>
<p>This method does not take any arguments.</p>
<p>When this method is called, after loading a tour or after pausing a tour, it resumes the tour from the step which the user last saw.</p>
<p>When this method is called, after stopping a tour, it resumes the tour from the first step.</p>
<p>if this method is called, after the browser was redirected to a new page by EPT Tour Player, it resumes the tour from the next step in sequence on that page. This makes playing multipage tours no different from playing a single page tour.</p>
<p>If this method is called after the user has seen the entire tour, no action occurs. In addition a <a href="/docs/play/errors-thrown" target="_blank">507 error</a> is thrown.</p>