<h3 style="text-align:left;" class="well">Errors Thrown by EPT Tour Player</h3>
<br>
<p>During the course of playing a tour EPT Tour Player will throw various errors</a>. Depending on whether or not your app/site needs to respond to specific actions, you might need to catch these errors.</p>
<br>
<p>The errors thrown by EPT Tour Player are:</p>
<table class="table table-striped table-bordered">
	<tr>
		<th>Error Code</th>
		<th>Error Name</th>
		<th>Thrown When</th>
	</tr>
	<tr>
		<td>501</td>
		<td>tour.load.error</td>
		<td>If the tour id to be loaded is not specified or is invalid.</td>
	</tr>
	<tr>
		<td>502</td>
		<td>tour.load.error</td>
		<td>If the tour id specified does not correspond to an actual tour.</td>
	</tr>
	<tr>
		<td>503</td>
		<td>tour.load.error</td>
		<td>If an internal error occurred on the EPT server while trying to load your tour data or if EPT is down.</td>
	</tr>
	<tr>
		<td>504</td>
		<td>tour.load.error</td>
		<td>If the tour data retrieved from the EPT servers are invalid or incomplete or corrupt.</td>
	</tr>
	<tr>
		<td>505</td>
		<td>step.load.error</td>
		<td>If the specified step index to be loaded is invalid.</td>
	</tr>
	<tr>
		<td>506</td>
		<td>step.load.error</td>
		<td>If the specified step index does not exist for the currently loaded tour.</td>
	</tr>
	<tr>
		<td>507</td>
		<td>step.load.error</td>
		<td>If the user being tracked has already completed this tour, and any step in the tour is played again.</td>
	</tr>
</table>
<br>
<p>The error object passed as the first parameter to the callback function, has two properties:
	<ul>
		<li><strong>code:</strong> This is the error code of the error being thrown.</li>
		<li><strong>msg:</strong> This is a short textual message describing the error being thrown.</li>
	</ul>
</p>