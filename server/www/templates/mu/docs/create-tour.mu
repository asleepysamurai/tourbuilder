<h3 style="text-align:left;" class="well">Create a Tour</h3>
<br>
<h4>To Create a New Tour</h4>
<ol>
	<li>Navigate to the URL on which you wish to place the first step of your tour.</li>
	<li><a href="/docs/build/launch-editor" target="_blank">Launch</a> the EPT Tour Editor.</li>
	<li>If you are not automatically logged in, then click the 'Login' button, and fill in and submit the login form.</li>
	<li>Once logged in, click the 'New Tour' button.</li>
	<li>A modal dialog should popup, asking you to name the tour. Fill in an appropriate name. This name is used only for display purposes and hence you can have multiple tours with the same name.</li>
	<li>Click the 'Create' button, to create the tour.</li>
	(or)
	<li>If you decide against creating a new tour, simply click the 'Cancel' button.</li>
</ol>
<p>
	A new tour with the name you specified should have been created. When you create a new tour, it is automatically loaded as the current tour in the Tour Editor. In addition, the first step is also automatically created and loaded.
</p>