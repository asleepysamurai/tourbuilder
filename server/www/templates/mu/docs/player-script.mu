<h3 style="text-align:left;" class="well">Including the EPT Tour Player Script</h3>
<br>
<p>Once you have created your tour, you need to actually play it on your site for your users. For this, you have to include the EPT Tour Player script.</p>
<br>
<h4>EPT Tour Player Bootstrap Script</h4>
<p>
	To load the EPT Tour Player on your site, you need to include the EPT Tour Player Bootstrapper. The bootstrapper, is a bit of javascript, which loads the actual tour player and the required tour data from the EPT servers. 
</p>
<p>
	To include the bootstrapper, include the following tags just before the <code class="prettyprint">&lt;/body&gt;</code> on all the pages you wish to play your tour on.
</p>
<p>
	If your site already uses Twitter Bootstrap, and you want your tours to use your site's Bootstrap theme, include:
</p>
<pre class="prettyprint">
&lt;link rel="stylesheet" type="text/css" href="http://www.easyproducttours.com/player/player.min.css" /&gt;
&lt;script type="text/javascript" src="http://www.easyproducttours.com/player/player.min.js"&gt;&lt;/script&gt;
</pre>
<p>
	If your site does not use Twitter Bootstrap, include:
</p>
<pre class="prettyprint">
&lt;link rel="stylesheet" type="text/css" href="http://www.easyproducttours.com/player/bootstrap.min.css" /&gt;
&lt;link rel="stylesheet" type="text/css" href="http://www.easyproducttours.com/player/player.min.css" /&gt;
&lt;script type="text/javascript" src="http://www.easyproducttours.com/player/player.min.js"&gt;&lt;/script&gt;
</pre>
<p>If you are running the tour on an SSL enabled page, replace 'http' with 'https' in the above URLs.</p>
<p>
	This bootstrapper will run as soon as your page is loaded and download the necessary scripts for the EPT Tour Player. 
</p>
<br>
<h4>The eptPlayer Object and No-conflict</h4>
<p>
	Once the Player is downloaded and initialized, it will register an object in the global <code class="prettyprint">window</code> scope, with the name <code class="prettyprint">eptPlayer</code>.
</p>
<p>
	If this name conflicts with the existing code your site uses, you can obtain a new reference to it by calling the <code class="prettyprint">noConflict</code> method as below:
	<pre class="prettyprint">var new_reference_for_player = eptPlayer.noConflict();</pre>
</p>
<p>
	This will release the <code class="prettyprint">eptPlayer</code> reference to the object that previously contained it, and the EPT Tour Player can be accessed through the <code class="prettyprint">new_reference_for_player</code> object.
</p>
<br>
<p>The next step is to actually <a href="/docs/play/load-tour" target="_blank">load</a> the tour you wish to run.</p>