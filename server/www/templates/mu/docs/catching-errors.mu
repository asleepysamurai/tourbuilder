<h3 style="text-align:left;" class="well">Catching Errors Thrown by the Tour Player</h3>
<br>
<p>During the course of playing a tour EPT Tour Player will throw <a href="/docs/play/errors-thrown" target="_blank">various errors</a>. Depending on whether or not your app/site needs to respond to specific actions, you might need to catch these errors.</p>
<br>
<h4>Registering Error Handlers</h4>
<p>In order to catch an error, you need to first register an error handler for that particular error. To register an error handler you have to call the <code class="prettyprint">on</code> method as below:</p>
<pre class="prettyprint">eptPlayer.on(error_name, handler);</pre>
<p>The arguments are:
	<ul>
		<li><strong>error_name:</strong> This is the name of the error that you wish to handle. A list of errors thrown by EPT Tour Player is available <a href="/docs/play/errors-thrown" target="_blank">here</a>.<br><strong>Type:</strong> String. <strong>Required:</strong> Yes.</li>
		<li><strong>handler:</strong> This is the callback function which will handle the error thrown. When EPT Tour Player calls this callback function, it passes one object as the argument to it. The exact content of the object depends on the error thrown.<br><strong>Type:</strong> Function. <strong>Required:</strong> Yes.</li>
	</ul>
</p>
<br>
<h4>De-registering Error Handlers</h4>
<p>If you no longer wish to catch an error, you need to de-register the error handler for that particular error. To de-register an error handler you have to call the <code class="prettyprint">off</code> method as below:</p>
<pre class="prettyprint">eptPlayer.off(error_name);</pre>
<p>The arguments are:
	<ul>
		<li><strong>error_name:</strong> This is the name of the error that you had previously registered an error handler for.<br><strong>Type:</strong> String. <strong>Required:</strong> Yes.</li>
	</ul>
</p>