<h3 style="text-align:left;" class="well">Events Thrown by EPT Tour Player</h3>
<br>
<p>During the course of playing a tour EPT Tour Player will throw various events</a>. Depending on whether or not your app/site needs to respond to specific actions, you might need to catch these events.</p>
<br>
<p>The events thrown by EPT Tour Player are:</p>
<table class="table table-striped table-bordered">
	<tr>
		<th>Event Name</th>
		<th>Thrown When</th>
		<th>Event Object</th>
	</tr>
	<tr>
		<td>tour.load</td>
		<td>When the tour data has been loaded succesfully.</td>
		<td>{<strong>tour:</strong> the id of the loaded tour}</td>
	</tr>
	<tr>
		<td>tour.completed</td>
		<td>This event is thrown only if user tracking is enabled. It is thrown when the user has viewed the last step in the currently loaded tour.</td>
		<td>{<strong>tour:</strong> the id of the loaded tour}</td>
	</tr>
	<tr>
		<td>step.load.changepage</td>
		<td>This event is thrown if the next step to be loaded is on a different page from the current page. It is thrown just before redirecting the browser.</td>
		<td>{<strong>tour:</strong> the id of the loaded tour,<br><strong>step: </strong>{<br><strong>from:</strong> the index of the current step being displayed,<br><strong>to:</strong> the index of the next step being loaded.<br>}}</td>
	</tr>
	<tr>
		<td>step.show.before</td>
		<td>This event is thrown after the current step is unloaded and just before the next step is shown.</td>
		<td>{<strong>tour:</strong> the id of the loaded tour,<br><strong>step: </strong>{<br><strong>from:</strong> the index of the current step being displayed,<br><strong>to:</strong> the index of the next step being loaded.<br>}}</td>
	</tr>
	<tr>
		<td>step.show.after</td>
		<td>This event is thrown just after the current step is shown. This event is usually thrown immediately after <code class="prettyprint">step.show.before</code>.</td>
		<td>{<strong>tour:</strong> the id of the loaded tour,<br><strong>step: </strong>{<br><strong>from:</strong> the index of the current step being displayed,<br><strong>to:</strong> the index of the next step being loaded.<br>}}</td>
	</tr>
	<tr>
		<td>step.hide.before</td>
		<td>This event is thrown just before the current step is unloaded.</td>
		<td>{<strong>tour:</strong> the id of the loaded tour,<br><strong>step: </strong>{<br><strong>from:</strong> the index of the current step being displayed<br>}}</td>
	</tr>
	<tr>
		<td>step.hide.after</td>
		<td>This event is thrown just after the current step is unloaded. This  event is usually thrown immediately after <code class="prettyprint">step.hide.before</code>.</td>
		<td>{<strong>tour:</strong> the id of the loaded tour,<br><strong>step: </strong>{<br><strong>from:</strong> the index of the current step being displayed<br>}}</td>
	</tr>
	<tr>
		<td>events.all</td>
		<td>This is a special event that is used to catch all the above events and errors using a single handler. This event is thrown for each event and error that is thrown by EPT Tour Player.</td>
		<td>Corresponding to the particular event thrown.</td>
	</tr>
	<tr>
		<td>Custom Events</td>
		<td>These are the events that <a href="/docs/build/edit-step" target="_blank">you specify</a> to be triggered while you create the tour.</td>
		<td>{<strong>tour:</strong> the id of the loaded tour,<br><strong>step: </strong>{<br><strong>from:</strong> the index of the current step being displayed,<br><strong>to:</strong> the index of the next step being loaded.<br>}}</td>
	</tr>
</table>