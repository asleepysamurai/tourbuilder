<h3 style="text-align:left;" class="well">Change Your EPT Plan</h3>
<p>
	Currently, it is not possible to change the plan associated with your EPT user account via the web interface. Please send us an email to <a href="mailto:support@easyproducttours.com">support@easyproducttours.com</a>, and we will manually change your EPT plan for you.
</p>