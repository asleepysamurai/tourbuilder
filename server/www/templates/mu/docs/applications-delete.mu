<h3 style="text-align:left;" class="well">Delete an Application</h3>
<br>
<h4 id="delete">Delete an Application</h4>
<p>
	Before deleting an application, ask yourself if you are really sure about deleting the application. This is not an action which can be undone, and deleting an application will also delete all data associated with it including all the tours for that application. 
</p>
<p>
	Once you are absolutely sure you wish to delete an application, you can delete it by:
	<ol>
		<li><a href="/login" target="_blank">Login</a> to Easy Product Tours.</li>
		<li>Once, you are at your <a href="/dash" target="_blank">EPT Dashboard</a>, click the tab for the app you wish to edit.</li>
		<li>Click the 'Delete Application' button at the bottom of the page.</li>
		<li>A modal dialog should popup asking you to confirm if you really wish to delete the application. This is your last opportunity to back out of deleting the application.</li>
		<li>If you decide against deleting the application, click the 'Don't Delete' button.</li>
		<li>If you decide to delete the application, click the 'Delete Application' button. Your application should now be deleted.</li>
	</ol>
</p>
<p>
	<strong>NOTE:</strong> In case you accidentally deleted an application, you can write to us at <a href="mailto:support@easyproducttours.com">support@easyproducttours.com</a>, and we will try to recover it for you. However, we cannot guarantee that your application data can be recovered, so it is best to tread carefully while deleting applications.
</p>