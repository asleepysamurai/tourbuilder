<div id="verify" class="page container">
	<div class="row-fluid">
		<div class="span12 form-verify">			
			<form class="form-horizontal">
				<div class="control-group">
					<h3 class="form-header control-label">Account Activation</h3>
				</div>
				<div class="control-group" id="cg-code">
					<label class="control-label" for="i-code">Activation Code</label>
					<div class="controls">
						<input type="text" id="i-code" class="code" autocorrect="off" autocapitalize="off" value="{{code}}">
						<small><span class="help-inline" id="help-code">An email has been sent to you containing this code. If you did not receive it, try <a href="/resendactivation">resending</a>.</span></small>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn btn-success">Activate</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div id="processing">
				<span class="spinner-text">Processing... Please wait... </span>
			</div>
		</div>
	</div>
</div>