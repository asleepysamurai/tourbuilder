<div id="login" class="page container">
	<div class="row-fluid">
		<div class="span6">
		</div>
		<div class="span6 form-login">
			<form class="form-horizontal" id="login" novalidate="novalidate">
				<div class="control-group">
					<h3 class="form-header control-label">Login</h3>
				</div>
				<div class="control-group" id="cg-email-l">
					<label class="control-label" for="i-email-l">Email</label>
					<div class="controls">
						<input type="email" id="i-email-l" placeholder="Email" class="email login" autocorrect="off" autocapitalize="off">
						<small><span class="help-inline" id="help-email-l"></span></small>
					</div>
				</div>
				<div class="control-group" id="cg-password-l">
					<label class="control-label" for="i-password-l">Password</label>
					<div class="controls">
						<input type="password" id="i-password-l" placeholder="Password" class="password login" autocorrect="off" autocapitalize="off">
						<small><span class="help-inline" id="help-password-l"></span></small>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="hidden" id="i-next" class="next login" value="{{next}}">
						<button type="submit" class="btn btn-success">Login</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div id="processing">
				<span class="spinner-text">Processing... Please wait... </span>
			</div>
		</div>
	</div>
</div>