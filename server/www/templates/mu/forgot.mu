<div id="forgotpassword" class="page container">
	<div class="row-fluid">
		<div class="span12 form-forgotpassword">
			<form class="form-horizontal" novalidate="novalidate">
				<div class="control-group">
					<h3 class="form-header control-label">Reset Password</h3>
				</div>
				<div class="control-group"  id="cg-email">
					<label class="control-label" for="i-email">Email</label>
					<div class="controls">
						<input type="email" id="i-email" class="email" autocorrect="off" autocapitalize="off">
						<small><span class="help-inline" id="help-email"></span></small>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn btn-success">Send Reset Code</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div id="action-msg">
				Reset code will be sent to your email within 10 minutes. Follow the instructions in the email to reset your password. Please check your SPAM folder also.
			</div>
			<div id="processing">
				<span class="spinner-text">Processing... Please wait... </span>
			</div>
		</div>
	</div>
</div>