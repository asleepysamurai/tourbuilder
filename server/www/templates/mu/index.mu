<div id="landing" class="page">
	<div class="hero nomargin" style="padding: 40px 10px; text-align:center">
		<div class="container row-fluid">
			<div class="span12">
				<div class="pull-right">
					<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.easyproducttours.com" data-text="Easy Product Tours for your webapp - increase user engagement in minutes." data-via="eptours" data-count="none" data-hashtags="eptours" data-dnt="true">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
				<div class="clearfix"></div>
				<img src="/img/landing.png" />
			</div>
		</div>
		<div class="container row-fluid">
			<div class="span12">
				<h2>Drag, drop, type, done!</h2>
				<p class="lead">Create an interactive product tour for your webapp and increase user engagement in minutes.</p>
				<div class="action">
					<a href="/pricing" class="btn btn-success btn-large">Get Started Now!</a><br>
					<a href="/docs/general/editor-demo" target="_blank">or try the editor demo</a>
				</div>
			</div>
		</div>
	</div>
	<div class="well nomargin" id="features">
		<div class="container">
			<div class="row-fluid">
				<div class="span4">
					<img src="/img/wysiwyg.png" />
				</div>
				<div class="span8">
					<br>
					<h3>Easy to use WYSIWYG Editor</h3>
					<p>What You See Is What You Get (WYSIWYG) editor displays how your tour will look to your users, as you edit it. Making an interactive product tour has never been so easy.</p>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span8">
					<br>
					<h3>Drag-Drop Repositioning and Resizing</h3>
					<p>Just click and drag your tour steps into place on the screen to position them. Click and drag an edge of your teour step to resize it. Easy and Simple.</p>
				</div>
				<div class="span4">
					<img src="/img/reposition.png" />
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4">
					<img src="/img/markdown.png" />
				</div>
				<div class="span8">
					<br>
					<h3>Markdown Supported</h3>
					<p>Control the formatting of your tour steps using Markdown. Writing Markdown is almost as easy as writing English.</p>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span8">
					<br>
					<h3>Multipage tours and Multiple tours</h3>
					<p>At times, one tour may need to span across multiple pages. At other times, you may need multiple tours to demonstrate functionality in a single page. Easy Tour Builer makes it easy to build both multipage tours and multiple tours on the same page.</p>
				</div>
				<div class="span4">
					<img src="/img/multitours.png" />
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4">
					<img src="/img/customize.png" />
				</div>
				<div class="span8">
					<br>
					<h3>Customize tour appearance</h3>
					<p>Use your own custom CSS to completely control the look and feel of your tours. Your users will never know that you're using an external tour player. If you are using Twitter Bootstrap, Easy Product Tours uses your site's Bootstrap theme out of the box.</p>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span8">
					<br>
					<h3>Custom events</h3>
					<p>Trigger custom events whenever the user interacts with your tour. You can use these events, to determine which action your app should perform next.</p>
				</div>
				<div class="span4">
					<img src="/img/trigger.png" />
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4">
					<img src="/img/pc.png" />
				</div>
				<div class="span8">
					<br>
					<h3>Programatically controllable tours</h3>
					<p>You can completely control the tour player using our Javascript API. Play, pause, stop, show the next or previous step or skip to any step - at any time, from anywhere in your app.</p>
				</div>
			</div>
		</div>
	</div>
</div>