echo "Making production version of client"
cd /media/sda7/tools/wamp/www/t/client/

echo "Minifying JS"
MIN="/media/sda7/tools/yui/yuicompressor.jar"
echo "Minifying Library JS"
cd editor/js/lib
java -jar $MIN --type js jquery.js > jquery.min.js
java -jar $MIN --type js jquery-ui-1.9.2.custom.js > jquery-ui-1.9.2.custom.min.js
java -jar $MIN --type js bootstrap.js > bootstrap.min.js
java -jar $MIN --type js markdown.js > markdown.min.js
echo "Minifying App JS"
cd ../app
java -jar $MIN --type js persistanceManager.js > persistanceManager.min.js
java -jar $MIN --type js api.js > api.min.js
java -jar $MIN --type js ui.js > ui.min.js
java -jar $MIN --type js modal.js > modal.min.js
java -jar $MIN --type js tour.js > tour.min.js
java -jar $MIN --type js step.js > step.min.js
echo "Minifying CSS"
cd ../../css
java -jar $MIN --type css jquery-ui-1.9.2.custom.css > jquery-ui-1.9.2.custom.min.css
java -jar $MIN --type css bootstrap.css > bootstrap.min.css
java -jar $MIN --type css player.bootstrap.css > player.bootstrap.min.css
java -jar $MIN --type css app.css > app.min.css

echo "Minifying Player"
cd /media/sda7/tools/wamp/www/t/client/player
java -jar $MIN --type js demoplayer.js > demoplayer.min.js
java -jar $MIN --type js player.js > player.min.js
java -jar $MIN --type css bootstrap.css > bootstrap.min.css
java -jar $MIN --type css player.css > player.min.css

echo "Minification done"
echo "Modifying references to minified versions"
cd /media/sda7/tools/wamp/www/t/client/
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/jquery.js/jquery.min.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/jquery-ui-1.9.2.custom.js/jquery-ui-1.9.2.custom.min.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/jquery-ui-1.9.2.custom.css/jquery-ui-1.9.2.custom.min.css/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/bootstrap.js/bootstrap.min.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/bootstrap.css/bootstrap.min.css/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/player.bootstrap.css/player.bootstrap.min.css/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/persistanceManager.js/persistanceManager.min.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/api.js/api.min.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/ui.js/ui.min.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/modal.js/modal.min.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/tour.js/tour.min.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/step.js/step.min.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/markdown.js/markdown.min.js/g'
find . -name "*.js" -print0 | xargs -0 sed -i '' -e 's/app.css/app.min.css/g'
echo "Modification done"

echo "Client Done"

echo "Making production version of www"
cd /media/sda7/tools/wamp/www/t/server/www/

echo "Minifying static JS"
cd static/js
java -jar $MIN --type js jquery.js > jquery.min.js
java -jar $MIN --type js bootstrap.js > bootstrap.min.js
java -jar $MIN --type js mailcheck.js > mailcheck.min.js
java -jar $MIN --type js prettify.js > prettify.min.js
java -jar $MIN --type js spin.js > spin.min.js
echo "Minifying static CSS"
cd ../css
java -jar $MIN --type css bootstrap-responsive.css > bootstrap-responsive.min.css
java -jar $MIN --type css cosmo.css > cosmo.min.css
java -jar $MIN --type css prettify.css > prettify.min.css
java -jar $MIN --type css app.css > app.min.css
java -jar $MIN --type css blog.css > blog.min.css
echo "Minification Done"

echo "Modifying references to minified versions"
cd /media/sda7/tools/wamp/www/t/server/www/
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/jquery.js/jquery.min.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/bootstrap.js/bootstrap.min.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/mailcheck.js/mailcheck.min.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/prettify.js/prettify.min.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/spin.js/spin.min.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/bootstrap-responsive.css/bootstrap-responsive.min.css/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/app.css/app.min.css/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/blog.css/blog.min.css/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/cosmo.css/cosmo.min.css/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/prettify.css/prettify.min.css/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/demoplayer.js/demoplayer.min.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/player.js/player.min.js/g'
find . -name "*.mu" -print0 | xargs -0 sed -i '' -e 's/player.css/player.min.css/g'
echo "Modification done"

echo "www Done"

echo "Minifying Store CSS"
cd /media/sda7/tools/wamp/www/t/fsstore
java -jar $MIN --type css store.css > store.min.css
find . -name "*.xhtml" -print0 | xargs -0 sed -i '' -e 's/store.css/store.min.css/g'
echo "Store Done"

echo "All Done. Production ready."