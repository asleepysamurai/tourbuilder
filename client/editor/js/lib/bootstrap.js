/* ===================================================
 * bootstrap-transition.js v2.2.2
 * http://twitter.github.com/bootstrap/javascript.html#transitions
 * ===================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


  /* CSS TRANSITION SUPPORT (http://www.modernizr.com/)
   * ======================================================= */

  $(function () {

    $.support.transition = (function () {

      var transitionEnd = (function () {

        var el = document.createElement('bootstrap')
          , transEndEventNames = {
               'WebkitTransition' : 'webkitTransitionEnd'
            ,  'MozTransition'    : 'transitionend'
            ,  'OTransition'      : 'oTransitionEnd otransitionend'
            ,  'transition'       : 'transitionend'
            }
          , name

        for (name in transEndEventNames){
          if (el.style[name] !== undefined) {
            return transEndEventNames[name]
          }
        }

      }())

      return transitionEnd && {
        end: transitionEnd
      }

    })()

  })

}(window[document.head.getAttribute('data-eptEditorNamespace')].jQuery);
/* =========================================================
 * bootstrap-tbimodal.min.js v2.2.2
 * http://twitter.github.com/bootstrap/javascript.html#tbimodals
 * =========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */


!function ($) {

  "use strict"; // jshint ;_;


 /* MODAL CLASS DEFINITION
  * ====================== */

  var tbiModal = function (element, options) {
    this.options = options
    this.$element = $(element)
      .delegate('[data-dismiss="modal"]', 'click.dismiss.tbimodal', $.proxy(this.hide, this))
    this.options.remote && this.$element.find('.modal-body').load(this.options.remote)
  }

  tbiModal.prototype = {

      constructor: tbiModal

    , toggle: function () {
        return this[!this.isShown ? 'show' : 'hide']()
      }

    , show: function () {
        var that = this
          , e = $.Event('show')

        this.$element.trigger(e)

        if (this.isShown || e.isDefaultPrevented()) return

        this.isShown = true

        this.escape()

        this.backdrop(function () {
          var transition = $.support.transition && that.$element.hasClass('fade')

          if (!that.$element.parent().length) {
            that.$element.appendTo(document.body) //don't move modals dom position
          }

          that.$element
            .show()

          if (transition) {
            that.$element[0].offsetWidth // force reflow
          }

          that.$element
            .addClass('in')
            .attr('aria-hidden', false)

          that.enforceFocus()

          transition ?
            that.$element.one($.support.transition.end, function () { that.$element.focus().trigger('shown') }) :
            that.$element.focus().trigger('shown')

        })
      }

    , hide: function (e) {
        e && e.preventDefault()

        var that = this

        e = $.Event('hide')

        this.$element.trigger(e)

        if (!this.isShown || e.isDefaultPrevented()) return

        this.isShown = false

        this.escape()

        $(document).off('focusin.tbimodal')

        this.$element
          .removeClass('in')
          .attr('aria-hidden', true)

        $.support.transition && this.$element.hasClass('fade') ?
          this.hideWithTransition() :
          this.hidetbiModal()
      }

    , enforceFocus: function () {
        var that = this
        $(document).on('focusin.tbimodal', function (e) {
          if (that.$element[0] !== e.target && !that.$element.has(e.target).length) {
            that.$element.focus()
          }
        })
      }

    , escape: function () {
        var that = this
        if (this.isShown && this.options.keyboard) {
          this.$element.on('keyup.dismiss.tbimodal', function ( e ) {
            e.which == 27 && that.hide()
          })
        } else if (!this.isShown) {
          this.$element.off('keyup.dismiss.tbimodal')
        }
      }

    , hideWithTransition: function () {
        var that = this
          , timeout = setTimeout(function () {
              that.$element.off($.support.transition.end)
              that.hidetbiModal()
            }, 500)

        this.$element.one($.support.transition.end, function () {
          clearTimeout(timeout)
          that.hidetbiModal()
        })
      }

    , hidetbiModal: function (that) {
        this.$element
          .hide()
          .trigger('hidden')

        this.backdrop()
      }

    , removeBackdrop: function () {
        this.$backdrop.remove()
        this.$backdrop = null
      }

    , backdrop: function (callback) {
        var that = this
          , animate = this.$element.hasClass('fade') ? 'fade' : ''

        if (this.isShown && this.options.backdrop) {
          var doAnimate = $.support.transition && animate

          this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
            .appendTo($('#tour-builder-modal'));

          this.$backdrop.click(
            this.options.backdrop == 'static' ?
              $.proxy(this.$element[0].focus, this.$element[0])
            : $.proxy(this.hide, this)
          )

          if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

          this.$backdrop.addClass('in')

          doAnimate ?
            this.$backdrop.one($.support.transition.end, callback) :
            callback()

        } else if (!this.isShown && this.$backdrop) {
          this.$backdrop.removeClass('in')

          $.support.transition && this.$element.hasClass('fade')?
            this.$backdrop.one($.support.transition.end, $.proxy(this.removeBackdrop, this)) :
            this.removeBackdrop()

        } else if (callback) {
          callback()
        }
      }
  }


 /* MODAL PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.tbimodal

  $.fn.tbimodal = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tbimodal')
        , options = $.extend({}, $.fn.tbimodal.defaults, $this.data(), typeof option == 'object' && option)
      if (!data) $this.data('tbimodal', (data = new tbiModal(this, options)))
      if (typeof option == 'string') data[option]()
      else if (options.show) data.show()
    })
  }

  $.fn.tbimodal.defaults = {
      backdrop: true
    , keyboard: true
    , show: true
  }

  $.fn.tbimodal.Constructor = tbiModal


 /* MODAL NO CONFLICT
  * ================= */

  $.fn.tbimodal.noConflict = function () {
    $.fn.tbimodal = old
    return this
  }


 /* MODAL DATA-API
  * ============== */

  $(document).on('click.tbimodal.data-api', '[data-toggle="modal"]', function (e) {
    var $this = $(this)
      , href = $this.attr('href')
      , $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) //strip for ie7
      , option = $target.data('tbimodal') ? 'toggle' : $.extend({ remote:!/#/.test(href) && href }, $target.data(), $this.data())

    e.preventDefault()

    $target
      .tbimodal(option)
      .one('hide', function () {
        $this.focus()
      })
  })

}(window[document.head.getAttribute('data-eptEditorNamespace')].jQuery);

/* ============================================================
 * bootstrap-tbidropdown.js v2.2.2
 * http://twitter.github.com/bootstrap/javascript.html#tbidropdowns
 * ============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* DROPDOWN CLASS DEFINITION
  * ========================= */

  var toggle = '[data-toggle=tbidropdown]'
    , tbiDropdown = function (element) {
        var $el = $(element).on('click.tbidropdown.data-api', this.toggle)
        $('html').on('click.tbidropdown.data-api', function () {
          $el.parent().removeClass('open')
        })
      }

  tbiDropdown.prototype = {

    constructor: tbiDropdown

  , toggle: function (e) {
      var $this = $(this)
        , $parent
        , isActive

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)

      isActive = $parent.hasClass('open')

      clearMenus()

      if (!isActive) {
        $parent.toggleClass('open')
      }

      $this.focus()

      return false
    }

  , keydown: function (e) {
      var $this
        , $items
        , $active
        , $parent
        , isActive
        , index

      if (!/(38|40|27)/.test(e.keyCode)) return

      $this = $(this)

      e.preventDefault()
      e.stopPropagation()

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)

      isActive = $parent.hasClass('open')

      if (!isActive || (isActive && e.keyCode == 27)) return $this.click()

      $items = $('[role=menu] li:not(.divider):visible a', $parent)

      if (!$items.length) return

      index = $items.index($items.filter(':focus'))

      if (e.keyCode == 38 && index > 0) index--                                        // up
      if (e.keyCode == 40 && index < $items.length - 1) index++                        // down
      if (!~index) index = 0

      $items
        .eq(index)
        .focus()
    }

  }

  function clearMenus() {
    $(toggle).each(function () {
      getParent($(this)).removeClass('open')
    })
  }

  function getParent($this) {
    var selector = $this.attr('data-target')
      , $parent

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    $parent = $(selector)
    $parent.length || ($parent = $this.parent())

    return $parent
  }


  /* DROPDOWN PLUGIN DEFINITION
   * ========================== */

  var old = $.fn.tbidropdown

  $.fn.tbidropdown = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tbidropdown')
      if (!data) $this.data('tbidropdown', (data = new tbiDropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.tbidropdown.Constructor = tbiDropdown


 /* DROPDOWN NO CONFLICT
  * ==================== */

  $.fn.tbidropdown.noConflict = function () {
    $.fn.tbidropdown = old
    return this
  }


  /* APPLY TO STANDARD DROPDOWN ELEMENTS
   * =================================== */

  $(document)
    .on('click.tbidropdown.data-api touchstart.tbidropdown.data-api', clearMenus)
    .on('click.tbidropdown touchstart.tbidropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('touchstart.tbidropdown.data-api', '.dropdown-menu', function (e) { e.stopPropagation() })
    .on('click.tbidropdown.data-api touchstart.tbidropdown.data-api'  , toggle, tbiDropdown.prototype.toggle)
    .on('keydown.tbidropdown.data-api touchstart.tbidropdown.data-api', toggle + ', [role=menu]' , tbiDropdown.prototype.keydown)

}(window[document.head.getAttribute('data-eptEditorNamespace')].jQuery);
/* ===========================================================
 * bootstrap-tbitooltip.js v2.1.1
 * http://twitter.github.com/bootstrap/javascript.html#tbitooltips
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ===========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* TOOLTIP PUBLIC CLASS DEFINITION
  * =============================== */

  var tbiTooltip = function (element, options) {
    this.init('tbitooltip', element, options)
  }

  tbiTooltip.prototype = {

    constructor: tbiTooltip

  , init: function (type, element, options) {
      var eventIn
        , eventOut

      this.type = type
      this.$element = $(element)
      this.options = this.getOptions(options)
      this.enabled = true

      if (this.options.trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (this.options.trigger != 'manual') {
        eventIn = this.options.trigger == 'hover' ? 'mouseenter' : 'focus'
        eventOut = this.options.trigger == 'hover' ? 'mouseleave' : 'blur'
        this.$element.on(eventIn + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }

      this.options.selector ?
        (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
        this.fixTitle()
    }

  , getOptions: function (options) {
      options = $.extend({}, $.fn[this.type].defaults, options, this.$element.data())

      if (options.delay && typeof options.delay == 'number') {
        options.delay = {
          show: options.delay
        , hide: options.delay
        }
      }

      return options
    }

  , enter: function (e) {
      var self = $(e.currentTarget)[this.type](this._options).data(this.type)

      if (!self.options.delay || !self.options.delay.show) return self.show()

      clearTimeout(this.timeout)
      self.hoverState = 'in'
      this.timeout = setTimeout(function() {
        if (self.hoverState == 'in') self.show()
      }, self.options.delay.show)
    }

  , leave: function (e) {
      var self = $(e.currentTarget)[this.type](this._options).data(this.type)

      if (this.timeout) clearTimeout(this.timeout)
      if (!self.options.delay || !self.options.delay.hide) return self.hide()

      self.hoverState = 'out'
      this.timeout = setTimeout(function() {
        if (self.hoverState == 'out') self.hide()
      }, self.options.delay.hide)
    }

    , show: function () {
      var $tip
        , inside
        , pos
        , actualWidth
        , actualHeight
        , placement
        , tp
        , wp
        , $arrow
        , ap
        , apFlags = {}

      if (this.hasContent() && this.enabled) {
        $tip = this.tip()        
        this.setContent()

        if (this.options.animation) {
          $tip.addClass('fade')
        }

        placement = typeof this.options.placement == 'function' ?
          this.options.placement.call(this, $tip[0], this.$element[0]) :
          this.options.placement

        inside = /in/.test(placement)

        $tip
          .remove()
          .css({ top: 0, left: 0, display: 'block' })
          .appendTo(inside ? this.$element : this.options.attachTo ? this.options.attachTo : document.body) 
          .attr('id', this.options.id)

        if(this.options.onShow) this.options.onShow();
        if(this.options.onClick) $tip.on('click', this.options.onClick);

        pos = this.getPosition(inside)

        //Bugfix - jQuery bug: if target element is fixed position or relative to a fixed position, then top value is wrongly reported
        //Well technically its the correct value, but for our purposes its wrong
        // To correct it, remove window.scrollTop from reported position
        if(this.$element[0] != window && !this.options.maintainPosition){
          if(this.$element.parents().filter(function(){ return $(this).css('position') == 'fixed'; }).length > 0)
            pos.top -= $(window).scrollTop()
        }

        if(this.options.frame && this.options.frame.width){
          var $b = $('body')
          var vpw = $(window).width() - (parseInt($b.css('padding-left')) || 0) - (parseInt($b.css('padding-right')) || 0)
          if(this.options.frame.width < vpw)
            $tip.width(this.options.frame.width)
        else
          $tip.width(vpw)
        }


        actualWidth = $tip[0].offsetWidth
        actualHeight = $tip[0].offsetHeight

        wp = {height: $(document).height(), width: $(document).width()}

        var wasAuto = placement.indexOf('auto') != -1;
        var wasCustom = placement.indexOf('custom') != -1;
        if (inside ? placement.split(' ')[1] : placement == 'auto') {
            if (pos.left + pos.width + actualWidth < wp.width)
              placement = placement.replace('auto', 'right')
            else if (pos.left - actualWidth > 200)
              placement = placement.replace('auto', 'left')
            else if (pos.top + pos.height + actualHeight < wp.height)
              placement = placement.replace('auto', 'bottom')
            else if (pos.top - actualHeight > 65)
              placement = placement.replace('auto', 'top')
            else
              placement = placement.replace('auto', 'center')
        }

        switch(this.options.arrow && this.options.arrow.d){
          case 1:
            placement = 'bottom';
            break;
          case 2:
            placement = 'top';
            break;
          case 3:
            placement = 'right';
            break;
          case 4:
            placement = 'left';
            break;
        }

        if(wasCustom){
          tp = this.options.frame
        }
        else{
          switch (inside ? placement.split(' ')[1] : placement) {
            case 'bottom':
              tp = {top: pos.top + pos.height, left: pos.left + pos.width / 2 - actualWidth / 2}
              apFlags.left = true
              break
            case 'top':
              tp = {top: pos.top - actualHeight - 10, left: pos.left + pos.width / 2 - actualWidth / 2}
              apFlags.left = true
              break
            case 'left':
              tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth - 10}
              apFlags.top = true
              break
            case 'right':
              tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width}
              apFlags.top = true
              break
            case 'center':
              tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width / 2 - actualWidth / 2}
              break
          }
        }

        if(wasAuto){
          if(tp.top + actualHeight > wp.height)
            tp.top = ( wp.height - actualHeight )//(pos.height > actualHeight) ? ( pos.top + (wp.height - pos.top) / 2 - actualHeight / 2 ) : ( wp.height - actualHeight );
          if(tp.top < 57)
            tp.top = 57
          if(tp.left + actualWidth > wp.width)
            tp.left = ( wp.width - actualWidth )//(pos.width > actualWidth) ? ( pos.left + (wp.width - pos.left) / 2 - actualWidth / 2 ) : ( wp.width - actualWidth );
          if(tp.left < 15)
            tp.left = 15
        }
        
        $arrow = $('.arrow', $tip)

        if(!wasAuto && this.options.arrow && this.options.arrow.p && this.options.arrow.d){
          var dir, pos, ah;
          if(this.options.arrow.d == 1 || this.options.arrow.d == 2)
            dir = 'left', pos = actualWidth;
          else
            dir = 'top', pos = actualHeight;
          
          switch(this.options.arrow.p){
            case 1:
              $arrow.css(dir, 17);
              break;
            case 2:
              $arrow.css(dir, pos/2);
              break;
            case 3:
              $arrow.css(dir, pos - 17);
              break;
          }

        }
        else{
          if(apFlags.top && actualHeight > pos.height)
            $arrow.css('top', pos.top + pos.height / 2 - tp.top)
          if(apFlags.left && actualWidth > pos.width)
            $arrow.css('left', pos.left + pos.width / 2 - tp.left)
        }

        if(!(this.options.arrow && this.options.arrow.p && this.options.arrow.d)){
          ap = $arrow.position()
          if(ap && (ap.top < 0 || ap.left < 0 || ap.left + 10 > pos.left + pos.width || ap.top + 10 > pos.top + pos.width))
            $arrow.css('display', 'none')
        }

        tp['max-width'] = 'none';

        $tip
          .css(tp)
          .addClass(placement)
          .addClass('in')
/*
         if(this.options.hideAfter){
          var that = this;
          window.setTimeout(function(){that.hide(that)},that.options.hideAfter)
         }
*/
      }
    }

  , setContent: function () {
      var $tip = this.tip()
        , title = this.getTitle()

      $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
      $tip.removeClass('fade in top bottom left right')
    }

  , hide: function (_this) {
      var that = _this ? _this : this
        , $tip = this.tip()

      $tip.removeClass('in')

      function removeWithAnimation() {
        var timeout = setTimeout(function () {
          $tip.off($.support.transition.end).remove()
        }, 500)

        $tip.one($.support.transition.end, function () {
          clearTimeout(timeout)
          $tip.remove()
        })
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        removeWithAnimation() :
        $tip.remove()

      return this
    }

  , fixTitle: function () {
      var $e = this.$element
      if ($e.attr('title') || typeof($e.attr('data-original-title')) != 'string') {
        $e.attr('data-original-title', $e.attr('title') || '').removeAttr('title')
      }
    }

  , hasContent: function () {
      return this.getTitle()
    }

  , getPosition: function (inside) {
      return this.$element[0] == window ? 
       $.extend({}, {top: 0, left: 0}, { width: this.$element.width(), height: this.$element.height() }) :
       $.extend({}, (inside ? 
        {top: 0, left: 0} : 
        this.$element.offset()), { width: this.$element[0].offsetWidth , height: this.$element[0].offsetHeight })
    }

  , getTitle: function () {
      var title
        , $e = this.$element
        , o = this.options

      title = $e.attr('data-original-title')
        || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

      return title
    }

  , tip: function () {
      return this.$tip = this.$tip || $(this.options.template)
    }

  , validate: function () {
      if (!this.$element[0].parentNode) {
        this.hide()
        this.$element = null
        this.options = null
      }
    }

  , enable: function () {
      this.enabled = true
    }

  , disable: function () {
      this.enabled = false
    }

  , toggleEnabled: function () {
      this.enabled = !this.enabled
    }

  , toggle: function () {
      this[this.tip().hasClass('in') ? 'hide' : 'show']()
    }

  , destroy: function () {
      this.hide().$element.off('.' + this.type).removeData(this.type)
    }

  }


 /* TOOLTIP PLUGIN DEFINITION
  * ========================= */

  $.fn.tbitooltip = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tbitooltip')
        , options = typeof option == 'object' && option
      if (!data) $this.data('tbitooltip', (data = new tbiTooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tbitooltip.Constructor = tbiTooltip

  $.fn.tbitooltip.defaults = {
    animation: true
  , placement: 'top'
  , selector: false
  , template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
  , trigger: 'hover'
  , title: ''
  , delay: 0
  , html: true
  }

}(window[document.head.getAttribute('data-eptEditorNamespace')].jQuery);
/* ===========================================================
 * bootstrap-tbipopover.js v2.1.1
 * http://twitter.github.com/bootstrap/javascript.html#tbipopovers
 * ===========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* POPOVER PUBLIC CLASS DEFINITION
  * =============================== */

  var tbiPopover = function (element, options) {
    this.init('tbipopover', element, options)
  }


  /* NOTE: POPOVER EXTENDS BOOTSTRAP-TOOLTIP.js
     ========================================== */

  tbiPopover.prototype = $.extend({}, $.fn.tbitooltip.Constructor.prototype, {

    constructor: tbiPopover

  , setContent: function () {
      var $tip = this.tip()
        , title = this.getTitle()
        , content = this.getContent()
        , footer = this.getFooter()

      $tip.find('.popover-title:first')[this.options.html ? 'html' : 'text'](title)
      $tip.find('.popover-content')[this.options.html ? 'html' : 'text'](content)
      $tip.find('.popover-footer')[this.options.html ? 'html' : 'text'](footer);

      $tip.removeClass('fade top bottom left right in')
    }

  , hasContent: function () {
      return this.getTitle() || this.getContent()
    }

  , getContent: function () {
      var content
        , $e = this.$element
        , o = this.options

      content = $e.attr('data-content')
        || (typeof o.content == 'function' ? o.content.call($e[0]) :  o.content)

      return content
    }

  , getFooter: function () {
      var footer
        , $e = this.$element
        , o = this.options

      footer = $e.attr('data-footer')
        || (typeof o.footer == 'function' ? o.footer.call($e[0]) :  o.footer)

      return footer
    }

  , tip: function () {
      if (!this.$tip) {
        this.$tip = $(this.options.template)
      }
      return this.$tip
    }

  , destroy: function () {
      if(this.options.onHide) this.options.onHide(this.$element.data('val'));
      this.hide().$element.off('.' + this.type).removeData(this.type)
    }

  })


 /* POPOVER PLUGIN DEFINITION
  * ======================= */

  $.fn.tbipopover = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tbipopover')
        , options = typeof option == 'object' && option
      if (!data) $this.data('tbipopover', (data = new tbiPopover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tbipopover.Constructor = tbiPopover

  $.fn.tbipopover.defaults = $.extend({} , $.fn.tbitooltip.defaults, {
    placement: 'right'
  , trigger: 'click'
  , content: ''
  , template: '<div class="popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div><div class="popover-footer"></div></div></div>'
  })

}(window[document.head.getAttribute('data-eptEditorNamespace')].jQuery);
/* ==========================================================
 * bootstrap-tbialert.js v2.2.2
 * http://twitter.github.com/bootstrap/javascript.html#tbialerts
 * ==========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* ALERT CLASS DEFINITION
  * ====================== */

  var dismiss = '[data-dismiss="alert"]'
    , tbiAlert = function (el) {
        $(el).on('click', dismiss, this.close)
      }

  tbiAlert.prototype.close = function (e) {
    var $this = $(this)
      , selector = $this.attr('data-target')
      , $parent

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    $parent = $(selector)

    e && e.preventDefault()

    $parent.length || ($parent = $this.hasClass('alert') ? $this : $this.parent())

    $parent.trigger(e = $.Event('close'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      $parent
        .trigger('closed')
        .remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent.on($.support.transition.end, removeElement) :
      removeElement()
  }


 /* ALERT PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.tbialert

  $.fn.tbialert = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tbialert')
      if (!data) $this.data('tbialert', (data = new tbiAlert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.tbialert.Constructor = tbiAlert


 /* ALERT NO CONFLICT
  * ================= */

  $.fn.tbialert.noConflict = function () {
    $.fn.tbialert = old
    return this
  }


 /* ALERT DATA-API
  * ============== */

  $(document).on('click.tbialert.data-api', dismiss, tbiAlert.prototype.close)

}(window[document.head.getAttribute('data-eptEditorNamespace')].jQuery);
/* ============================================================
 * bootstrap-tbibutton.js v2.2.2
 * http://twitter.github.com/bootstrap/javascript.html#tbibuttons
 * ============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* BUTTON PUBLIC CLASS DEFINITION
  * ============================== */

  var tbiButton = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.tbibutton.defaults, options)
  }

  tbiButton.prototype.setState = function (state) {
    var d = 'disabled'
      , $el = this.$element
      , data = $el.data()
      , val = $el.is('input') ? 'val' : 'html'

    state = state + 'Text'
    data.resetText || $el.data('resetText', $el[val]())

    $el[val](data[state] || this.options[state])

    // push to event loop to allow forms to submit
    setTimeout(function () {
      state == 'loadingText' ?
        $el.addClass(d).attr(d, d) :
        $el.removeClass(d).removeAttr(d)
    }, 0)
  }

  tbiButton.prototype.toggle = function () {
    var $parent = this.$element.closest('[data-toggle="tbibuttons-radio"]')

    $parent && $parent
      .find('.active')
      .removeClass('active')

    this.$element.toggleClass('active')
  }


 /* BUTTON PLUGIN DEFINITION
  * ======================== */

  var old = $.fn.tbibutton

  $.fn.tbibutton = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tbibutton')
        , options = typeof option == 'object' && option
      if (!data) $this.data('tbibutton', (data = new tbiButton(this, options)))
      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  $.fn.tbibutton.defaults = {
    loadingText: 'loading...'
  }

  $.fn.tbibutton.Constructor = tbiButton


 /* BUTTON NO CONFLICT
  * ================== */

  $.fn.tbibutton.noConflict = function () {
    $.fn.tbibutton = old
    return this
  }


 /* BUTTON DATA-API
  * =============== */

  $(document).on('click.tbibutton.data-api', '[data-toggle^=tbibutton]', function (e) {
    var $btn = $(e.target)
    if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
    $btn.tbibutton('toggle')
  })

}(window[document.head.getAttribute('data-eptEditorNamespace')].jQuery);