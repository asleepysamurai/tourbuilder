(function(namespace){
	var $ = window[namespace].jQuery
		, persistanceManager = window[namespace].persistanceManager;
	var ui = $.extend(true, window[namespace].ui, {
		init: function(){
			var $db = $(document.body);
			$db.find('*')
				.filter(function() { var $t = $(this); var pos = $t.position(), off = $t.offset(); return (off.top == 0) })
				.css({top: '+=42px'});

			var $tbc = $('<div id="tour-builder-container" class="tour-builder-container"><div id="tour-builder-interface" class="navbar"></div></div>').prependTo($db);
			$('<div id="tour-builder-popover-container"></div>').prependTo($db);
			$('<div id="tour-builder-spacer"></div>').prependTo($db);
			var $tbi = $('<div class="navbar-inner"><span class="brand"><span id="tour-builder-tour-title"></span>Easy Product Tours</span></div>').prependTo($tbc.find('.navbar'));

			$('<div class="pull-left" id="tour-builder-left-buttons"><div class="btn-group"><button id="tour-builder-login" class="btn btn-inverse" disabled="true">Login</button></div><div class="btn-group"><button id="tour-builder-add-tour" class="btn" disabled="true">New Tour</button><button id="tour-builder-delete-tour" class="btn btn-danger" disabled="true">Delete</button></div><div class="btn-group" id="tour-builder-switch-tour-container"><button id="tour-builder-switch-tour" class="btn dropdown-toggle" data-toggle="tbidropdown" disabled="true">Switch Tour&nbsp;&nbsp;<span class="caret"></span></button><ul class="dropdown-menu" id="tour-builder-switch-tour-dropdown-menu"></ul></div></div>').prependTo($tbi);
			$('<div class="pull-right" id="tour-builder-right-buttons"><div class="btn-group" id="tour-builder-switch-step-container"><button id="tour-builder-switch-step" class="btn dropdown-toggle" data-toggle="tbidropdown" disabled="true"><span class="caret"></span>&nbsp;&nbsp;Switch Step</button><ul class="dropdown-menu pull-right" id="tour-builder-switch-step-dropdown-menu"></ul></div><div class="btn-group"><button id="tour-builder-delete-step" class="btn btn-danger" disabled="true">Delete</button><button id="tour-builder-add-step" class="btn" disabled="true">New Step</button></div><div class="btn-group"><button id="tour-builder-previous-step" class="btn" disabled="true">Previous</button><button id="tour-builder-next-step" class="btn" disabled="true">Next</button></div></div>').prependTo($tbi);

			ui.initLoginButton();
		},
		initToursForApp: function(appData){
			var handle200 = function(data, textStatus, xhr){
				persistanceManager.clear();
				if(!persistanceManager.demo.is()){
					var tours = data.tours;
					for(var d in tours){
						tours[d].steps = JSON.parse(tours[d].steps);
						persistanceManager.tour.set(tours[d].id, tours[d]);
						persistanceManager.tour.list.add(tours[d].id);
					}
				}
				ui.initTourButtons();
				ui.initStepButtons();
			};
			if(!appData){
				window[namespace].api.tour.get(window.location.href, {
					200: handle200,
					404: function(xhr, textStatus, error){
						ui.initTourButtons();
						ui.initStepButtons();
						$('#tour-builder-add-tour').trigger('click');
					},
					500: function(xhr, textStatus, error){
						window[namespace].modal.show('error', {msg: 'An error occurred while trying to load tours for this app. This is a temporary error. Please try again later.'});
					}
				});
			}
			else
				handle200(appData);
		},
		initLoginButton: function(){
			window[namespace].api.user.loggedIn({
				200: function(data, textStatus, xhr){
					$('#tour-builder-login').prop('disabled', false).text('Logout');
					ui.logged(true, data);
				},
				401: function(xhr, textStatus, error){
					$('#tour-builder-login').prop('disabled', false).text('Login');
					window[namespace].modal.show('login');
				},
				404: function(xhr, textStatus, error){
					$('#tour-builder-login').prop('disabled', false).text('Logout');
					ui.initTourButtons();
					ui.initStepButtons();
					$('#tour-builder-add-tour').trigger('click');
				},
				500: function(xhr, textStatus, error){
					window[namespace].modal.show('error', {msg: 'An error occurred while trying to load tours for this app. This is a temporary error. Please try again later.'});
				}
			});
			$('#tour-builder-login').on('click', function(ev){
				if($(this).text() == 'Login')
					window[namespace].modal.show('login');
				else{
					window[namespace].api.user.logout({
						200: function(data, textStatus, xhr){
							ui.logged(false);
						}
					});
				}
			});
		},
		initTourButtons: function(){
			var modal = window[namespace].modal, tour = window[namespace].tour;
			$('#tour-builder-add-tour').on('click', function(ev){
				modal.show('newTour');
			}).prop('disabled', false);
			$('#tour-builder-delete-tour').on('click', function(ev){
				modal.show('deleteTour');
			});
			tour.list.load();
			tour.load(persistanceManager.tour.current());
		},
		initStepButtons: function(){
			var step = window[namespace].step;
			$('#tour-builder-add-step').on('click', function(ev){
				step.create();
			});
			$('#tour-builder-delete-step').on('click', function(ev){
				step.remove();
			});
			$('#tour-builder-next-step').on('click', function(ev){
				step.next();
			});
			$('#tour-builder-previous-step').on('click', function(ev){
				step.previous();
			});
			//step.list.load();
			//step.load(persistanceManager.step.current(tour.current()));
		},
		enableStepButtons: function(){
			$('#tour-builder-right-buttons button').prop('disabled', false);
		},
		setNavigationButtons : function(id){
			if(id == 0)
				$('#tour-builder-previous-step').prop('disabled', true);
			else
				$('#tour-builder-previous-step').prop('disabled', false);

			if(id >= persistanceManager.step.list.get(tour.current()).length-1)
				$('#tour-builder-next-step').prop('disabled', true);
			else
				$('#tour-builder-next-step').prop('disabled', false);			
		},
		logged: function(_in, data){
			if(_in){
				$('#tour-builder-login').text('Logout');
				ui.initToursForApp(data);
			}
			else{
				window[namespace].tour.unload();
				$('#tour-builder-login').text('Login');
				$('#tour-builder-left-buttons .btn').filter(':not(#tour-builder-login)').prop('disabled', true);
				$('#tour-builder-right-buttons .btn').prop('disabled', true);
			}
		}
	});
	window[namespace].ui.loaded = true;
})(document.head.getAttribute('data-eptEditorNamespace'));