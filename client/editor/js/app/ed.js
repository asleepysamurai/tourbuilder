//IE Shim for document.head
if(!document.head)
	document.head = document.getElementsByTagName('head')[0];
(function(){
	//Unload if already loaded
	var namespace = document.head.getAttribute('data-eptEditorNamespace');
	if(document.head.getAttribute('data-eptEditorInitialized')){
		document.head.removeChild(document.getElementById('tour-builder-js-loader'));
		return window[namespace].unload();
	}
	else
		document.head.setAttribute('data-eptEditorInitialized', 'true');

	var timer = Date.now();
	var $;
	var modal, ui, persistanceManager;
	function createXMLHTTPObject() {
		var XMLHttpFactories = [
			function () {return new XDomainRequest()},
			function () {return new XMLHttpRequest()}
		];
		var xmlhttp = false;
		for (var i=0;i<XMLHttpFactories.length;i++) {
			try {
				xmlhttp = XMLHttpFactories[i]();
			}
			catch (e) {
				continue;
			}
			break;
		}
		return xmlhttp;
	}
	String.prototype.nl2br = function(is_xhtml) {   
		var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
		return (this + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
	};
	String.prototype.escapeHTML = function(){
		return this.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
	};
	var createSafeNamespace = function(){
		if(namespace) return namespace;
		do namespace = Math.random();
		while(window[namespace] != undefined);
		window[namespace] = {
			unload: function(){
				console.log('Tour builder is still loading... Please wait...');
			}
		};
		document.head.setAttribute('data-eptEditorNamespace', namespace);
		return namespace;
	};
	var loadScript = function(id, url, async, css){
		if(css) async = true;
		var ed = document.createElement(css ? 'link' : 'script'); 
		ed.type = css ? 'text/css' : 'text/javascript'; 
		url = window.location.protocol + '//www.easyproducttours.com/editor/'+(css?'css':'js')+'/'+url;//+'?x='+Math.random();
		if(async){
			ed[css ? 'href' : 'src'] = url; 
			ed.async = true;
		}
		else{
			var xhr = createXMLHTTPObject();
			xhr.onload = function(){
				ed.text = xhr.responseText;
			};
			xhr.open('GET', url, false);
			xhr.send('');
		}
		ed.id='tour-builder-'+(css?'css':'js')+'-'+id;
		if(css) ed.rel = 'stylesheet';
		document.head.appendChild(ed);
	};
	var loadAllScripts = function(){
		console.log('Loading libraries...');
		//Inititalize Namespaced Modules to empty objects so that they are passed by refernce
		window[namespace].modal = {};
		window[namespace].tour = {};
		window[namespace].step = {};
		window[namespace].persistanceManager = {};
		window[namespace].ui = {};
		window[namespace].api = {};

		console.log('Loading jQuery...');
		if(!window[namespace].jQuery){
			loadScript('jquery', 'lib/jquery.min.js?x=0', false);
			if(typeof jQuery == 'undefined') return window.setTimeout(waitForjQuery, 100);
			else waitForjQuery();
		}
		else waitForjQuery();
	};
	var waitForjQuery = function(){
		console.log('Waiting for jQuery...');
		if(!window[namespace].jQuery && typeof jQuery == 'undefined') return window.setTimeout(waitForjQuery, 100);
		if(!window[namespace].jQuery)
			window[namespace].jQuery = jQuery.noConflict(true);
		console.log('Loading jQueryUI...');
		if(!window[namespace].jQuery.fn.resizable) loadScript('jqueryui', 'lib/jquery-ui-1.9.2.custom.min.js', true);
		if(!document.getElementById('tour-builder-css-jqueryui')) loadScript('jqueryui', 'jquery-ui-1.9.2.custom.min.css', false, true);

		console.log('Loading Bootstrap...');
		if(window[namespace].jQuery.fn.tbibutton == undefined)
			loadScript('bootstrap', 'lib/bootstrap.min.js', true);
		if(!document.getElementById('tour-builder-css-bootstrap')) loadScript('bootstrap', 'bootstrap.min.css', false, true);

		if(document.getElementById('tour-builder-js-loader').getAttribute('data-loadbootstrap') == 'true')
			if(!document.getElementById('tour-builder-css-playerbootstrap')) loadScript('playerbootstrap', 'player.bootstrap.min.css', false, true);

		//if(!document.getElementById('tour-builder-css-icomoon')) loadScript('icomoon', 'icomoon.css', false, true);

		console.log('Loading app scripts...');
		var $ = window[namespace].jQuery, x = Math.random();
		if($.isEmptyObject(window[namespace].persistanceManager)) loadScript('persistanceManager', 'app/persistanceManager.min.js?x='+x, true);
		if($.isEmptyObject(window[namespace].api)) loadScript('api', 'app/api.min.js?x='+x, true);
		if($.isEmptyObject(window[namespace].ui)) loadScript('ui', 'app/ui.min.js?x='+x, true);
		if($.isEmptyObject(window[namespace].modal)) loadScript('modal', 'app/modal.min.js?x='+x, true);
		if($.isEmptyObject(window[namespace].tour)) loadScript('tour', 'app/tour.min.js?x='+x, true);
		if($.isEmptyObject(window[namespace].step)) loadScript('step', 'app/step.min.js?x='+x, true);
		if($.isEmptyObject(window[namespace].markdown)) loadScript('markdown', 'lib/markdown.min.js?x='+x, true);

		if(!document.getElementById('tour-builder-css-app')) loadScript('app', 'app.min.css', false, true);

		window.setTimeout(waitForLibraries, 100);
	};
	var waitForLibraries = function(){
		console.log('Waiting for libraries...');
		if(typeof window[namespace].jQuery == 'undefined')
			return window.setTimeout(waitForLibraries, 100);
		var $ = window[namespace].jQuery;
		if(window[namespace].jQuery.fn.tbibutton == undefined || window[namespace].jQuery.fn.resizable == undefined || 
			window[namespace].step.loaded !== true ||  window[namespace].modal.loaded !== true || window[namespace].tour.loaded !== true ||
			window[namespace].persistanceManager.loaded !== true || window[namespace].ui.loaded !== true || 
			window[namespace].api.loaded !== true || window[namespace].markdown == undefined)
			window.setTimeout(waitForLibraries, 100);
		else
			importNamespacedScripts();
	};
	var importNamespacedScripts = function(){
		$ = window[namespace].jQuery;
		persistanceManager = window[namespace].persistanceManager;
		modal = window[namespace].modal;
		tour = window[namespace].tour;
		ui = window[namespace].ui;
		loadInterface();
	};
	var loadInterface = function(){
		console.log('Libraries loaded. Inititalizing interface...')

		persistanceManager.loadData();

		ui.init();
		//ui.initInterfaceActions();

		//initDataAndState();

		console.log('Tour Builder Ready.');
		console.log('Loaded in '+(Date.now()-timer)+'ms');

		//For testing only - remove in production
		$(document.body).on('tourbuilder.step.next', function(ev){
			console.log('Triggered tourbuilder.step.next from tour: '+ev.tour+' and step: '+ev.step);
		}).on('tourbuilder.step.previous', function(ev){
			console.log('Triggered tourbuilder.step.previous from tour: '+ev.tour+' and step: '+ev.step);
		});
		
		//Now bind unload to namespace
		//Binding any time before this would cause interface to be unloaded before loading.
		//Impatient users multiclicking should not cause unloading.
		window[namespace].unload = unload;
	};

	// Now that all functions have been defined, execute them
	namespace = createSafeNamespace();
	loadAllScripts();

	//Unload tour builder - called from bookmarklet
	var unload = function(clean){
		console.log('Unloading Tour Builder...');
		var $ = window[namespace].jQuery;
		$(document.body).find('*')
			.filter(function() { var $t = $(this); var pos = $t.position(), off = $t.offset(); return (off.top == 0 || off.top == pos.top) })
			.css({top: '-=42px'});

		$('#tour-builder-js-ui').remove();
		$('#tour-builder-js-modal').remove();
		$('#tour-builder-js-tour').remove();
		$('#tour-builder-js-step').remove();
		$('#tour-builder-js-persistanceManager').remove();
		$('#tour-builder-js-api').remove();
	
		$('#tour-builder-js-bootstrap').remove();
		$('#tour-builder-js-jqueryui').remove();
		$('#tour-builder-js-jquery').remove();
		$('head .tour-builder-js-loader').remove();
		$('#tour-builder-container').remove();
		$('#tour-builder-spacer').remove();

		window[namespace].persistanceManager.unloadData();
		document.head.removeAttribute('data-eptEditorInitialized');

		if(clean){
			$('#tour-builder-css-bootstrap').remove();
			$('#tour-builder-css-jqueryui').remove();
			$('#tour-builder-css-app').remove();
			document.head.removeAttribute('data-eptEditorNamespace')
			delete window[namespace];
		}
	
		console.log((clean?'Cleanly ':'')+'Done.');
	};
})();