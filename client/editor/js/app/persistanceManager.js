(function(namespace){
	var $ = window[namespace].jQuery;
	var api = window[namespace].api;
	var documentStorage = {
		setItem: function(key, val){
			key = persistanceManager._localStorageKey + '-' + key;
			if(!val) return;
			val = $.isArray(val) ? $.merge([], val) : typeof val == 'object' ? $.extend(true, {}, val) : val;
			$(document).data(key, val);
		},
		getItem: function(key){
			key = persistanceManager._localStorageKey + '-' + key;
			var val = $(document).data(key);
			val = val ? val : null;
			val = $.isArray(val) ? $.merge([], val) : val ? typeof val == 'object' ? $.extend(true, {}, val) : val : null;
			return val;
		},
		removeItem: function(key){
			key = persistanceManager._localStorageKey + '-' + key;
			$(document).removeData(key);
		}
	};
	var persistanceManager = $.extend(true, window[namespace].persistanceManager, {
		_localStorageKey: 'tour-builder-editor-data',
		clear: function(){
			localStorage.removeItem(persistanceManager._localStorageKey);
		},
		_getItemFromLocalStorage: function(name, isJSON){			
			var tbd = localStorage.getItem(persistanceManager._localStorageKey);
			tbd = tbd ? JSON.parse(tbd) : null;
			if(!tbd) return null;
			return name ? tbd[name] : tbd;
		},
		_saveItemToLocalStorage: function(name, val, isJSON){
			var tbd = persistanceManager._getItemFromLocalStorage(null, true);
			if(!tbd) tbd = {};
			tbd[name] = val;
			localStorage.setItem(persistanceManager._localStorageKey, JSON.stringify(tbd));
		},
		_removeFromLocalStorage: function(name){
			var tbd = persistanceManager._getItemFromLocalStorage(null, true);
			if(!tbd) return;
			delete tbd[name];
			localStorage.setItem(persistanceManager._localStorageKey, JSON.stringify(tbd));
		},
		_getItemFromDocumentStorage: function(name){
			return documentStorage.getItem(name);
		},
		_saveItemToDocumentStorage: function(name, val){
			documentStorage.setItem(name, val);
		},
		_removeFromDocumentStorage: function(name){
			documentStorage.removeItem(name);
		},
		_getItemFromPersistanceMedium: function(name, isJSON){
			return persistanceManager._getItemFromDocumentStorage(name+'', isJSON);
		},
		_saveItemToPersistanceMedium: function(name, val, isJSON){
			persistanceManager._saveItemToDocumentStorage(name+'', val, isJSON);
			persistanceManager._saveItemToLocalStorage(name+'', val, isJSON);
		},
		_removeFromPersistanceMedium: function(name){
			persistanceManager._removeFromDocumentStorage(name+'');
			persistanceManager._removeFromLocalStorage(name+'');
		},
		loadData: function(){
			persistanceManager._saveItemToDocumentStorage('tourCount', persistanceManager._getItemFromLocalStorage('tourCount'));
			persistanceManager._saveItemToDocumentStorage('currentTour', persistanceManager._getItemFromLocalStorage('currentTour'));
			persistanceManager._saveItemToDocumentStorage('showNext', persistanceManager._getItemFromLocalStorage('showNext', true));
			persistanceManager._saveItemToDocumentStorage('currentApp', persistanceManager._getItemFromLocalStorage('currentApp', true));
			persistanceManager._saveItemToDocumentStorage('isDemo', persistanceManager._getItemFromLocalStorage('isDemo', true));
			var list = persistanceManager._getItemFromLocalStorage('tourList', true);
			persistanceManager._saveItemToDocumentStorage('tourList', list);
			for(var l=0, len=list ? list.length : 0; l<len; ++l){
				persistanceManager._saveItemToDocumentStorage('tour-'+list[l], persistanceManager._getItemFromLocalStorage('tour-'+list[l], true));
			}
		},
		unloadData: function(){
			var list = persistanceManager._getItemFromLocalStorage('tourList', true);
			for(var l=0, len=list ? list.length : 0; l<len; ++l){
				persistanceManager._removeFromDocumentStorage('tour-'+list[l]);
			}
			persistanceManager._removeFromDocumentStorage('tourList');
			persistanceManager._removeFromDocumentStorage('tourCount');
			persistanceManager._removeFromDocumentStorage('currentTour');
			persistanceManager._removeFromDocumentStorage('showNext');
			persistanceManager._removeFromDocumentStorage('currentApp');
			persistanceManager._removeFromDocumentStorage('isDemo');
		},

		//Public APIs
		demo: {
			is: function(val){
				if(val === undefined)
					return persistanceManager._getItemFromPersistanceMedium('isDemo', true);
				else if(val === null)
					persistanceManager._removeFromPersistanceMedium('isDemo');
				else
					persistanceManager._saveItemToPersistanceMedium('isDemo', val, true);
			}
		},
		app: {
			current: function(id){
				if(id === undefined)
					return persistanceManager._getItemFromPersistanceMedium('currentApp');
				else if(id === null)
					persistanceManager._removeFromPersistanceMedium('currentApp');
				else
					persistanceManager._saveItemToPersistanceMedium('currentApp', id);
			}
		},
		tour: {
			count: function(inc){
				if(inc)
					persistanceManager._saveItemToPersistanceMedium('tourCount', persistanceManager.tour.count()+1, false);
				else{
					var c = persistanceManager._getItemFromPersistanceMedium('tourCount', false);
					return c ? parseInt(c) : 1;
				}
			},
			list: {
				add: function(id){
					var list = persistanceManager.tour.list.get();
					list = list ? list : [];
					if(list.indexOf(id) == -1)
						list.push(id);
					persistanceManager._saveItemToPersistanceMedium('tourList', list, true);
				},
				remove: function(id){
					var list = persistanceManager.tour.list.get();
					if(list){
						var i = list.indexOf(parseInt(id));
						if(i > -1) list.splice(i,1);
					}
					persistanceManager._saveItemToPersistanceMedium('tourList', list, true);
				},
				get: function(){	
					return persistanceManager._getItemFromPersistanceMedium('tourList', true);
				}
			},
			create: function(title, callback){
				//var id = persistanceManager.tour.count();
				api.tour.add(title, null, {
					201: function(data, textStatus, xhr){
						persistanceManager.tour.set(data.tour.id, data.tour);
						persistanceManager.tour.list.add(data.tour.id);
						callback(null, data.tour.id);
					},
					401: function(xhr, textStatus, error){
						if(xhr.indexOf('501') != -1)
							window[namespace].modal.show('error', {msg: 'Your plan has expired. Please write to us at <a href="support@easyproducttours.com">support@easyproducttours.com</a> to reinstate this account.'});
						else if(xhr.indexOf('502') != -1)
							window[namespace].modal.show('error', {msg: 'You have not selected a valid plan yet. Please <a href="https://www.easyproducttours.com/select-plan" target="_blank">select</a> a plan.'});
						else if(xhr.indexOf('503') != -1)
							window[namespace].modal.show('error', {msg: 'You have reached the tour limit for your plan. To add more tours <a href="https://www.easyproducttours.com/docs/general/change-plan" target="_blank">upgrade</a> your plan.'});
						callback(500, null);
					},
					500: function(xhr, textStatus, error){
						window[namespace].modal.show('error', {msg: 'An error occurred while trying to create a new tour for this app. This is a temporary error. Please try again later.'});
						callback(500, null);
					}
				});
			},
			remove: function(id){
				api.tour.remove(id, {
					200: function(data, textStatus, xhr){
						console.log('Removed tour: '+id+'.');
					},
					500: function(xhr, textStatus, error){
						console.log('Failed to remove tour: '+id+'.');	
					}
				});
				persistanceManager._removeFromPersistanceMedium('tour-'+id);
				persistanceManager.step.next(null, null);
				persistanceManager.tour.list.remove(id);
				var list = persistanceManager.tour.list.get(), len = list.length;
				if(len > 0) id = list[len-1];
				else id = null;
				persistanceManager.tour.current(id);
				return id;
			},
			get: function(id){
				return persistanceManager._getItemFromPersistanceMedium('tour-'+id, true);
			},
			set: function(id, tourData){
				persistanceManager._saveItemToPersistanceMedium('tour-'+id, tourData, true);
			},
			edit: function(id, change, deep){
				deep = deep === false ? false : true;
				var tourData = persistanceManager.tour.get(id);
				tourData = $.extend(deep, tourData, change);
				if(change.steps){
					api.tour.edit(id, tourData.steps, {
						200: function(data, textStatus, xhr){
							console.log('Edited tour: '+id+'.');
						},
						404: function(data, textStatus, xhr){
							console.log('Tour not found: '+id+'. Might have been deleted recently.');
						},
						500: function(data, textStatus, xhr){
							console.log('Failed to edit tour: '+id+'.');
						}
					});
				}
				persistanceManager.tour.set(id, tourData);
			},
			current: function(id){
				if(id===undefined)
					return persistanceManager._getItemFromPersistanceMedium('currentTour', false);
				else if(id===null)
					persistanceManager._removeFromPersistanceMedium('currentTour');
				else
					persistanceManager._saveItemToPersistanceMedium('currentTour', id, false);
			}
		},
		step: {
			list: {
				add: function(tid, stepData){
					if(!stepData) return;
					var list = persistanceManager.step.list.get(tid);
					list = list ? list : [];
					var id = persistanceManager.step.current(tid);
					if(id === null) id = list.length - 1;
					list.splice(++id,0,stepData);
					persistanceManager.tour.edit(tid, {steps: list}, false);
					return id;
				},
				remove: function(tid, id){
					var list = persistanceManager.step.list.get(tid);
					if(list && id > -1 && id < list.length){
						list.splice(id,1);
						persistanceManager.tour.edit(tid, {steps: list}, false);
						if(list.length == 0)
							persistanceManager.step.current(tid, null);
					}
				},
				get: function(tid){
					var tourData = persistanceManager.tour.get(tid);
					return tourData && tourData.steps ? tourData.steps : null;
				},
				move: function(tid, oid, nid){
					var list = persistanceManager.step.list.get(tid);
					var stepData = list[oid];
					list.splice(oid,1);
					list.splice(nid,0,stepData);
					persistanceManager.tour.edit(tid, {steps: list}, false);
				}
			},
			create: function(tid, stepData){
				var id = persistanceManager.step.list.add(tid, stepData);
				persistanceManager.step.current(tid, id);
				return id;
			},
			remove: function(tid, id){
				persistanceManager.step.list.remove(tid, id);
				var list = persistanceManager.step.list.get(tid), len = list.length;
				if(len > 0 && id >= len-1) id = len-1;
				else if(len <= 0) id = null;
				persistanceManager.step.current(tid, id);
				return id;
			},
			get: function(tid, id){
				var steps = persistanceManager.step.list.get(tid);
				return steps && steps[id] ? steps[id] : null;
			},
			edit: function(tid, id, stepData){
				var stepList = persistanceManager.step.list.get(tid);
				if(!stepList[id]) return;
				$.extend(true, stepList[id], stepData);
				persistanceManager.tour.edit(tid, {steps: stepList});
			},
			/*
			addTrigger: function(tid, id, button, eventName){
				var stepData = persistanceManager.step.get(tid, id);
				if(!stepData.b[button]) return;
				stepData.b[button].e = eventName;
				persistanceManager.step.edit(tid, id, stepData);
			},
			removeTrigger: function(tid, id, button){
				persistanceManager.step.addTrigger(tid, id, button, null);
			},
			*/
			current: function(tid, id){
				if(id===undefined){
					var tourData = persistanceManager.tour.get(tid);
					return tourData && typeof tourData.currentStep == 'number' ? tourData.currentStep : null;
				}
				else if(id===null){
					var tourData = persistanceManager.tour.get(tid);
					if(tourData && typeof tourData.currentStep == 'number') delete tourData.currentStep;
					persistanceManager.tour.set(tid, tourData);
				}
				else
					persistanceManager.tour.edit(tid, {currentStep: id});
			},
			next: function(tid, id){
				if(tid === null && id === null)
					persistanceManager._removeFromPersistanceMedium('showNext');
				else if(tid === undefined && id === undefined)
					return persistanceManager._getItemFromPersistanceMedium('showNext', true);
				else
					persistanceManager._saveItemToPersistanceMedium('showNext', {t: tid, s: id}, true);
			},
			lastForURL: function(tid, url){
				var list = persistanceManager.step.list.get(tid), matches = [];
				for(var l=0, len=list ? list.length : 0; l<len; ++l){
					if(list[l].u == url)
						matches.push(parseInt(l));
				}
				if(matches.length > 0) return matches.pop();
				return null;
			}
		}
	});
	window[namespace].persistanceManager.loaded = true;
})(document.head.getAttribute('data-eptEditorNamespace'));