(function(namespace){
	var $ = window[namespace].jQuery;
	var persistanceManager = window[namespace].persistanceManager;
	var api = $.extend(true, window[namespace].api, {
		_endpoint: window.location.protocol + '//www.easyproducttours.com/api/',
		_getCSRFToken: function(){
			var cookies = document.cookie, key='EaToBu_csrf';
			if(!cookies || cookies.length < 1) return null;
			cookies = cookies.split(';');
			for(var c=0,cl=cookies.length; c<cl;++c){
				cookies[c] = cookies[c].trim();
				if(cookies[c].indexOf(key+'=') == 0)
					return cookies[c].replace(key+'=','');
			}
			return null;
		},
		_safePost: function(url, postData, callbackMap){
			url = api._endpoint + url;
			//postData should always be passed in as an object. If passed as string urlencoding issues happen. So pass as object and let jQuery do the heavy lifting
			//append csrf token to postdata params
			$.extend(callbackMap, {420: function(xhr, textStatus, error){
				console.log('420: Possible CSRF attack detected. The server dropped this request.');
			}});

			var _csrf = api._getCSRFToken();
			if(_csrf){
				if(!postData)
					postData = {};
				$.extend(postData, {'_csrf': _csrf});
			}

			$.ajax({
				type: 'POST',
				url: url,
				crossDomain: true,
				dataType: 'json',
				data: postData,
				xhrFields: {
					withCredentials: true
				},
				statusCode: {
					200: function(data, textStatus, xhr){
						if(callbackMap[data.code])
							callbackMap[data.code](data.responseText, textStatus, xhr);
					}
				}
			});
		},
		//User related methods
		user: {
			login: function(email, pass, callbackMap){
				if(email == 'demo@demo' && pass == 'demo@demo'){
					persistanceManager.demo.is(true);
					callbackMap[200]();
				}
				else
					api._safePost('user/login', {e: email, p: pass, v: 'e'}, callbackMap);
			},
			logout: function(callbackMap){
				if(persistanceManager.demo.is()){
					persistanceManager.demo.is(null);
					callbackMap[200]();
				}
				else
					api._safePost('user/logout', null, callbackMap);
			},
			loggedIn: function(callbackMap){
				if(persistanceManager.demo.is())
					callbackMap[200]({app: 'demo'});
				else
					api._safePost('user/loggedin', {url: window.location.href, loggedin: 't'}, callbackMap);
			}
		},
		//Tour related methods
		tour: {
			add: function(name, steps, callbackMap){
				if(persistanceManager.demo.is())
					callbackMap[201]({tour: {id: 'demo-'+Math.random().toString().replace('.',''), name: name, steps: steps}});
				else
					api._safePost('tours/add', {fqdn: window.location.hostname, name: name, steps: steps}, callbackMap);
			},
			get: function(url, callbackMap){
				if(persistanceManager.demo.is())
					callbackMap[200]({});
				else
					api._safePost('tours/get', {url: url}, callbackMap);
			},
			edit: function(tid, steps, callbackMap){
				if(persistanceManager.demo.is())
					callbackMap[200]({id: tid});
				else
					api._safePost('tours/edit', {tour: tid, steps: JSON.stringify(steps)}, callbackMap);
			},
			rename: function(tid, name, callbackMap){
				if(persistanceManager.demo.is())
					callbackMap[200]({id: tid, name: name});
				else
					api._safePost('tours/rename', {tour: tid, name: name}, callbackMap);
			},
			remove: function(tid, callbackMap){
				if(persistanceManager.demo.is())
					callbackMap[200]({id: tid});
				else
					api._safePost('tours/remove', {tour: tid}, callbackMap);
			}
		}
	});
	window[namespace].api.loaded = true;
})(document.head.getAttribute('data-eptEditorNamespace'));