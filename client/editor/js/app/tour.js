(function(namespace){
	var $ = window[namespace].jQuery;
	var persistanceManager = window[namespace].persistanceManager;
	var tour = $.extend(true, window[namespace].tour, {
		suffix: function(){
			return 'New Tour';
		},
		create: function(title){
			persistanceManager.tour.create(title, function(err, id){
				if(!err){
					tour.load(id);
					$('#tour-builder-add-step').trigger('click');
				}
			});
		},
		load: function(id){
			tour.unload();
			tourData = persistanceManager.tour.get(id);
			if(!tourData){
				var tid = $('#tour-builder-switch-tour-container li a.tour-builder-switch-tour-item:first').attr('data-tour');
				if(typeof tid == 'string' && tid.indexOf('demo-') == 0) return tour.load(tid);
				else{
					tid = parseInt(tid);
					return isNaN(tid) ? false : tour.load(tid);
				}
			}
			window[namespace].ui.enableStepButtons();
			$('#tour-builder-tour-title').html(tourData.name+'&nbsp;-&nbsp;');
			$('#tour-builder-delete-tour').prop('disabled', false);
			tour.list.load();
			tour.current(id);
			window[namespace].step.load();
		},
		unload: function(){			
			$('#tour-builder-tour-title').text('');
			window[namespace].step.list.unload();
			window[namespace].step.unload();
			$('#tour-builder-right-buttons button').prop('disabled', true);
			$('#tour-builder-delete-tour').prop('disabled', true);
			tour.current(null);
		},
		remove: function(){
			var id = persistanceManager.tour.current();
			if(id) id = persistanceManager.tour.remove(id);
			tour.list.load();
			tour.load(id);
		},
		current: function(id){
			if(id){
				$('#tour-builder-tour-title').data('currentTour', id);
				$('#tour-builder-switch-tour-item-'+id).prepend('<span class="icon-checkmark"></span>');
				persistanceManager.tour.current(id);
			}
			else if(id === undefined)
				return $('#tour-builder-tour-title').data('currentTour');
			else
				$('#tour-builder-tour-title').removeData('currentTour');
		},
		list: {
			load: function(){
				tour.list.unload();
				var list = persistanceManager.tour.list.get();
				for(var l=0, len=list ? list.length : 0; l<len; ++l){
					var i = list[l];
					list[l] = persistanceManager.tour.get(list[l]);
					if(list[l]){
						var li = '<li><a href="javascript:void(0);" id="tour-builder-switch-tour-item-'+i+'" class="dropdown-submenu tour-builder-switch-tour-item" data-tour="'+i+'">'+list[l].name+'</a></li>';
						$(li).appendTo('#tour-builder-switch-tour-dropdown-menu');
					}
				}
				if(list && list.length > 0){
					$('#tour-builder-switch-tour').prop('disabled', false);
					$('#tour-builder-delete-tour').prop('disabled', false);
					$('#tour-builder-switch-tour-container .tour-builder-switch-tour-item').on('click', function(ev){						
						tour.load($(this).attr('data-tour'));
					});
				}
				else{
					$('#tour-builder-switch-tour').prop('disabled', true);
					$('#tour-builder-delete-tour').prop('disabled', true);					
				}
			},
			unload: function(){
				$('#tour-builder-switch-tour-dropdown-menu').html('');
				$('#tour-builder-switch-tour').prop('disabled', true);
				$('#tour-builder-delete-tour').prop('disabled', true);
			}
		}
	});
	window[namespace].tour.loaded = true;
})(document.head.getAttribute('data-eptEditorNamespace'));