(function(namespace){
	var $ = window[namespace].jQuery;
	var modal = $.extend(true, window[namespace].modal, {
		_getValue: function(input, defaultNull){
			var title = input.val();
			title = title ? title : defaultNull ? null : input.attr('placeholder');
			return title.escapeHTML();
		},
		create: function(){
			return $('<div id="tour-builder-modal">' + 
				'<div class="modal hide fade" id="tour-builder-modalDialog">' +
					'<div class="modal-header">' +
						'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
						'<h4 id="tour-builder-modal-title"></h4>' +
					'</div>' +
					'<div class="modal-body">' +
					'</div>' +
					'<div class="modal-footer">' +
						'<span id="tour-builder-modal-spinner" class="pull-left" style="display:none"></span>' +
						'<span id="tour-builder-modal-notice" class="pull-left" style="display:none"></span>' +
						'<button id="tour-builder-modal-cancel"></button>' +
						'<button id="tour-builder-modal-confirm"></button>' +
					'</div>' +
				'</div>' +
			'</div>').prependTo('#tour-builder-container');
		},
		show: function(dialog, data){
			$('#tour-builder-modal').remove();
			var $modal = modal.create();
			var options = modal[dialog];
			if(options.confirm && options.confirm.track)
				options.confirm.dialog = dialog;
			modal.initModal(options.title, options.body(), options.action, options.cancel, options.confirm, data, options.disableConfirmIfEmpty);
			if(options.track)
				_gaq.push(['_trackEvent', 'modal', 'show', dialog]);
		},
		initModal: function(title, body, action, cancel, confirm, data){
			$(document.body).data('showingModal', true);
			$('#tour-builder-modal-title').text(title);
			$('div .modal-body').html(body);
			var $mCancel = $('#tour-builder-modal-cancel');
			if(cancel){
				$mCancel.text(cancel.text);
				$mCancel.unbind('click');
				$mCancel.bind('click', function(){ 
					if(typeof cancel.action == 'function') 
						cancel.action(); 
					if(!cancel.dontClose)
						$('#tour-builder-modalDialog').tbimodal('hide');
				});
				$mCancel.removeClass().addClass(cancel.type ? cancel.type : 'btn');
				$mCancel.show();
			}
			else
				$mCancel.hide();
			var $mConfirm = $('#tour-builder-modal-confirm');
			if(confirm){
				$mConfirm.text(confirm.text);
				$mConfirm.unbind('click');
				$mConfirm.bind('click', function(ev){
					if(typeof confirm.action == 'function') confirm.action(ev, this);
					if(confirm.track)
						_gaq.push(['_trackEvent', 'modal', 'confirm', confirm.dialog]);
					if(!confirm.dontClose)
						$('#tour-builder-modalDialog').tbimodal('hide');
				});
				$mConfirm.removeClass().addClass(confirm.type ? confirm.type : 'btn btn-primary');
				$mConfirm.show();
			}
			else
				$mConfirm.hide();
			var $mDialog = $('#tour-builder-modalDialog');
			if(data) $mDialog.data('mData', data);
			if(typeof action == 'function') action();
			$mDialog.on('hide', function(ev){
				$('#inputTitle').add(document.body).unbind('keydown');
				$mDialog.removeData('mData');
				$(document.body).removeData('showingModal');
			});
			$('#tour-builder-modal-spinner .spinner').remove();
			$mDialog.tbimodal('show');
		},
		newTour: {
			title: 'New Tour',
			body: function(){ var title = window[namespace].tour.suffix(); title = title.escapeHTML(); return '<form class="form-horizontal"><label class="control-label" for="inputTitle">Tour Name</label><div class="controls"><input type="text" id="inputTitle" placeholder="'+title+'" value="'+title+'"></div></form>'; },
			action: function(){
				var $inp = $('#tour-builder-modalDialog input');
				$('#tour-builder-modalDialog').on('shown', function(){$inp.focus()});
				$inp.keydown(function(ev){
					if(ev.which == 27 || ev.which == 13)
						ev.preventDefault();
					if(ev.which == 27)
						$('#tour-builder-modal-cancel').click();
					else if(ev.which == 13)
						$('#tour-builder-modal-confirm').click();
				});
				var inpChanged = function(ev){
					if([13,27,37,38,39,40].indexOf(ev.which) != -1) return;
					var $t = $(this);
					$('#tour-builder-modalDialog .control-group').removeClass('error');
					$('#tour-builder-modalDialog .help-inline').html('&nbsp;');
					if($t.val().length > 0)
						$('#tour-builder-modal-confirm').prop('disabled', false);
					else
						$('#tour-builder-modal-confirm').prop('disabled', true);
				};
				$inp.keyup(inpChanged).change(inpChanged);
				if($inp.length > 0 && $inp.val().length == 0)
					$('#tour-builder-modal-confirm').prop('disabled', true);
				else
					$('#tour-builder-modal-confirm').prop('disabled', false);
			},
			cancel: { text: 'Cancel' },
			confirm: {
				text: 'Create', 
				action: function(){ 
					window[namespace].tour.create(modal._getValue($('#inputTitle')));
				}
			}
		},
		deleteTour: {
			title: 'Delete Tour',
			body: function(){ return '<p>Are you sure you want to delete this entire tour? This action cannot be undone.<p>'},
			action: function(){
				var $inp = $('#inputTitle');
				$('#modalDialog').on('shown', function(){$inp.focus()});
				$inp.add(document.body).keydown(function(ev){
					if(ev.which == 27 || ev.which == 13){
						ev.preventDefault();
						$('#modal-confirm').click();
					}
				});
			},
			confirm: { text: 'Cancel', type: 'btn' },
			cancel: { 
				text: 'Delete',
				action: function(){
					window[namespace].tour.remove();
				},
				type: 'btn btn-danger'
			}
		},
		editTrigger: {
			title: 'Add Trigger',
			body: function(){
				return '<p>In addition to moving to the next/previous step in the tour, clicking the <span id="tour-builder-modal-buttonname"></span> button can trigger a custom event with the specified name.</p>'+
					'<form class="form-horizontal">' +
						'<div class="control-group">' +
							'<label class="control-label" for="tour-builder-modal-inputEvent">Event Name:</label>' +
							'<div class="controls">' +
								'<input type="text" id="tour-builder-modal-inputEvent">' +
							'</div>' +
						'</div>' +
					'</form>';
			},
			action: function(){
				var mData = $('#tour-builder-modalDialog').data('mData');
				$('#tour-builder-modal-inputEvent').val(mData.eventName);
				if(mData.originalEvent){
					$('#tour-builder-modal-confirm').removeClass('btn-primary').addClass('btn-danger').text('Remove Trigger');
					$('#tour-builder-modal-title').text('Remove Trigger');
					$('#tour-builder-modal-inputEvent').prop('disabled', true).val(mData.originalEvent);
				}
				modal.newTour.action();
			},
			cancel: { text: 'Cancel', type: 'btn' },
			confirm: {
				text: 'Add Trigger',
				action: function(ev, that){
					var $t = $(that), persistanceManager = window[namespace].persistanceManager, name = $.trim($('#tour-builder-modal-inputEvent').val());
					var mData = $('#tour-builder-modalDialog').data('mData');
					if($t.hasClass('btn-primary')){
						if(name.length > 0)
							mData.callback(name);
							//persistanceManager.step.addTrigger(window[namespace].tour.current(), window[namespace].step.current(), mData.button, name);
					}
					else{
						mData.callback(null);
						//persistanceManager.step.removeTrigger(window[namespace].tour.current(), window[namespace].step.current(), mData.button);
					}
				}
			}
		},
		bindElement: {
			title: 'Element Selector',
			body: function(){
				return '<form class="form-horizontal">' +
						'<div class="control-group">' +
							'<label class="control-label" for="tour-builder-modal-inputSelector">Element Selector:</label>' +
							'<div class="controls">' +
								'<input type="text" id="tour-builder-modal-inputSelector"><span class="help-inline">Any valid jQuery selector</span>' +
							'</div>' +
						'</div>' +
					'</form>';
			},
			action: function(){
				var mData = $('#tour-builder-modalDialog').data('mData');
				$('#tour-builder-modal-inputSelector').val(mData.selector);
				if(mData.selector){
					$('#tour-builder-modal-confirm').removeClass('btn-primary').addClass('btn-danger').text('Remove Selector');
					$('#tour-builder-modal-inputSelector').prop('disabled', true).val(mData.selector);
				}
				modal.newTour.action();
			},
			cancel: { text: 'Cancel', type: 'btn', action: function(){ window[namespace].step.bindToElement(null);} },
			confirm: {
				text: 'Add Selector',
				action: function(ev, that){
					var $t = $(that), persistanceManager = window[namespace].persistanceManager, name = $.trim($('#tour-builder-modal-inputSelector').val());
					var $m = $('#tour-builder-modalDialog');
					var mData = $m.data('mData');
					if($t.hasClass('btn-primary')){
						if(name.length > 0){
							try{
								if(name=='#tour-builder-container' || name=='#tour-builder-spacer' || window[namespace].step.boundElement(name).length == 0){
									$m.find('.help-inline').text('No elements on page matched selector');
									$m.find('.control-group').addClass('error');
								}
								else{
									window[namespace].step.bindToElement(name);
									$m.tbimodal('hide');
								}
							}
							catch(e){
								$m.find('.help-inline').text('Invalid selector');
								$m.find('.control-group').addClass('error');
							}
						}
					}
					else{
						window[namespace].step.bindToElement(-1);
						$('#tour-builder-popover-container .popover .btn-bind:nth-child(2)').trigger('click');
						$m.tbimodal('hide');
					}
				},
				dontClose: true
			}
		},
		login: {
			title: 'Login',
			body: function(){
				return '<form class="form-horizontal">'+
							'<div class="control-group" id="tour-builder-control-group-email">'+
								'<label class="control-label" for="tour-builder-inputEmail">Email:</label>'+
								'<div class="controls"><input type="text" id="tour-builder-inputEmail" placeholder="your+email@domain.tld"><span class="help-inline" id="tour-builder-help-email">Emails with \'+\' character accepted</span></div>'+
							'</div>'+
							'<div class="control-group" id="tour-builder-control-group-pass">'+
								'<label class="control-label" for="tour-builder-inputPass">Password:</label>'+
								'<div class="controls"><input type="password" id="tour-builder-inputPass" placeholder="Password"><span class="help-inline" id="tour-builder-help-pass">Minimum six characters</span></div>'+
							'</div>'+
						'</form>'; 
			},
			action: function(){
				modal._notice('<a href="https://www.easyproducttours.com/signup" target="_blank">Don\'t have an account? Signup Now.</a>');
				$('#tour-builder-modalDialog').on('shown', function(){$('#tour-builder-inputEmail').focus()});
				var $email = $('#tour-builder-inputEmail'), $pass = $('#tour-builder-inputPass');
				$email.on('keyup', function(ev){
					$('#tour-builder-help-email').text('Emails with \'+\' character accepted');
					$('#tour-builder-control-group-email').removeClass('error');
					$('#tour-builder-modal-confirm').prop('disabled', false);
					if(ev.which == 13)
						$('#tour-builder-modal-confirm').trigger('click');
				});
				$pass.on('keyup', function(ev){
					$('#tour-builder-help-pass').text('Minimum six characters');
					$('#tour-builder-control-group-pass').removeClass('error');					
					$('#tour-builder-modal-confirm').prop('disabled', false);
					if(ev.which == 13)
						$('#tour-builder-modal-confirm').trigger('click');
				});
			},
			cancel: { 'text': 'Cancel',  type: 'btn' },
			confirm: {
				'text': 'Login', 
				type: 'btn btn-primary',
				dontClose: true,
				action: function(){
					$('#tour-builder-modal-confirm').prop('disabled', true);
					modal._spinner();
					var email = $('#tour-builder-inputEmail').val(), pass = $('#tour-builder-inputPass').val();
					if(modal.login._validateEmail(email) && modal.login._validatePass(pass)){
						var api = window[namespace].api;
						api.user.login(email, pass, {
							200: function(data, textStatus, xhr){								
								modal._spinner();
								$('#tour-builder-modal-confirm').prop('disabled', false);
								$('#tour-builder-modalDialog').tbimodal('hide');
								window[namespace].ui.logged(true);
							},
							400: function(xhr, textStatus, error){
								if(xhr.responseText.indexOf('email') != -1){
									$('#tour-builder-help-email').text('Invalid email');
									$('#tour-builder-inputEmail').focus();
									$('#tour-builder-control-group-email').addClass('error');
								}
								else if(xhr.responseText.indexOf('pass') != -1){
									$('#tour-builder-help-pass').text('Invalid Password. Should be 6 characters or longer.');
									$('#tour-builder-inputPass').focus();
									$('#tour-builder-control-group-pass').addClass('error');
								}
								modal._spinner();
								$('#tour-builder-modal-confirm').prop('disabled', false);
							},
							401: function(xhr, textStatus, error){
								$('#tour-builder-help-pass').html('Password did not match.<br><a href="https://www.easyproducttours.com/password_reset" target="_blank">Forgot your password?</a>')
								$('#tour-builder-control-group-pass').addClass('error');
								modal._spinner();
								$('#tour-builder-modal-confirm').prop('disabled', false);
							},
							404: function(xhr, textStatus, error){
								$('#tour-builder-help-email').html('This email has not been registered.<br><a href="https://www.easyproducttours.com/signup" target="_blank">Create new account?</a>')
								$('#tour-builder-control-group-email').addClass('error');
								modal._spinner();
								$('#tour-builder-modal-confirm').prop('disabled', false);
							}
						});
					}
				}
			},
			_validateEmail: function(email){
				var flag = false, eatpos = email.indexOf('@');
				if(email.length > 4 && eatpos != -1 && eatpos > 0 && eatpos < email.length-1)
					flag = true;
				if(flag){
					$('#tour-builder-help-email').text('Emails with \'+\' character accepted');
					$('#tour-builder-control-group-email').removeClass('error');
				}
				else{
					$('#tour-builder-help-email').text('Invalid email address');
					$('#tour-builder-inputEmail').focus();
					$('#tour-builder-control-group-email').addClass('error');
				}
				return flag;
			},
			_validatePass: function(pass){
				var flag = false;
				if(pass.length > 5)
					flag = true;
				if(flag){
					$('#tour-builder-help-pass').text('Minimum six characters');
					$('#tour-builder-control-group-pass').removeClass('error');
				}
				else{
					$('#tour-builder-help-pass').text('Invalid password');
					$('#tour-builder-inputPass').focus();
					$('#tour-builder-control-group-pass').addClass('error');
				}
				return flag;
			}
		},
		noApp: {
			title: 'Application Not Found',
			body: function(){ return '<p>No application has been created for this website. In order to create a tour, you have to first create an application with this site\'s URL. To create this application click <a href="https://www.easyproducttours.com/dash/new?u='+window.location.href+'" target="_blank">here</a>.<p>'},
			confirm: { text: 'Close', type: 'btn' },
		},
		error: {
			title: 'Sorry, something went wrong :(',
			body: function(){ return '<p id="tour-builder-modal-error-msg"><p>'},
			action: function(){
				var mData = $('#tour-builder-modalDialog').data('mData');
				$('#tour-builder-modal-error-msg').html(mData.msg);
			},
			confirm: { text: 'Close', type: 'btn' },
		},
		_spinner: function(){
			var $s = $('#tour-builder-modal-spinner');
			if($s.is(':visible'))
				$s.hide();
			else
				$s.show();
		},
		_notice: function(val){
			var $n = $('#tour-builder-modal-notice');
			if(val)
				$n.show().html(val);
			else
				$n.hide().html('');
		}
	});
	window[namespace].modal.loaded = true;
})(document.head.getAttribute('data-eptEditorNamespace'));