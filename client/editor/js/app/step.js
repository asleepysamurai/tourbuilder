(function(namespace){
	var parseURL = function(url){
		var a = document.createElement('a');
		a.href = url;
		return a;
	};
	var $ = window[namespace].jQuery;
	var persistanceManager = window[namespace].persistanceManager
		, tour = window[namespace].tour;
	var step = $.extend(true, window[namespace].step, {
		_template: function(stepData){
			return '<div class="popover"><div class="arrow"></div><div class="popover-inner"><span class="pull-right"><small><a href="javascript:void(0);" class="tour-builder-edit-step">Edit</a>&nbsp;</small></span>'+(stepData.t ? '<h3 class="popover-title"></h3>' : '')+'<div class="popover-content"></div>'+(stepData.b && stepData.b.length > 0 ? '<div class="popover-footer popover-title clearfix"></div>' : '')+'</div></div>';
		},
		_suffix: function(){
			var list = persistanceManager.step.list.get(tour.current());
			list = list ? list : [];
			return 'Step ' + (list.length+1);
		},
		_editmode: function(that, $ts, tid, id){
			$ts.data('editmode', true);
			var stepData = persistanceManager.step.get(tid,id);

			$ts.resizable('disable').draggable('disable');
			$(that).text('Done');

			var $tst = $ts.find('.popover-title:first');
			var $tsti = $('<input class="tour-builder-step-title-input" type="text" value="'+$tst.text()+'">').width($tst.width());
			$tst.html('').append($tsti).find('.tour-builder-step-title-input').focus();

			var $tsc = $ts.find('.popover-content');
			var $tsci = $('<textarea class="tour-builder-step-content-input">'+stepData.c+'</textarea>').width($tsc.width()).height($tsc.height());
			$tsc.html('').append($tsci);

			var $tsp = $ts.find('.btn-previous'), $tsf = $ts.find('.popover-footer');
			var tsfw = $tsf.innerWidth() - (parseInt($tsf.css('padding-left')) + parseInt($tsf.css('padding-right')));
			var tsiw = (tsfw/2 - 40) + 'px', tsih = $tsp.height()+'px';
			var dddiv = function(tsih, i, d){
				return '<div class="btn-group"> \
							<button class="btn btn-small dropdown-toggle" data-toggle="tbidropdown" style="height:'+tsih+';"> <span class="caret"></span></button> \
							<ul class="dropdown-menu pull-right"> \
								<li><a href="javascript:void(0);" class="tour-builder-button-trigger">Edit Trigger</a></li> \
								<li class="dropdown-submenu"><a href="javascript:void(0);" class="tour-builder-button-display" data-button="'+i+'" data-display="'+d+'"> \
									Display: ' + (d == 1 ? 'Show' : d == 2 ? 'Hide' : 'Auto') + '</a> \
									<ul class="dropdown-menu"> \
										<li><a href="javascript:void(0);" class="tour-builder-button-display-show tour-builder-button-display-item" data-display="1">Show</a></li> \
										<li><a href="javascript:void(0);" class="tour-builder-button-display-hide tour-builder-button-display-item" data-display="2">Hide</a></li> \
										<li><a href="javascript:void(0);" class="tour-builder-button-display-auto tour-builder-button-display-item" data-display="0">Auto</a></li> \
									</ul> \
								</li> \
							</ul> \
						</div>';
			}
			var $tspi = $('<div class="input-append"><input class="input-small tour-builder-step-previous-input" type="text" value="'+$tsp.text()+'" style="width:'+tsiw+';height:'+tsih+';font-size:'+$tsp.css('font-size')+'">'+dddiv(tsih, 0, stepData.b[0].s)+'</div>');
			var $tsn = $ts.find('.btn-next');
			var $tsni = $('<div class="input-append"><input class="input-small tour-builder-step-next-input" type="text" value="'+$tsn.text()+'" style="width:'+tsiw+';height:'+tsih+';font-size:'+$tsp.css('font-size')+'">'+dddiv(tsih, 1, stepData.b[1].s)+'</div>');
			$ts.find('.footer-previous').append($tspi).find('.tour-builder-button-trigger').on('click', function(ev){
				var $t = $(this);
				window[namespace].modal.show('editTrigger', {button: 0, eventName: 'tourbuilder.step.previous', originalEvent: persistanceManager.step.get(tid,id).b[0].e, callback: function(eventName){
					$t.data('eventName', eventName);
				}});
			});
			$ts.find('.footer-next').append($tsni).find('.tour-builder-button-trigger').on('click', function(ev){
				var $t = $(this);
				window[namespace].modal.show('editTrigger', {button: 1, eventName: 'tourbuilder.step.next', originalEvent: persistanceManager.step.get(tid,id).b[1].e, callback: function(eventName){
					$t.data('eventName', eventName);
				}});
			});
			$ts.find('.tour-builder-button-display-item').on('click', function(ev){
				var $t = $(this), val = parseInt($t.attr('data-display'));
				$t.parent().parent().siblings('.tour-builder-button-display').attr('data-display', val).text('Display: '+(val == 1 ? 'Show' : val == 2 ? 'Hide' : 'Auto'));
			});
			
			$tsp.hide();
			$tsn.hide();
			
			var $footer = $ts.find('.popover-footer');
			$footer.append('<div class="btn-group clearfix btn-group-bind" data-toggle="tbibuttons-radio" style="width:100%;"><button class="btn btn-small btn-bind'+(stepData.p==1?' active':'')+'">Window</button><button class="btn btn-small btn-bind'+(stepData.p==2?' active':'')+'" style="padding-right: 7px; padding-left: 7px">Document</button><button class="btn btn-small btn-bind'+(stepData.p!=1 && stepData.p!=2?' active':'')+'">Element</button></div>')
				.find('.btn-group > .btn-bind:last-child').on('click', function(ev){
					var be = step.bindToElement();
					window[namespace].modal.show('bindElement', {selector: be ? (typeof be == 'string' ? be : null) : (stepData.p != 1 && stepData.p != 2 ? stepData.p : null)});
				});
			$footer.find('.btn-bind').on('mouseup', function(ev){
				var tc = $(this).text();
				if(tc == 'Window' || tc == 'Document'){
					$('#tour-builder-popover-container .popover-footer .btn-group-arrow-position').css('display', 'inline-block');
					$('#tour-builder-popover-container .popover-footer .btn-arrow-points').eq(0).prop('disabled', false);
				}
				else{
					$('#tour-builder-popover-container .popover-footer .btn-group-arrow-position').css('display', 'none');
					$('#tour-builder-popover-container .popover-footer .btn-arrow-points').eq(0).prop('disabled', true);
				}
			});

			stepData.arrow = stepData.arrow ? stepData.arrow : {d: 0, p: 0};
			var $apoints = $footer.append('<div class="btn-group btn-group-arrow-points" data-toggle="tbibuttons-radio" style="width:100%;margin-top:5px;margin-left:0px;padding-top:5px;border-top:1px solid #EBEBEB;"><button class="btn btn-small btn-arrow-points'+(stepData.arrow.d?'':' active')+'" data-points="0">&nbsp;</button><button class="btn btn-small btn-arrow-points'+(stepData.arrow.d==1?' active':'')+'" data-points="1"><span class="icon-arrow-up"></span></button><button class="btn btn-small btn-arrow-points'+(stepData.arrow.d==2?' active':'')+'" data-points="2"><span class="icon-arrow-down"></span></button><button class="btn btn-small btn-arrow-points'+(stepData.arrow.d==3?' active':'')+'" data-points="3"><span class="icon-arrow-left"></span></button><button class="btn btn-small btn-arrow-points'+(stepData.arrow.d==4?' active':'')+'" data-points="4"><span class="icon-arrow-right"></span></button></div>')
				.find('.btn-group .btn-arrow-points').on('click', function(ev){
					var points = parseInt($(this).attr('data-points'));
					if(points == 1 || points == 2){
						$('#tour-builder-popover-container .btn-arrow-position').each(function(){
							var $t = $(this), dir = parseInt($t.attr('data-position'));
							if(dir == 1) $t.text('Left');
							else if(dir == 2) $t.text('Center');
							else if(dir == 3) $t.text('Right');
						});
					}
					else{
						$('#tour-builder-popover-container div .btn-arrow-position').each(function(){
							var $t = $(this), dir = parseInt($t.attr('data-position'));
							if(dir == 1) $t.text('Top');
							else if(dir == 2) $t.text('Middle');
							else if(dir == 3) $t.text('Bottom');
						});
					}
				});
			$footer.append('<div class="btn-group btn-group-arrow-position" data-toggle="tbibuttons-radio" style="width:100%;margin-top:5px;margin-left:0px;"><button class="btn btn-small btn-arrow-position'+(stepData.arrow.p==1?' active':'')+'" data-position="1">T</button><button class="btn btn-small btn-arrow-position'+(stepData.arrow.p==2?' active':'')+'" data-position="2">M</button><button class="btn btn-small btn-arrow-position'+(stepData.arrow.p==3?' active':'')+'" data-position="3">B</button></div>');
			$apoints.filter('.active').trigger('click');
			$footer.find('.btn-bind').filter('.active').trigger('mouseup');

			$footer.find('.btn-group-bind').tbitooltip({placement: 'bottom', title: 'Step Binding Options', attachTo: '#tour-builder-container', maintainPosition: true});
			$footer.find('.btn-group-arrow-points').tbitooltip({placement: 'bottom', title: 'Arrow Pointing Direction', attachTo: '#tour-builder-container', maintainPosition: true});
			$footer.find('.btn-group-arrow-position').tbitooltip({placement: 'bottom', title: 'Arrow Position', attachTo: '#tour-builder-container', maintainPosition: true});
		},
		_previewmode: function(that, $ts, tid, id){
			$ts.removeData('editmode');
			$ts.resizable('enable').draggable('enable');

			var stepData = persistanceManager.step.get(tid,id);
			$(that).text('Edit');

			var $tsti = $ts.find('.tour-builder-step-title-input');
			stepData.t = $tsti.val();
			$ts.find('.popover-title:first').text(stepData.t);
			$tsti.remove();

			var $tsci = $ts.find('.tour-builder-step-content-input');
			stepData.c = $tsci.val();
			$ts.find('.popover-content').html(window[namespace].markdown.toHTML(stepData.c));
			$tsci.remove();

			var $tspi = $ts.find('.tour-builder-step-previous-input');
			var $footer = $ts.find('.footer-previous');
			stepData.b = [{t: $tspi.val().escapeHTML().nl2br(), s: parseInt($footer.find('.tour-builder-button-display').attr('data-display')), e: $footer.find('.tour-builder-button-trigger').data('eventName')}];
			$footer.find('.btn-previous').css('display', stepData.b[0].s == 1 ?  'inline-block' : stepData.b[0].s == 2 || id == 0 ? 'none' : 'inline-block');

			var $tsni = $ts.find('.tour-builder-step-next-input');
			$footer = $ts.find('.footer-next');						
			stepData.b.push({t: $tsni.val().escapeHTML().nl2br(), s: parseInt($footer.find('.tour-builder-button-display').attr('data-display')), e: $footer.find('.tour-builder-button-trigger').data('eventName')});
			$footer.find('.btn-next').css('display', stepData.b[1].s == 1 ?  'inline-block' : stepData.b[1].s == 2 || id == (persistanceManager.step.list.get(tid).length - 1) ? 'none' : 'inline-block');
			$ts.find('.popover-footer').find('.input-append').remove();

			var reload = false;
			$footer = $ts.find('.popover-footer .btn-group');
			var arrowData = {};
			arrowData.d = $footer.eq(1).find('.active').attr('data-points');
			arrowData.p = $footer.eq(2).find('.active').attr('data-position');
			arrowData.d = arrowData.d ? parseInt(arrowData.d) : 0;
			arrowData.p = arrowData.p ? parseInt(arrowData.p) : 0;
			stepData.arrow = stepData.arrow ? stepData.arrow : {d: 0, p: 0};
			if(stepData.arrow.d != arrowData.d || stepData.arrow.p != arrowData.p)
				reload = true;
			stepData.arrow = arrowData;

			var bat = $footer.eq(0).find('.active').text();
			bat = bat == 'Window' ? 1 : bat == 'Document' ? 2 : 3;
			$footer.remove();

			stepData.f = stepData.f ? stepData.f : { t: 92, l: 50, w: 236 };
			if(bat != 3 && !stepData.arrow.d){
				$ts.find('.arrow').hide();
			}			
			if(stepData.p != bat){
				if(bat == 1){
					var off = $(document.body).offset();
					off.top += $(window).scrollTop(), off.left += $(window).scrollLeft();
					$ts.css({top: '-='+off.top, left: '-='+off.left});
					stepData.f.t -= off.top, stepData.f.l -= off.left;
					$ts.draggable('option', 'containment', 'window');
					$ts.draggable('option', 'scroll', false);
				}
				else{
					if(stepData.p == 1 && bat == 2){
						var off = $ts.offset();
						$ts.css({top: off.top, left: off.left});						
						stepData.f.t = off.top - $('#tour-builder-interface').height(), stepData.f.l = off.left;
						$ts.draggable('option', 'containment', 'document');
						$ts.draggable('option', 'scroll', true);
					}
					if(bat == 3){
						bat = step.bindToElement();
						if(bat && bat != stepData.p) reload = true;
						$ts.draggable('disable');
					}
				}
			}
			stepData.p = bat;
			$('#tour-builder-popover-container').css('position', bat == 1 ? 'fixed' : 'static');
			if(bat == 1)
				$ts.css({'top': stepData.f.t, 'left': stepData.f.l});
			if(bat != 1 && bat != 2) reload = true;

			persistanceManager.step.edit(tid, id, stepData);
			if(reload){
				step.unload();
				window.setTimeout(function(){ step.load(id); }, 200);
			}
		},
		_disableStepButtons: function(){
			step.unload();
			var steps = persistanceManager.step.list.get(tour.current());
			if(steps && steps.length > 0)
				$('#tour-builder-delete-step').prop('disabled', true);
			else
				$('#tour-builder-right-buttons button').not('#tour-builder-add-step').prop('disabled', true);			
		},
		boundElement: function(selector){
			var $sel;
			$(selector).each(function(){
				if($(this).parents('#tour-builder-popover-container, #tour-builder-container, #tour-builder-spacer').length == 0) return $sel = $(this);
				else return null;
			});
			$sel = $sel ? $sel.eq(0) : [];
			return $sel;
		},
		bindToElement: function(selector){
			if(selector === null){
				$('#tour-builder-popover-container .popover').removeData('bindSelector');
				var stepData = persistanceManager.step.get(tour.current(),step.current());
				if(stepData.p == 1)
					$('#tour-builder-popover-container .popover .btn-bind:nth-child(1)').trigger('click');
				else if(stepData.p == 2)
					$('#tour-builder-popover-container .popover .btn-bind:nth-child(2)').trigger('click');
				if(stepData.p != 3)
					$('#tour-builder-popover-container .popover .btn-group-arrow-position').css('display', 'inline-block');
			}
			else if(selector === undefined)
				return $('#tour-builder-popover-container .popover').data('bindSelector');
			else
				$('#tour-builder-popover-container .popover').data('bindSelector', selector);
		},
		create: function(){
			var stepData = {t: step._suffix(), c: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', u: window.location.href, p: 1, b: [{t: 'Previous'},{t: 'Next'}]};
			var id = persistanceManager.step.create(tour.current(), stepData);
			step.load(id);
		},
		load: function(id){
			var tid = tour.current(), stepData;
			step.list.load();
			if(typeof id != 'number'){
				id = persistanceManager.step.next();
				if(!id) id = {t: tid, s: persistanceManager.step.lastForURL(tid, window.location.href)};
				if(tid != id.t) id = persistanceManager.step.lastForURL(tid, window.location.href);
				else id = id.s;
				if(typeof id != 'number') return step._disableStepButtons();
				stepData = persistanceManager.step.get(tid, id);
				if(!stepData) return step._disableStepButtons();
				var pURL = parseURL(stepData.u);
				if(pURL && window.location.href.replace(window.location.protocol,'') != pURL.href.replace(pURL.protocol,'')) return step._disableStepButtons();
			}
			else
				stepData = persistanceManager.step.get(tid, id);

			step.unload();
			if(!stepData) return step._disableStepButtons;
			window.setTimeout(function(){
				persistanceManager.step.next(tid, id);
				
				var pURL = parseURL(stepData.u);
				if(pURL && window.location.href.replace(window.location.protocol,'') != pURL.href.replace(pURL.protocol,'')){
					window.location.href = stepData.u;
					return step._disableStepButtons();
				}

				stepData.p = stepData.p ? stepData.p : 1;
				var $be = step.boundElement(stepData.p);
				var bindTo = stepData.p == 1 ? window : stepData.p == 2 ? document.body : $be.length > 0 ? $be : document.body;
				var footer = '', buttons = $.merge([], stepData.b);
				for(var b=0, len=buttons ? buttons.length : 0; b<len; ++b){
					buttons[b].s = buttons[b].s == 1 ? true : 
										buttons[b].s == 2 ? false :
										b == 0 ? 
											id == 0 ? false :
												true :
											id == persistanceManager.step.list.get(tid).length - 1 ? false :
												true;
					footer += '<div class="footer-'+(b==0?'previous':'next')+'"><button class="btn btn-small btn-'+(b==0?'previous':'next')+'" style="display:'+(buttons[b].s ? 'inline-block' : 'none')+'">'+buttons[b].t.escapeHTML()+'</button></div>';
				}
				$('#tour-builder-popover-container').css('position', stepData.p == 1 ? 'fixed' : 'static');
				$(document.body).data('tour-builder-bindTo', bindTo);
				$(bindTo).tbipopover({title: stepData.t.escapeHTML(), content: window[namespace].markdown.toHTML(stepData.c), footer: footer, html: true, placement: stepData.p == 1 || stepData.p == 2 ? 'custom' : 'auto', frame: stepData.f ? {width: stepData.f.w, left: stepData.f.l, top: stepData.f.t + (stepData.p == 2 ? 42 : 0)} : { top: 92, left: 50, width: 236 }, arrow: stepData.arrow, id: 'tour-builder-tour-'+tid+'-step-'+id, attachTo: '#tour-builder-popover-container', trigger: 'manual', template: step._template(stepData), onShow: function(){
					var $ts = $('#tour-builder-tour-'+tid+'-step-'+id);
					$ts.removeData('editmode');

					$ts.find('.btn-previous').on('click', function(ev){
						step.previous();
					});
					$ts.find('.btn-next').on('click', function(ev){
						step.next();
					});

					$ts.resizable({fixed: 'height', containment: 'document', handles: 'e, w, ne, se, sw, nw', autoHide: true,
						stop: function(ev, ui){
							var stepData = persistanceManager.step.get(tid, id);
							if(stepData.p != 1 && stepData.p != 2){
								var $be = step.boundElement(stepData.p);
								var opt = $be.data('tbipopover');
								opt.options.frame = {top: ui.position.top, left: ui.position.left, width: ui.size.width};
								$be.tbipopover('show');
							}
							persistanceManager.step.edit(tid, id, {f: {t: ui.position.top, l: ui.position.left, w: ui.size.width}});
						}
					}).draggable({containment: stepData.p == 1 ? "window" : stepData.p == 2 ? "document" : false, cursor: 'move', 
						start: function(ev, ui){
							stepData = persistanceManager.step.get(tid, id);
						},
						drag: function(ev, ui){
							if(stepData.p == 1){
								var boff = $(document.body).offset();
								if(ui.position.top < -boff.top) ui.position.top = Math.max(ui.position.top, -boff.top);
								if(ui.position.left < -boff.left) ui.position.left = Math.max(ui.position.left, -boff.left);
							}
						},
						stop: function(ev, ui){
							persistanceManager.step.edit(tid, id, {f: {t: ui.position.top - (stepData.p == 2 ? 42 : 0), l: ui.position.left}});
						}
					});
					$ts.draggable('option', 'scroll', stepData.p == 1 ? false : true);

					if(stepData.p !== 1 && stepData.p !== 2)
						$ts.draggable('disable');

					$ts.find('.tour-builder-edit-step').on('click', function(ev){
						if($ts.data('editmode'))
							step._previewmode(this, $ts, tid, id);
						else
							step._editmode(this, $ts, tid, id);
					});
				}}).tbipopover('show');

				if(stepData.p != 1){
					var tpos = $('#tour-builder-tour-'+tid+'-step-'+id).position(), pos;
					if(stepData.p == 2)
						pos = tpos;
					else{
						var bepos = step.boundElement(stepData.p).position();
						pos = {top: Math.min(tpos.top, bepos.top), left: Math.min(tpos.left, bepos.left)};
					}
					var ih = $('#tour-builder-interface').height(), margin = {top: 50, left: 50}, left;
					$('body,html').stop().animate({pageYOffset: pos.top-ih-margin.top, pageXOffset: pos.left-margin.left}, {
						duration: 500,
						easing: 'swing',
						step: function(now, fx) {
							if (fx.prop == 'pageXOffset') {
								left = now;
							} else if (fx.prop == 'pageYOffset') {
								window.scrollTo(left, now);
							}
						}
					});
				}
				
				window[namespace].ui.setNavigationButtons(id);

				step.current(id);
			}, 200);
		},
		unload: function(){
			var bindTo = $(document.body).data('tour-builder-bindTo');
			if(bindTo)
				$(bindTo).tbipopover('destroy');
			$(document.body).removeData('tour-builder-bindTo');
			step.current(null);
		},		
		next: function(){
			var id = step.current(), tid = tour.current();
			if(typeof id != 'number') return;
			var stepList = persistanceManager.step.list.get(tid);
			if(stepList[id] && stepList[id].b[1].e)
				$(document.body).trigger($.Event(stepList[id].b[1].e, {tour: parseInt(tid), step: parseInt(id)}));
			if(!stepList || ++id >= stepList.length) return;
			//Wait 200ms between loading and unloading to prevent the unloaded step from jumping around while fading
			step.unload();
			//window.setTimeout(function(){ step.load(id); }, 200);
			step.load(id);
		},
		previous: function(){
			var id = step.current(), tid = tour.current();
			if(typeof id != 'number') return;
			var stepList = persistanceManager.step.list.get(tid);
			if(stepList[id] && stepList[id].b[0].e)
				$(document.body).trigger($.Event(stepList[id].b[0].e, {tour: parseInt(tid), step: parseInt(id)}));
			if(--id < 0) return;
			//Wait 200ms between loading and unloading to prevent the unloaded step from jumping around while fading
			step.unload();
			//window.setTimeout(function(){ step.load(id); }, 200);
			step.load(id);
		},
		remove: function(){
			var id = step.current();
			if(typeof id != 'number') return;
			id = persistanceManager.step.remove(tour.current(), id);
			step.list.load();
			step.load(id);
		},
		current: function(id){
			if(typeof id == 'number'){
				$('#tour-builder-tour-title').data('currentStep', id);
				$('#tour-builder-switch-step-item-'+id).prepend('<span class="icon-checkmark"></span>');
				persistanceManager.step.current(tour.current(), id);
			}
			else if(id === undefined)
				return $('#tour-builder-tour-title').data('currentStep');
			else
				$('#tour-builder-tour-title').removeData('currentStep');
		},
		list: {
			load: function(){
				step.list.unload();
				var list = persistanceManager.step.list.get(tour.current());
				for(var l=0, len=list ? list.length : 0; l<len; ++l){
					//list[l] = persistanceManager.step.get(list[l]);
					if(list[l]){
						var li = '<li><a href="javascript:void(0);" id="tour-builder-switch-step-item-'+l+'" class="dropdown-submenu tour-builder-switch-step-item" data-step="'+l+'">'+list[l].t+'</a></li>';
						$(li).appendTo('#tour-builder-switch-step-dropdown-menu');
					}
					$('#tour-builder-switch-step-dropdown-menu').sortable('destroy').sortable({axis: 'y', containment: 'parent', cursor: 'move',
						stop: function(ev, ui){
							var nid = ui.item.index();
							persistanceManager.step.list.move(tour.current(), ui.item.find('a').attr('data-step'), nid);
							step.list.load();
						}
					}).tbitooltip('destroy').tbitooltip({html: false, title: 'Drag to re-order', attachTo: '#tour-builder-interface', placement: 'bottom'});
				}
				if(list && list.length > 0){
					$('#tour-builder-switch-step').prop('disabled', false);
					$('#tour-builder-delete-step').prop('disabled', false);
					$('#tour-builder-switch-step-container .tour-builder-switch-step-item').on('click', function(ev){						
						step.load(parseInt($(this).attr('data-step')));
					});
				}
				else{
					$('#tour-builder-switch-step').prop('disabled', true);
					$('#tour-builder-delete-step').prop('disabled', true);					
				}
			},
			unload: function(){
				$('#tour-builder-switch-step-dropdown-menu').html('');
				$('#tour-builder-switch-step').prop('disabled', true);
				$('#tour-builder-delete-step').prop('disabled', true);
			}
		}
	});
	window[namespace].step.loaded = true;
})(document.head.getAttribute('data-eptEditorNamespace'));